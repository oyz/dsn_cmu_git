create_debug_core u_ila_0 ila
set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
set_property ALL_PROBE_SAME_MU_CNT 1 [get_debug_cores u_ila_0]
set_property C_ADV_TRIGGER false [get_debug_cores u_ila_0]
set_property C_DATA_DEPTH 1024 [get_debug_cores u_ila_0]
set_property C_EN_STRG_QUAL false [get_debug_cores u_ila_0]
set_property C_INPUT_PIPE_STAGES 1 [get_debug_cores u_ila_0]
set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
set_property port_width 1 [get_debug_ports u_ila_0/clk]
connect_debug_port u_ila_0/clk [get_nets [list clk_wiz_0_0_inst/inst/clk_out1_10M]]
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
set_property port_width 3 [get_debug_ports u_ila_0/probe0]
connect_debug_port u_ila_0/probe0 [get_nets [list {master_spi_mcp23s17_inst/i_CS_en[0]} {master_spi_mcp23s17_inst/i_CS_en[1]} {master_spi_mcp23s17_inst/i_CS_en[2]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
set_property port_width 3 [get_debug_ports u_ila_0/probe1]
connect_debug_port u_ila_0/probe1 [get_nets [list {master_spi_mcp23s17_inst/i_pin_adrs_A[0]} {master_spi_mcp23s17_inst/i_pin_adrs_A[1]} {master_spi_mcp23s17_inst/i_pin_adrs_A[2]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe2]
set_property port_width 8 [get_debug_ports u_ila_0/probe2]
connect_debug_port u_ila_0/probe2 [get_nets [list {master_spi_mcp23s17_inst/i_reg_adrs_A[0]} {master_spi_mcp23s17_inst/i_reg_adrs_A[1]} {master_spi_mcp23s17_inst/i_reg_adrs_A[2]} {master_spi_mcp23s17_inst/i_reg_adrs_A[3]} {master_spi_mcp23s17_inst/i_reg_adrs_A[4]} {master_spi_mcp23s17_inst/i_reg_adrs_A[5]} {master_spi_mcp23s17_inst/i_reg_adrs_A[6]} {master_spi_mcp23s17_inst/i_reg_adrs_A[7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe3]
set_property port_width 8 [get_debug_ports u_ila_0/probe3]
connect_debug_port u_ila_0/probe3 [get_nets [list {master_spi_mcp23s17_inst/i_socket_en[0]} {master_spi_mcp23s17_inst/i_socket_en[1]} {master_spi_mcp23s17_inst/i_socket_en[2]} {master_spi_mcp23s17_inst/i_socket_en[3]} {master_spi_mcp23s17_inst/i_socket_en[4]} {master_spi_mcp23s17_inst/i_socket_en[5]} {master_spi_mcp23s17_inst/i_socket_en[6]} {master_spi_mcp23s17_inst/i_socket_en[7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe4]
set_property port_width 8 [get_debug_ports u_ila_0/probe4]
connect_debug_port u_ila_0/probe4 [get_nets [list {master_spi_mcp23s17_inst/i_wr_DA[0]} {master_spi_mcp23s17_inst/i_wr_DA[1]} {master_spi_mcp23s17_inst/i_wr_DA[2]} {master_spi_mcp23s17_inst/i_wr_DA[3]} {master_spi_mcp23s17_inst/i_wr_DA[4]} {master_spi_mcp23s17_inst/i_wr_DA[5]} {master_spi_mcp23s17_inst/i_wr_DA[6]} {master_spi_mcp23s17_inst/i_wr_DA[7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe5]
set_property port_width 8 [get_debug_ports u_ila_0/probe5]
connect_debug_port u_ila_0/probe5 [get_nets [list {master_spi_mcp23s17_inst/i_wr_DB[0]} {master_spi_mcp23s17_inst/i_wr_DB[1]} {master_spi_mcp23s17_inst/i_wr_DB[2]} {master_spi_mcp23s17_inst/i_wr_DB[3]} {master_spi_mcp23s17_inst/i_wr_DB[4]} {master_spi_mcp23s17_inst/i_wr_DB[5]} {master_spi_mcp23s17_inst/i_wr_DB[6]} {master_spi_mcp23s17_inst/i_wr_DB[7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe6]
set_property port_width 1 [get_debug_ports u_ila_0/probe6]
connect_debug_port u_ila_0/probe6 [get_nets [list {master_spi_mcp23s17_inst/o_S1_SPI_CSB[0]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe7]
set_property port_width 8 [get_debug_ports u_ila_0/probe7]
connect_debug_port u_ila_0/probe7 [get_nets [list {master_spi_mcp23s17_inst/o_rd_DA[0]} {master_spi_mcp23s17_inst/o_rd_DA[1]} {master_spi_mcp23s17_inst/o_rd_DA[2]} {master_spi_mcp23s17_inst/o_rd_DA[3]} {master_spi_mcp23s17_inst/o_rd_DA[4]} {master_spi_mcp23s17_inst/o_rd_DA[5]} {master_spi_mcp23s17_inst/o_rd_DA[6]} {master_spi_mcp23s17_inst/o_rd_DA[7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe8]
set_property port_width 8 [get_debug_ports u_ila_0/probe8]
connect_debug_port u_ila_0/probe8 [get_nets [list {master_spi_mcp23s17_inst/o_rd_DB[0]} {master_spi_mcp23s17_inst/o_rd_DB[1]} {master_spi_mcp23s17_inst/o_rd_DB[2]} {master_spi_mcp23s17_inst/o_rd_DB[3]} {master_spi_mcp23s17_inst/o_rd_DB[4]} {master_spi_mcp23s17_inst/o_rd_DB[5]} {master_spi_mcp23s17_inst/o_rd_DB[6]} {master_spi_mcp23s17_inst/o_rd_DB[7]}]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe9]
set_property port_width 1 [get_debug_ports u_ila_0/probe9]
connect_debug_port u_ila_0/probe9 [get_nets [list master_spi_mcp23s17_inst/i_forced_pin_mode_en]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe10]
set_property port_width 1 [get_debug_ports u_ila_0/probe10]
connect_debug_port u_ila_0/probe10 [get_nets [list master_spi_mcp23s17_inst/i_forced_sig_csel]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe11]
set_property port_width 1 [get_debug_ports u_ila_0/probe11]
connect_debug_port u_ila_0/probe11 [get_nets [list master_spi_mcp23s17_inst/i_forced_sig_mosi]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe12]
set_property port_width 1 [get_debug_ports u_ila_0/probe12]
connect_debug_port u_ila_0/probe12 [get_nets [list master_spi_mcp23s17_inst/i_forced_sig_sclk]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe13]
set_property port_width 1 [get_debug_ports u_ila_0/probe13]
connect_debug_port u_ila_0/probe13 [get_nets [list master_spi_mcp23s17_inst/i_R_W_bar]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe14]
set_property port_width 1 [get_debug_ports u_ila_0/probe14]
connect_debug_port u_ila_0/probe14 [get_nets [list master_spi_mcp23s17_inst/i_SPIOx_MISO]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe15]
set_property port_width 1 [get_debug_ports u_ila_0/probe15]
connect_debug_port u_ila_0/probe15 [get_nets [list master_spi_mcp23s17_inst/i_trig_SPI_frame]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe16]
set_property port_width 1 [get_debug_ports u_ila_0/probe16]
connect_debug_port u_ila_0/probe16 [get_nets [list master_spi_mcp23s17_inst/o_busy_SPI_frame]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe17]
set_property port_width 1 [get_debug_ports u_ila_0/probe17]
connect_debug_port u_ila_0/probe17 [get_nets [list master_spi_mcp23s17_inst/o_done_SPI_frame]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe18]
set_property port_width 1 [get_debug_ports u_ila_0/probe18]
connect_debug_port u_ila_0/probe18 [get_nets [list master_spi_mcp23s17_inst/o_done_SPI_frame_TO]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe19]
set_property port_width 1 [get_debug_ports u_ila_0/probe19]
connect_debug_port u_ila_0/probe19 [get_nets [list master_spi_mcp23s17_inst/o_SPIOx_MOSI]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe20]
set_property port_width 1 [get_debug_ports u_ila_0/probe20]
connect_debug_port u_ila_0/probe20 [get_nets [list master_spi_mcp23s17_inst/o_SPIOx_SCLK]]
create_debug_port u_ila_0 probe
set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe21]
set_property port_width 1 [get_debug_ports u_ila_0/probe21]
connect_debug_port u_ila_0/probe21 [get_nets [list master_spi_mcp23s17_inst/reset_n]]
set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
connect_debug_port dbg_hub/clk [get_nets clk_out3_10M]
