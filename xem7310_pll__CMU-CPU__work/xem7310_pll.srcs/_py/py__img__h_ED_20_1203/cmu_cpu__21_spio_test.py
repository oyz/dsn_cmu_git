## cmu_cpu__21_spio_test.py : test code for spio MCP23S17


###########################################################################
# check debug mode
if __debug__:
	print('>>> In debug mode ... ')


###########################################################################
###########################################################################

####
## common
#  
def _form_dict_idx_(var,idx):
	return '{:20} --> {}'.format(idx, var[idx])
def _form_dict_idx_hex_16b_(var,idx):
	return '{:20} --> {:#04x}'.format(idx, var[idx])
def _form_hex_32b_(val):
	return '0x{:08X}'.format(val)
#


####
## TODO : library call
import eps
import cmu_cpu__lib as cmu
import cmu_cpu__lib_conf as conf

#eps.eps_test()

# read configuration : EP_ADRS_CONFIG, SPIO_CONFIG
EP_ADRS   = conf.EP_ADRS_CONFIG
SPIO_CONF = conf.SPIO_CONFIG

#  display some configuration info
print(_form_dict_idx_(EP_ADRS,'board_name'))
print(_form_dict_idx_(EP_ADRS,'ver'))
print(_form_dict_idx_(EP_ADRS,'bit_filename'))
#
print(_form_dict_idx_hex_16b_(EP_ADRS,'FPGA_IMAGE_ID'))
print(_form_dict_idx_hex_16b_(EP_ADRS,'TEST_CON'))
print(_form_dict_idx_hex_16b_(EP_ADRS,'BRD_CON'))

####
## TODO: init for end-point system : dev 
dev = eps.EPS_Dev()

####
## test ip 
#_host_,_port_ = eps.set_host_ip_by_ping()
_host_ = '192.168.168.84'
_port_ = 5025
print(_host_)
print(_port_)

####
## open socket and connect
ss = dev.Open(_host_,_port_) 

##  ####
##  ## flag check max retry count
##  MAX_CNT = 50
##  ##MAX_CNT = 500 # 20000 # 280 for 192000 samples
##  #MAX_CNT = 5000 # 1500 for 25600 samples @ 12500 count period	
##  
##  ## check open 
##  if dev.IsOpen():
##  	print('>>> device opened.')
##  else:
##  	print('>>> device is not open.')
##  	## raise
##  	input('')
##  	MAX_CNT = 3	


## get serial from board 
ret = dev.GetSerialNumber()
print(ret)



####
## read fpga_image_id
##$$fpga_image_id = cmu.cmu_read_fpga_image_id()
dev.UpdateWireOuts()
fpga_image_id = dev.GetWireOutValue(EP_ADRS['FPGA_IMAGE_ID'])
#
print(_form_hex_32b_(fpga_image_id))

####
## read FPGA internal temp and volt
##$$ret = cmu.cmu_monitor_fpga()
dev.UpdateWireOuts()
fpga_image_id = dev.GetWireOutValue(EP_ADRS['XADC_TEMP'])  
print(fpga_image_id)


########
# TEST on

####
## test counter on 
ret = cmu.cmu_test_counter(dev, EP_ADRS, 'ON')
print(ret)



##############################################
# TODO: test board control

# BRD_CON[0]  = hw reset
# BRD_CON[16] = timestamp disp en
print('\n>> {}'.format('test hw reset'))
dev.SetWireInValue(EP_ADRS['BRD_CON'],0x00000001,0x00010001) # (ep,val,mask)
dev.UpdateWireIns()

###
#input('ENTER key')

print('\n>> {}'.format('test timestamp disp en'))
dev.SetWireInValue(EP_ADRS['BRD_CON'],0x00010000,0x00010001) # (ep,val,mask)
dev.UpdateWireIns()
#
time_stamp = []
#
dev.UpdateWireOuts()
time_stamp += [dev.GetWireOutValue(EP_ADRS['TIME_STAMP'])]
print('{} = {}'.format('time_stamp',time_stamp))
#
dev.UpdateWireOuts()
time_stamp += [dev.GetWireOutValue(EP_ADRS['TIME_STAMP'])]
print('{} = {}'.format('time_stamp',time_stamp))
#
dev.UpdateWireOuts()
time_stamp += [dev.GetWireOutValue(EP_ADRS['TIME_STAMP'])]
print('{} = {}'.format('time_stamp',time_stamp))
#
time_stamp_diff = [zz[0]-zz[1] for zz in zip(time_stamp[1:], time_stamp[0:-1])]
print('{} = {}'.format('time_stamp_diff',time_stamp_diff))

# convert time based on 10MHz or 100ns or 0.1us
print('{} = {}'.format('time_stamp_diff_us',[xx*0.1 for xx in time_stamp_diff]))
print('{} = {}'.format('time_stamp_diff_ms',[xx*0.1*0.001 for xx in time_stamp_diff]))

###
#input('ENTER key')

print('\n>> {}'.format('test clear all'))
dev.SetWireInValue(EP_ADRS['BRD_CON'],0x00000000,0x00010001) # (ep,val,mask)
dev.UpdateWireIns()

###
#input('ENTER key')


########
# TODO: SPO 

####
ret = cmu.cmu_spo_enable(dev, EP_ADRS)
print(ret)
####

####
ret = cmu.cmu_spo_init(dev, EP_ADRS)
print(ret)
####

####
ret = cmu.cmu_spo_bit__leds(dev, EP_ADRS,0xFF)
print(ret)
####

####
ret = cmu.cmu_spo_bit__amp_pwr(dev, EP_ADRS, 'ON')
#ret = cmu.cmu_spo_bit__amp_pwr(dev, EP_ADRS,'OFF')
print(ret)
####


#############################################


########
# TODO: SPIO 

# enable 
print('\n>> {}'.format('SPIO  enable'))
#dev.SetWireInValue(EP_ADRS['SPIO_WI'], 0x01010001, 0xFFFFFFFF) # (ep,val,mask)
#dev.UpdateWireIns()	
#
cmu.spio_en(dev, EP_ADRS, pin_swap_CSB_MOSI=0) # for CMU 6-pin interface
#cmu.spio_en(dev, EP_ADRS, pin_swap_CSB_MOSI=1) # for PGU 6-pin interface

## note that enabling SPIO makes BIAS_A2A3 disabled.


# trig reset
print('\n>> {}'.format('SPIO  reset'))
#dev.ActivateTriggerIn(EP_ADRS['SPIO_TI'], 0)	## (ep, loc)
cmu.spio_reset(dev, EP_ADRS)

# trig frame
print('\n>> {}'.format('SPIO  send frame'))
#dev.ActivateTriggerIn(EP_ADRS['SPIO_TI'], 1)	## (ep, loc)
#
#ret = cmu.spio_send_frame_ep(dev, EP_ADRS, SPIO_WI=0x01010001, SPIO_FDAT_WI=0x00000001)

#ret = cmu.spio_send_frame(dev, EP_ADRS, 
#	socket_en_8b=0x01, cs_en_3b=0x1, 
#	pin_adrs_3b=0x4, rd__wr_bar=1, reg_adrs_8b=0x00, 
#	data_gpa_8b=0x81, data_gpb_8b=0xCA)

#ret = cmu.spio_send_frame(dev, EP_ADRS, 
#	socket_en_8b=0x01, cs_en_3b=0x1, 
#	pin_adrs_3b=0x0, rd__wr_bar=1, reg_adrs_8b=0x00, 
#	data_gpa_8b=0x81, data_gpb_8b=0xCA)

#ret = cmu.spio_send_frame(dev, EP_ADRS, 
#	socket_en_8b=0x01, cs_en_3b=0x1, 
#	pin_adrs_3b=0x0, rd__wr_bar=0, reg_adrs_8b=0x14, 
#	data_gpa_8b=0x81, data_gpb_8b=0xCA)

#ret = cmu.spio_send_frame(dev, EP_ADRS, 
#	socket_en_8b=0x01, cs_en_3b=0x1, 
#	pin_adrs_3b=0x0, rd__wr_bar=1, reg_adrs_8b=0x14, 
#	data_gpa_8b=0x00, data_gpb_8b=0x00)
#
#ret = cmu.spio_send_frame(dev, EP_ADRS, 
#	socket_en_8b=0x01, cs_en_3b=0x1, 
#	pin_adrs_3b=0x4, rd__wr_bar=1, reg_adrs_8b=0x14, 
#	data_gpa_8b=0x00, data_gpb_8b=0x00)

# set HAEN @ IOCON : enable hardware pin address // @ 0x0A
#ret = cmu.spio_send_frame(dev, EP_ADRS, 
#	socket_en_8b=0x01, cs_en_3b=0x1, 
#	pin_adrs_3b=0x0, rd__wr_bar=0, reg_adrs_8b=0x0A, 
#	data_gpa_8b=0x08, data_gpb_8b=0x08)
#
#print(ret)
#
#ret = cmu.spio_send_frame(dev, EP_ADRS, 
#	socket_en_8b=0x01, cs_en_3b=0x1, 
#	pin_adrs_3b=0x0, rd__wr_bar=1, reg_adrs_8b=0x0A, 
#	data_gpa_8b=0x00, data_gpb_8b=0x00)
#
#print(ret)

## note that MISO line pull-up is must to read back.

# init 
print('\n>> {}'.format('SPIO  init'))
cmu.spio_init(dev, EP_ADRS, SPIO_CONF)

# read regs 
#IOCON, OLAT, IODIR, GPIO = cmu.spio_hw_adrs_scan(dev, EP_ADRS, [0x1, 0x2, 0x3])
#print(IOCON)
#print(OLAT)
#print(IODIR)
#print(GPIO)


#############################################

#######
# TODO: GUI setup 

print('>>> tk GUI control')

#### GUI: Checkbutton and Button
from tkinter import *
from functools import partial


## window obj
master   = Tk()
sub_spio = Toplevel(master)

# titles 
master  .wm_title('CMU-CPU board')
sub_spio.wm_title('SPIO control      ')


## gui variables for board status
ee_FPGA_IMAGE_ID_ = Entry(master, justify='right', width=12)
ee_TEST_FLAG_____ = Entry(master, justify='right', width=12)
ee_MON_XADC______ = Entry(master, justify='right', width=12)
#
ee_temp_fpga_____ = Entry(master, justify='right', width=8)
#
#
ee_FPGA_IMAGE_ID_.insert(0,"0x00000000")
ee_TEST_FLAG_____.insert(0,"0x00000000")
ee_MON_XADC______.insert(0,"0x00000000")
#
ee_temp_fpga_____.insert(0,"0.0")


## gui variables for spio
ee_IOCON_0 = Entry(sub_spio, justify='right', width=12)
ee_IOCON_1 = Entry(sub_spio, justify='right', width=12)
ee_IOCON_2 = Entry(sub_spio, justify='right', width=12)
ee_IOCON_3 = Entry(sub_spio, justify='right', width=12)
ee_IOCON_4 = Entry(sub_spio, justify='right', width=12)
ee_IOCON_5 = Entry(sub_spio, justify='right', width=12)
ee_IOCON_6 = Entry(sub_spio, justify='right', width=12)
ee_IOCON_7 = Entry(sub_spio, justify='right', width=12)
#
ee_IOCON_0  .insert(0,"0x0000")
ee_IOCON_1  .insert(0,"0x0000")
ee_IOCON_2  .insert(0,"0x0000")
ee_IOCON_3  .insert(0,"0x0000")
ee_IOCON_4  .insert(0,"0x0000")
ee_IOCON_5  .insert(0,"0x0000")
ee_IOCON_6  .insert(0,"0x0000")
ee_IOCON_7  .insert(0,"0x0000")

ee_OLAT_0 = Entry(sub_spio, justify='right', width=12)
ee_OLAT_1 = Entry(sub_spio, justify='right', width=12)
ee_OLAT_2 = Entry(sub_spio, justify='right', width=12)
ee_OLAT_3 = Entry(sub_spio, justify='right', width=12)
ee_OLAT_4 = Entry(sub_spio, justify='right', width=12)
ee_OLAT_5 = Entry(sub_spio, justify='right', width=12)
ee_OLAT_6 = Entry(sub_spio, justify='right', width=12)
ee_OLAT_7 = Entry(sub_spio, justify='right', width=12)
# 
ee_OLAT_0  .insert(0,"0x0000")
ee_OLAT_1  .insert(0,"0x0000")
ee_OLAT_2  .insert(0,"0x0000")
ee_OLAT_3  .insert(0,"0x0000")
ee_OLAT_4  .insert(0,"0x0000")
ee_OLAT_5  .insert(0,"0x0000")
ee_OLAT_6  .insert(0,"0x0000")
ee_OLAT_7  .insert(0,"0x0000")

ee_IODIR_0 = Entry(sub_spio, justify='right', width=12)
ee_IODIR_1 = Entry(sub_spio, justify='right', width=12)
ee_IODIR_2 = Entry(sub_spio, justify='right', width=12)
ee_IODIR_3 = Entry(sub_spio, justify='right', width=12)
ee_IODIR_4 = Entry(sub_spio, justify='right', width=12)
ee_IODIR_5 = Entry(sub_spio, justify='right', width=12)
ee_IODIR_6 = Entry(sub_spio, justify='right', width=12)
ee_IODIR_7 = Entry(sub_spio, justify='right', width=12)
# 
ee_IODIR_0  .insert(0,"0x0000")
ee_IODIR_1  .insert(0,"0x0000")
ee_IODIR_2  .insert(0,"0x0000")
ee_IODIR_3  .insert(0,"0x0000")
ee_IODIR_4  .insert(0,"0x0000")
ee_IODIR_5  .insert(0,"0x0000")
ee_IODIR_6  .insert(0,"0x0000")
ee_IODIR_7  .insert(0,"0x0000")

ee_GPIO_0 = Entry(sub_spio, justify='right', width=12)
ee_GPIO_1 = Entry(sub_spio, justify='right', width=12)
ee_GPIO_2 = Entry(sub_spio, justify='right', width=12)
ee_GPIO_3 = Entry(sub_spio, justify='right', width=12)
ee_GPIO_4 = Entry(sub_spio, justify='right', width=12)
ee_GPIO_5 = Entry(sub_spio, justify='right', width=12)
ee_GPIO_6 = Entry(sub_spio, justify='right', width=12)
ee_GPIO_7 = Entry(sub_spio, justify='right', width=12)
#
ee_GPIO_0  .insert(0,"0x0000")
ee_GPIO_1  .insert(0,"0x0000")
ee_GPIO_2  .insert(0,"0x0000")
ee_GPIO_3  .insert(0,"0x0000")
ee_GPIO_4  .insert(0,"0x0000")
ee_GPIO_5  .insert(0,"0x0000")
ee_GPIO_6  .insert(0,"0x0000")
ee_GPIO_7  .insert(0,"0x0000")

var_GPIO_0__GPA0 = IntVar()
var_GPIO_0__GPA1 = IntVar()
var_GPIO_0__GPA2 = IntVar()
var_GPIO_0__GPA3 = IntVar()
var_GPIO_0__GPA4 = IntVar()
var_GPIO_0__GPA5 = IntVar()
var_GPIO_0__GPA6 = IntVar()
var_GPIO_0__GPA7 = IntVar()
var_GPIO_0__GPB0 = IntVar()
var_GPIO_0__GPB1 = IntVar()
var_GPIO_0__GPB2 = IntVar()
var_GPIO_0__GPB3 = IntVar()
var_GPIO_0__GPB4 = IntVar()
var_GPIO_0__GPB5 = IntVar()
var_GPIO_0__GPB6 = IntVar()
var_GPIO_0__GPB7 = IntVar()

var_GPIO_1__GPA0 = IntVar()
var_GPIO_1__GPA1 = IntVar()
var_GPIO_1__GPA2 = IntVar()
var_GPIO_1__GPA3 = IntVar()
var_GPIO_1__GPA4 = IntVar()
var_GPIO_1__GPA5 = IntVar()
var_GPIO_1__GPA6 = IntVar()
var_GPIO_1__GPA7 = IntVar()
var_GPIO_1__GPB0 = IntVar()
var_GPIO_1__GPB1 = IntVar()
var_GPIO_1__GPB2 = IntVar()
var_GPIO_1__GPB3 = IntVar()
var_GPIO_1__GPB4 = IntVar()
var_GPIO_1__GPB5 = IntVar()
var_GPIO_1__GPB6 = IntVar()
var_GPIO_1__GPB7 = IntVar()

var_GPIO_2__GPA0 = IntVar()
var_GPIO_2__GPA1 = IntVar()
var_GPIO_2__GPA2 = IntVar()
var_GPIO_2__GPA3 = IntVar()
var_GPIO_2__GPA4 = IntVar()
var_GPIO_2__GPA5 = IntVar()
var_GPIO_2__GPA6 = IntVar()
var_GPIO_2__GPA7 = IntVar()
var_GPIO_2__GPB0 = IntVar()
var_GPIO_2__GPB1 = IntVar()
var_GPIO_2__GPB2 = IntVar()
var_GPIO_2__GPB3 = IntVar()
var_GPIO_2__GPB4 = IntVar()
var_GPIO_2__GPB5 = IntVar()
var_GPIO_2__GPB6 = IntVar()
var_GPIO_2__GPB7 = IntVar()

var_GPIO_3__GPA0 = IntVar()
var_GPIO_3__GPA1 = IntVar()
var_GPIO_3__GPA2 = IntVar()
var_GPIO_3__GPA3 = IntVar()
var_GPIO_3__GPA4 = IntVar()
var_GPIO_3__GPA5 = IntVar()
var_GPIO_3__GPA6 = IntVar()
var_GPIO_3__GPA7 = IntVar()
var_GPIO_3__GPB0 = IntVar()
var_GPIO_3__GPB1 = IntVar()
var_GPIO_3__GPB2 = IntVar()
var_GPIO_3__GPB3 = IntVar()
var_GPIO_3__GPB4 = IntVar()
var_GPIO_3__GPB5 = IntVar()
var_GPIO_3__GPB6 = IntVar()
var_GPIO_3__GPB7 = IntVar()

var_GPIO_4__GPA0 = IntVar()
var_GPIO_4__GPA1 = IntVar()
var_GPIO_4__GPA2 = IntVar()
var_GPIO_4__GPA3 = IntVar()
var_GPIO_4__GPA4 = IntVar()
var_GPIO_4__GPA5 = IntVar()
var_GPIO_4__GPA6 = IntVar()
var_GPIO_4__GPA7 = IntVar()
var_GPIO_4__GPB0 = IntVar()
var_GPIO_4__GPB1 = IntVar()
var_GPIO_4__GPB2 = IntVar()
var_GPIO_4__GPB3 = IntVar()
var_GPIO_4__GPB4 = IntVar()
var_GPIO_4__GPB5 = IntVar()
var_GPIO_4__GPB6 = IntVar()
var_GPIO_4__GPB7 = IntVar()

var_GPIO_5__GPA0 = IntVar()
var_GPIO_5__GPA1 = IntVar()
var_GPIO_5__GPA2 = IntVar()
var_GPIO_5__GPA3 = IntVar()
var_GPIO_5__GPA4 = IntVar()
var_GPIO_5__GPA5 = IntVar()
var_GPIO_5__GPA6 = IntVar()
var_GPIO_5__GPA7 = IntVar()
var_GPIO_5__GPB0 = IntVar()
var_GPIO_5__GPB1 = IntVar()
var_GPIO_5__GPB2 = IntVar()
var_GPIO_5__GPB3 = IntVar()
var_GPIO_5__GPB4 = IntVar()
var_GPIO_5__GPB5 = IntVar()
var_GPIO_5__GPB6 = IntVar()
var_GPIO_5__GPB7 = IntVar()

var_GPIO_6__GPA0 = IntVar()
var_GPIO_6__GPA1 = IntVar()
var_GPIO_6__GPA2 = IntVar()
var_GPIO_6__GPA3 = IntVar()
var_GPIO_6__GPA4 = IntVar()
var_GPIO_6__GPA5 = IntVar()
var_GPIO_6__GPA6 = IntVar()
var_GPIO_6__GPA7 = IntVar()
var_GPIO_6__GPB0 = IntVar()
var_GPIO_6__GPB1 = IntVar()
var_GPIO_6__GPB2 = IntVar()
var_GPIO_6__GPB3 = IntVar()
var_GPIO_6__GPB4 = IntVar()
var_GPIO_6__GPB5 = IntVar()
var_GPIO_6__GPB6 = IntVar()
var_GPIO_6__GPB7 = IntVar()

var_GPIO_7__GPA0 = IntVar()
var_GPIO_7__GPA1 = IntVar()
var_GPIO_7__GPA2 = IntVar()
var_GPIO_7__GPA3 = IntVar()
var_GPIO_7__GPA4 = IntVar()
var_GPIO_7__GPA5 = IntVar()
var_GPIO_7__GPA6 = IntVar()
var_GPIO_7__GPA7 = IntVar()
var_GPIO_7__GPB0 = IntVar()
var_GPIO_7__GPB1 = IntVar()
var_GPIO_7__GPB2 = IntVar()
var_GPIO_7__GPB3 = IntVar()
var_GPIO_7__GPB4 = IntVar()
var_GPIO_7__GPB5 = IntVar()
var_GPIO_7__GPB6 = IntVar()
var_GPIO_7__GPB7 = IntVar()


### GUI functions ###

def master_bb_update_board_status_wo():
	print('>>> master_bb_update_board_status_wo')
	#
	## read endpoint
	dev.UpdateWireOuts()
	#
	wo20_data = dev.GetWireOutValue(0x20) # FPGA_IMAGE_ID
	wo21_data = dev.GetWireOutValue(0x21) # TEST_FLAG____
	wo3A_data = dev.GetWireOutValue(0x3A) # MON_XADC_____
	#
	print('{} = 0x{:08X}'.format('wo20_data', wo20_data))
	print('{} = 0x{:08X}'.format('wo21_data', wo21_data))
	print('{} = 0x{:08X}'.format('wo3A_data', wo3A_data))
	#
	ee_FPGA_IMAGE_ID_.delete(0,END)
	ee_TEST_FLAG_____.delete(0,END)
	ee_MON_XADC______.delete(0,END)
	#
	ee_temp_fpga_____.delete(0,END)
	#
	ee_FPGA_IMAGE_ID_.insert(0,'0x{:08X}'.format(wo20_data))
	ee_TEST_FLAG_____.insert(0,'0x{:08X}'.format(wo21_data))
	ee_MON_XADC______.insert(0,'0x{:08X}'.format(wo3A_data))
	#
	temp_fpga_____   = wo3A_data/1000
	ee_temp_fpga_____.insert(0,'{}'.format(temp_fpga_____))
	#
	
	return


def spio__gpio_conv__reg_to_bit(GPIO=0x0000):

	# GPIO      = {GPIO_A, GPIO_B}
	# bit _list = [GPIO_A_7, ... , GPIO_B_0]
	bit_list = [0]*16
	
	if ( GPIO&(0x8000>> 0) != 0): bit_list[ 0]=1
	if ( GPIO&(0x8000>> 1) != 0): bit_list[ 1]=1
	if ( GPIO&(0x8000>> 2) != 0): bit_list[ 2]=1
	if ( GPIO&(0x8000>> 3) != 0): bit_list[ 3]=1
	if ( GPIO&(0x8000>> 4) != 0): bit_list[ 4]=1
	if ( GPIO&(0x8000>> 5) != 0): bit_list[ 5]=1
	if ( GPIO&(0x8000>> 6) != 0): bit_list[ 6]=1
	if ( GPIO&(0x8000>> 7) != 0): bit_list[ 7]=1
	if ( GPIO&(0x8000>> 8) != 0): bit_list[ 8]=1
	if ( GPIO&(0x8000>> 9) != 0): bit_list[ 9]=1
	if ( GPIO&(0x8000>>10) != 0): bit_list[10]=1
	if ( GPIO&(0x8000>>11) != 0): bit_list[11]=1
	if ( GPIO&(0x8000>>12) != 0): bit_list[12]=1
	if ( GPIO&(0x8000>>13) != 0): bit_list[13]=1
	if ( GPIO&(0x8000>>14) != 0): bit_list[14]=1
	if ( GPIO&(0x8000>>15) != 0): bit_list[15]=1
	
	return bit_list

def spio__gpio_conv__bit_to_reg(bit_list=[0]*16):

	# bit _list = [GPIO_A_7, ... , GPIO_B_0]
	# GPIO      = {GPIO_A, GPIO_B}
	
	GPIO = 0
	
	if ( bit_list[ 0] != 0): GPIO = GPIO + (0x8000>> 0)
	if ( bit_list[ 1] != 0): GPIO = GPIO + (0x8000>> 1)
	if ( bit_list[ 2] != 0): GPIO = GPIO + (0x8000>> 2)
	if ( bit_list[ 3] != 0): GPIO = GPIO + (0x8000>> 3)
	if ( bit_list[ 4] != 0): GPIO = GPIO + (0x8000>> 4)
	if ( bit_list[ 5] != 0): GPIO = GPIO + (0x8000>> 5)
	if ( bit_list[ 6] != 0): GPIO = GPIO + (0x8000>> 6)
	if ( bit_list[ 7] != 0): GPIO = GPIO + (0x8000>> 7)
	if ( bit_list[ 8] != 0): GPIO = GPIO + (0x8000>> 8)
	if ( bit_list[ 9] != 0): GPIO = GPIO + (0x8000>> 9)
	if ( bit_list[10] != 0): GPIO = GPIO + (0x8000>>10)
	if ( bit_list[11] != 0): GPIO = GPIO + (0x8000>>11)
	if ( bit_list[12] != 0): GPIO = GPIO + (0x8000>>12)
	if ( bit_list[13] != 0): GPIO = GPIO + (0x8000>>13)
	if ( bit_list[14] != 0): GPIO = GPIO + (0x8000>>14)
	if ( bit_list[15] != 0): GPIO = GPIO + (0x8000>>15)
	
	return GPIO


def spio_bb_update():
	print('>>> spio_bb_update')
	
	# update GPIO from bits // not allowed

	# read all 
	IOCON, OLAT, IODIR, GPIO = cmu.spio_hw_adrs_scan(dev, EP_ADRS)

	ee_IOCON_0  .delete(0,END)
	ee_IOCON_1  .delete(0,END)
	ee_IOCON_2  .delete(0,END)
	ee_IOCON_3  .delete(0,END)
	ee_IOCON_4  .delete(0,END)
	ee_IOCON_5  .delete(0,END)
	ee_IOCON_6  .delete(0,END)
	ee_IOCON_7  .delete(0,END)
	#
	ee_IOCON_0  .insert(0,'0x{:04X}'.format(IOCON[0]))
	ee_IOCON_1  .insert(0,'0x{:04X}'.format(IOCON[1]))
	ee_IOCON_2  .insert(0,'0x{:04X}'.format(IOCON[2]))
	ee_IOCON_3  .insert(0,'0x{:04X}'.format(IOCON[3]))
	ee_IOCON_4  .insert(0,'0x{:04X}'.format(IOCON[4]))
	ee_IOCON_5  .insert(0,'0x{:04X}'.format(IOCON[5]))
	ee_IOCON_6  .insert(0,'0x{:04X}'.format(IOCON[6]))
	ee_IOCON_7  .insert(0,'0x{:04X}'.format(IOCON[7]))
	
	
	ee_OLAT_0   .delete(0,END)
	ee_OLAT_1   .delete(0,END)
	ee_OLAT_2   .delete(0,END)
	ee_OLAT_3   .delete(0,END)
	ee_OLAT_4   .delete(0,END)
	ee_OLAT_5   .delete(0,END)
	ee_OLAT_6   .delete(0,END)
	ee_OLAT_7   .delete(0,END)
	#           
	ee_OLAT_0   .insert(0,'0x{:04X}'.format(OLAT[0]))
	ee_OLAT_1   .insert(0,'0x{:04X}'.format(OLAT[1]))
	ee_OLAT_2   .insert(0,'0x{:04X}'.format(OLAT[2]))
	ee_OLAT_3   .insert(0,'0x{:04X}'.format(OLAT[3]))
	ee_OLAT_4   .insert(0,'0x{:04X}'.format(OLAT[4]))
	ee_OLAT_5   .insert(0,'0x{:04X}'.format(OLAT[5]))
	ee_OLAT_6   .insert(0,'0x{:04X}'.format(OLAT[6]))
	ee_OLAT_7   .insert(0,'0x{:04X}'.format(OLAT[7]))
	
	ee_IODIR_0  .delete(0,END)
	ee_IODIR_1  .delete(0,END)
	ee_IODIR_2  .delete(0,END)
	ee_IODIR_3  .delete(0,END)
	ee_IODIR_4  .delete(0,END)
	ee_IODIR_5  .delete(0,END)
	ee_IODIR_6  .delete(0,END)
	ee_IODIR_7	.delete(0,END)
	#           
	ee_IODIR_0  .insert(0,'0x{:04X}'.format(IODIR[0]))
	ee_IODIR_1  .insert(0,'0x{:04X}'.format(IODIR[1]))
	ee_IODIR_2  .insert(0,'0x{:04X}'.format(IODIR[2]))
	ee_IODIR_3  .insert(0,'0x{:04X}'.format(IODIR[3]))
	ee_IODIR_4  .insert(0,'0x{:04X}'.format(IODIR[4]))
	ee_IODIR_5  .insert(0,'0x{:04X}'.format(IODIR[5]))
	ee_IODIR_6  .insert(0,'0x{:04X}'.format(IODIR[6]))
	ee_IODIR_7  .insert(0,'0x{:04X}'.format(IODIR[7]))
	
	ee_GPIO_0  .delete(0,END)
	ee_GPIO_1  .delete(0,END)
	ee_GPIO_2  .delete(0,END)
	ee_GPIO_3  .delete(0,END)
	ee_GPIO_4  .delete(0,END)
	ee_GPIO_5  .delete(0,END)
	ee_GPIO_6  .delete(0,END)
	ee_GPIO_7  .delete(0,END)
	#
	ee_GPIO_0  .insert(0,'0x{:04X}'.format(GPIO[0]))
	ee_GPIO_1  .insert(0,'0x{:04X}'.format(GPIO[1]))
	ee_GPIO_2  .insert(0,'0x{:04X}'.format(GPIO[2]))
	ee_GPIO_3  .insert(0,'0x{:04X}'.format(GPIO[3]))
	ee_GPIO_4  .insert(0,'0x{:04X}'.format(GPIO[4]))
	ee_GPIO_5  .insert(0,'0x{:04X}'.format(GPIO[5]))
	ee_GPIO_6  .insert(0,'0x{:04X}'.format(GPIO[6]))
	ee_GPIO_7  .insert(0,'0x{:04X}'.format(GPIO[7]))
	
	
	# read bits from GPIO
	bit_list_0 = spio__gpio_conv__reg_to_bit (GPIO[0])
	bit_list_1 = spio__gpio_conv__reg_to_bit (GPIO[1])
	bit_list_2 = spio__gpio_conv__reg_to_bit (GPIO[2])
	bit_list_3 = spio__gpio_conv__reg_to_bit (GPIO[3])
	bit_list_4 = spio__gpio_conv__reg_to_bit (GPIO[4])
	bit_list_5 = spio__gpio_conv__reg_to_bit (GPIO[5])
	bit_list_6 = spio__gpio_conv__reg_to_bit (GPIO[6])
	bit_list_7 = spio__gpio_conv__reg_to_bit (GPIO[7])
	
	print(bit_list_0)
	print(bit_list_1)
	print(bit_list_2)
	print(bit_list_3)
	print(bit_list_4)
	print(bit_list_5)
	print(bit_list_6)
	print(bit_list_7)

	
	var_GPIO_0__GPA0.set(bit_list_0[ 7])
	var_GPIO_0__GPA1.set(bit_list_0[ 6])
	var_GPIO_0__GPA2.set(bit_list_0[ 5])
	var_GPIO_0__GPA3.set(bit_list_0[ 4])
	var_GPIO_0__GPA4.set(bit_list_0[ 3])
	var_GPIO_0__GPA5.set(bit_list_0[ 2])
	var_GPIO_0__GPA6.set(bit_list_0[ 1])
	var_GPIO_0__GPA7.set(bit_list_0[ 0])
	var_GPIO_0__GPB0.set(bit_list_0[15])
	var_GPIO_0__GPB1.set(bit_list_0[14])
	var_GPIO_0__GPB2.set(bit_list_0[13])
	var_GPIO_0__GPB3.set(bit_list_0[12])
	var_GPIO_0__GPB4.set(bit_list_0[11])
	var_GPIO_0__GPB5.set(bit_list_0[10])
	var_GPIO_0__GPB6.set(bit_list_0[ 9])
	var_GPIO_0__GPB7.set(bit_list_0[ 8])
	
	var_GPIO_1__GPA0.set(bit_list_1[ 7])
	var_GPIO_1__GPA1.set(bit_list_1[ 6])
	var_GPIO_1__GPA2.set(bit_list_1[ 5])
	var_GPIO_1__GPA3.set(bit_list_1[ 4])
	var_GPIO_1__GPA4.set(bit_list_1[ 3])
	var_GPIO_1__GPA5.set(bit_list_1[ 2])
	var_GPIO_1__GPA6.set(bit_list_1[ 1])
	var_GPIO_1__GPA7.set(bit_list_1[ 0])
	var_GPIO_1__GPB0.set(bit_list_1[15])
	var_GPIO_1__GPB1.set(bit_list_1[14])
	var_GPIO_1__GPB2.set(bit_list_1[13])
	var_GPIO_1__GPB3.set(bit_list_1[12])
	var_GPIO_1__GPB4.set(bit_list_1[11])
	var_GPIO_1__GPB5.set(bit_list_1[10])
	var_GPIO_1__GPB6.set(bit_list_1[ 9])
	var_GPIO_1__GPB7.set(bit_list_1[ 8])
	
	var_GPIO_2__GPA0.set(bit_list_2[ 7])
	var_GPIO_2__GPA1.set(bit_list_2[ 6])
	var_GPIO_2__GPA2.set(bit_list_2[ 5])
	var_GPIO_2__GPA3.set(bit_list_2[ 4])
	var_GPIO_2__GPA4.set(bit_list_2[ 3])
	var_GPIO_2__GPA5.set(bit_list_2[ 2])
	var_GPIO_2__GPA6.set(bit_list_2[ 1])
	var_GPIO_2__GPA7.set(bit_list_2[ 0])
	var_GPIO_2__GPB0.set(bit_list_2[15])
	var_GPIO_2__GPB1.set(bit_list_2[14])
	var_GPIO_2__GPB2.set(bit_list_2[13])
	var_GPIO_2__GPB3.set(bit_list_2[12])
	var_GPIO_2__GPB4.set(bit_list_2[11])
	var_GPIO_2__GPB5.set(bit_list_2[10])
	var_GPIO_2__GPB6.set(bit_list_2[ 9])
	var_GPIO_2__GPB7.set(bit_list_2[ 8])
	
	var_GPIO_3__GPA0.set(bit_list_3[ 7])
	var_GPIO_3__GPA1.set(bit_list_3[ 6])
	var_GPIO_3__GPA2.set(bit_list_3[ 5])
	var_GPIO_3__GPA3.set(bit_list_3[ 4])
	var_GPIO_3__GPA4.set(bit_list_3[ 3])
	var_GPIO_3__GPA5.set(bit_list_3[ 2])
	var_GPIO_3__GPA6.set(bit_list_3[ 1])
	var_GPIO_3__GPA7.set(bit_list_3[ 0])
	var_GPIO_3__GPB0.set(bit_list_3[15])
	var_GPIO_3__GPB1.set(bit_list_3[14])
	var_GPIO_3__GPB2.set(bit_list_3[13])
	var_GPIO_3__GPB3.set(bit_list_3[12])
	var_GPIO_3__GPB4.set(bit_list_3[11])
	var_GPIO_3__GPB5.set(bit_list_3[10])
	var_GPIO_3__GPB6.set(bit_list_3[ 9])
	var_GPIO_3__GPB7.set(bit_list_3[ 8])
	
	var_GPIO_4__GPA0.set(bit_list_4[ 7])
	var_GPIO_4__GPA1.set(bit_list_4[ 6])
	var_GPIO_4__GPA2.set(bit_list_4[ 5])
	var_GPIO_4__GPA3.set(bit_list_4[ 4])
	var_GPIO_4__GPA4.set(bit_list_4[ 3])
	var_GPIO_4__GPA5.set(bit_list_4[ 2])
	var_GPIO_4__GPA6.set(bit_list_4[ 1])
	var_GPIO_4__GPA7.set(bit_list_4[ 0])
	var_GPIO_4__GPB0.set(bit_list_4[15])
	var_GPIO_4__GPB1.set(bit_list_4[14])
	var_GPIO_4__GPB2.set(bit_list_4[13])
	var_GPIO_4__GPB3.set(bit_list_4[12])
	var_GPIO_4__GPB4.set(bit_list_4[11])
	var_GPIO_4__GPB5.set(bit_list_4[10])
	var_GPIO_4__GPB6.set(bit_list_4[ 9])
	var_GPIO_4__GPB7.set(bit_list_4[ 8])
	
	var_GPIO_5__GPA0.set(bit_list_5[ 7])
	var_GPIO_5__GPA1.set(bit_list_5[ 6])
	var_GPIO_5__GPA2.set(bit_list_5[ 5])
	var_GPIO_5__GPA3.set(bit_list_5[ 4])
	var_GPIO_5__GPA4.set(bit_list_5[ 3])
	var_GPIO_5__GPA5.set(bit_list_5[ 2])
	var_GPIO_5__GPA6.set(bit_list_5[ 1])
	var_GPIO_5__GPA7.set(bit_list_5[ 0])
	var_GPIO_5__GPB0.set(bit_list_5[15])
	var_GPIO_5__GPB1.set(bit_list_5[14])
	var_GPIO_5__GPB2.set(bit_list_5[13])
	var_GPIO_5__GPB3.set(bit_list_5[12])
	var_GPIO_5__GPB4.set(bit_list_5[11])
	var_GPIO_5__GPB5.set(bit_list_5[10])
	var_GPIO_5__GPB6.set(bit_list_5[ 9])
	var_GPIO_5__GPB7.set(bit_list_5[ 8])
	
	var_GPIO_6__GPA0.set(bit_list_6[ 7])
	var_GPIO_6__GPA1.set(bit_list_6[ 6])
	var_GPIO_6__GPA2.set(bit_list_6[ 5])
	var_GPIO_6__GPA3.set(bit_list_6[ 4])
	var_GPIO_6__GPA4.set(bit_list_6[ 3])
	var_GPIO_6__GPA5.set(bit_list_6[ 2])
	var_GPIO_6__GPA6.set(bit_list_6[ 1])
	var_GPIO_6__GPA7.set(bit_list_6[ 0])
	var_GPIO_6__GPB0.set(bit_list_6[15])
	var_GPIO_6__GPB1.set(bit_list_6[14])
	var_GPIO_6__GPB2.set(bit_list_6[13])
	var_GPIO_6__GPB3.set(bit_list_6[12])
	var_GPIO_6__GPB4.set(bit_list_6[11])
	var_GPIO_6__GPB5.set(bit_list_6[10])
	var_GPIO_6__GPB6.set(bit_list_6[ 9])
	var_GPIO_6__GPB7.set(bit_list_6[ 8])
	
	var_GPIO_7__GPA0.set(bit_list_7[ 7])
	var_GPIO_7__GPA1.set(bit_list_7[ 6])
	var_GPIO_7__GPA2.set(bit_list_7[ 5])
	var_GPIO_7__GPA3.set(bit_list_7[ 4])
	var_GPIO_7__GPA4.set(bit_list_7[ 3])
	var_GPIO_7__GPA5.set(bit_list_7[ 2])
	var_GPIO_7__GPA6.set(bit_list_7[ 1])
	var_GPIO_7__GPA7.set(bit_list_7[ 0])
	var_GPIO_7__GPB0.set(bit_list_7[15])
	var_GPIO_7__GPB1.set(bit_list_7[14])
	var_GPIO_7__GPB2.set(bit_list_7[13])
	var_GPIO_7__GPB3.set(bit_list_7[12])
	var_GPIO_7__GPB4.set(bit_list_7[11])
	var_GPIO_7__GPB5.set(bit_list_7[10])
	var_GPIO_7__GPB6.set(bit_list_7[ 9])
	var_GPIO_7__GPB7.set(bit_list_7[ 8])
	
	
	return


def spio_bb_update_hwa0():
	# bit _list = [GPIO_A_7, ... , GPIO_B_0]
	bit_list = [
		var_GPIO_0__GPA7.get() ,
		var_GPIO_0__GPA6.get() ,
		var_GPIO_0__GPA5.get() ,
		var_GPIO_0__GPA4.get() ,
		var_GPIO_0__GPA3.get() ,
		var_GPIO_0__GPA2.get() ,
		var_GPIO_0__GPA1.get() ,
		var_GPIO_0__GPA0.get() ,
		var_GPIO_0__GPB7.get() ,
		var_GPIO_0__GPB6.get() ,
		var_GPIO_0__GPB5.get() ,
		var_GPIO_0__GPB4.get() ,
		var_GPIO_0__GPB3.get() ,
		var_GPIO_0__GPB2.get() ,
		var_GPIO_0__GPB1.get() ,
		var_GPIO_0__GPB0.get() ]
	# set gpio from bit
	GPIO_new = spio__gpio_conv__bit_to_reg(bit_list)
	cmu.spio_hw_adrs_write_spio(dev, EP_ADRS, [0x0], GPIO_new)

	# read all 
	IOCON, OLAT, IODIR, GPIO = cmu.spio_hw_adrs_scan(dev, EP_ADRS, [0x0])

	ee_IOCON_0 .delete(0,END)
	ee_IOCON_0 .insert(0,'0x{:04X}'.format(IOCON[0]))

	ee_OLAT_0  .delete(0,END)
	ee_OLAT_0  .insert(0,'0x{:04X}'.format( OLAT[0]))

	ee_IODIR_0 .delete(0,END)
	ee_IODIR_0 .insert(0,'0x{:04X}'.format(IODIR[0]))

	ee_GPIO_0  .delete(0,END)
	ee_GPIO_0  .insert(0,'0x{:04X}'.format( GPIO[0]))
	
	return

def spio_bb_update_hwa1():
	# bit _list = [GPIO_A_7, ... , GPIO_B_0]
	bit_list = [
		var_GPIO_1__GPA7.get() ,
		var_GPIO_1__GPA6.get() ,
		var_GPIO_1__GPA5.get() ,
		var_GPIO_1__GPA4.get() ,
		var_GPIO_1__GPA3.get() ,
		var_GPIO_1__GPA2.get() ,
		var_GPIO_1__GPA1.get() ,
		var_GPIO_1__GPA0.get() ,
		var_GPIO_1__GPB7.get() ,
		var_GPIO_1__GPB6.get() ,
		var_GPIO_1__GPB5.get() ,
		var_GPIO_1__GPB4.get() ,
		var_GPIO_1__GPB3.get() ,
		var_GPIO_1__GPB2.get() ,
		var_GPIO_1__GPB1.get() ,
		var_GPIO_1__GPB0.get() ]
	# set gpio from bit
	GPIO_new = spio__gpio_conv__bit_to_reg(bit_list)
	cmu.spio_hw_adrs_write_spio(dev, EP_ADRS, [0x1], GPIO_new)

	# read all 
	IOCON, OLAT, IODIR, GPIO = cmu.spio_hw_adrs_scan(dev, EP_ADRS, [0x1])

	ee_IOCON_1 .delete(0,END)
	ee_IOCON_1 .insert(0,'0x{:04X}'.format(IOCON[0]))

	ee_OLAT_1  .delete(0,END)
	ee_OLAT_1  .insert(0,'0x{:04X}'.format( OLAT[0]))

	ee_IODIR_1 .delete(0,END)
	ee_IODIR_1 .insert(0,'0x{:04X}'.format(IODIR[0]))

	ee_GPIO_1  .delete(0,END)
	ee_GPIO_1  .insert(0,'0x{:04X}'.format( GPIO[0]))
	return

def spio_bb_update_hwa2():
	# bit _list = [GPIO_A_7, ... , GPIO_B_0]
	bit_list = [
		var_GPIO_2__GPA7.get() ,
		var_GPIO_2__GPA6.get() ,
		var_GPIO_2__GPA5.get() ,
		var_GPIO_2__GPA4.get() ,
		var_GPIO_2__GPA3.get() ,
		var_GPIO_2__GPA2.get() ,
		var_GPIO_2__GPA1.get() ,
		var_GPIO_2__GPA0.get() ,
		var_GPIO_2__GPB7.get() ,
		var_GPIO_2__GPB6.get() ,
		var_GPIO_2__GPB5.get() ,
		var_GPIO_2__GPB4.get() ,
		var_GPIO_2__GPB3.get() ,
		var_GPIO_2__GPB2.get() ,
		var_GPIO_2__GPB1.get() ,
		var_GPIO_2__GPB0.get() ]
	# set gpio from bit
	GPIO_new = spio__gpio_conv__bit_to_reg(bit_list)
	cmu.spio_hw_adrs_write_spio(dev, EP_ADRS, [0x2], GPIO_new)

	# read all 
	IOCON, OLAT, IODIR, GPIO = cmu.spio_hw_adrs_scan(dev, EP_ADRS, [0x2])

	ee_IOCON_2 .delete(0,END)
	ee_IOCON_2 .insert(0,'0x{:04X}'.format(IOCON[0]))

	ee_OLAT_2  .delete(0,END)
	ee_OLAT_2  .insert(0,'0x{:04X}'.format( OLAT[0]))

	ee_IODIR_2 .delete(0,END)
	ee_IODIR_2 .insert(0,'0x{:04X}'.format(IODIR[0]))

	ee_GPIO_2  .delete(0,END)
	ee_GPIO_2  .insert(0,'0x{:04X}'.format( GPIO[0]))
	
	return

def spio_bb_update_hwa3():
	# bit _list = [GPIO_A_7, ... , GPIO_B_0]
	bit_list = [
		var_GPIO_3__GPA7.get() ,
		var_GPIO_3__GPA6.get() ,
		var_GPIO_3__GPA5.get() ,
		var_GPIO_3__GPA4.get() ,
		var_GPIO_3__GPA3.get() ,
		var_GPIO_3__GPA2.get() ,
		var_GPIO_3__GPA1.get() ,
		var_GPIO_3__GPA0.get() ,
		var_GPIO_3__GPB7.get() ,
		var_GPIO_3__GPB6.get() ,
		var_GPIO_3__GPB5.get() ,
		var_GPIO_3__GPB4.get() ,
		var_GPIO_3__GPB3.get() ,
		var_GPIO_3__GPB2.get() ,
		var_GPIO_3__GPB1.get() ,
		var_GPIO_3__GPB0.get() ]
	# set gpio from bit
	GPIO_new = spio__gpio_conv__bit_to_reg(bit_list)
	cmu.spio_hw_adrs_write_spio(dev, EP_ADRS, [0x3], GPIO_new)

	# read all 
	IOCON, OLAT, IODIR, GPIO = cmu.spio_hw_adrs_scan(dev, EP_ADRS, [0x3])

	ee_IOCON_3 .delete(0,END)
	ee_IOCON_3 .insert(0,'0x{:04X}'.format(IOCON[0]))

	ee_OLAT_3  .delete(0,END)
	ee_OLAT_3  .insert(0,'0x{:04X}'.format( OLAT[0]))

	ee_IODIR_3 .delete(0,END)
	ee_IODIR_3 .insert(0,'0x{:04X}'.format(IODIR[0]))

	ee_GPIO_3  .delete(0,END)
	ee_GPIO_3  .insert(0,'0x{:04X}'.format( GPIO[0]))
	
	return

def spio_bb_update_hwa4():
	# bit _list = [GPIO_A_7, ... , GPIO_B_0]
	bit_list = [
		var_GPIO_4__GPA7.get() ,
		var_GPIO_4__GPA6.get() ,
		var_GPIO_4__GPA5.get() ,
		var_GPIO_4__GPA4.get() ,
		var_GPIO_4__GPA3.get() ,
		var_GPIO_4__GPA2.get() ,
		var_GPIO_4__GPA1.get() ,
		var_GPIO_4__GPA0.get() ,
		var_GPIO_4__GPB7.get() ,
		var_GPIO_4__GPB6.get() ,
		var_GPIO_4__GPB5.get() ,
		var_GPIO_4__GPB4.get() ,
		var_GPIO_4__GPB3.get() ,
		var_GPIO_4__GPB2.get() ,
		var_GPIO_4__GPB1.get() ,
		var_GPIO_4__GPB0.get() ]
	# set gpio from bit
	GPIO_new = spio__gpio_conv__bit_to_reg(bit_list)
	cmu.spio_hw_adrs_write_spio(dev, EP_ADRS, [0x4], GPIO_new)

	# read all 
	IOCON, OLAT, IODIR, GPIO = cmu.spio_hw_adrs_scan(dev, EP_ADRS, [0x4])

	ee_IOCON_4 .delete(0,END)
	ee_IOCON_4 .insert(0,'0x{:04X}'.format(IOCON[0]))

	ee_OLAT_4  .delete(0,END)
	ee_OLAT_4  .insert(0,'0x{:04X}'.format( OLAT[0]))

	ee_IODIR_4 .delete(0,END)
	ee_IODIR_4 .insert(0,'0x{:04X}'.format(IODIR[0]))

	ee_GPIO_4  .delete(0,END)
	ee_GPIO_4  .insert(0,'0x{:04X}'.format( GPIO[0]))
	
	return

def spio_bb_update_hwa5():
	# bit _list = [GPIO_A_7, ... , GPIO_B_0]
	bit_list = [
		var_GPIO_5__GPA7.get() ,
		var_GPIO_5__GPA6.get() ,
		var_GPIO_5__GPA5.get() ,
		var_GPIO_5__GPA4.get() ,
		var_GPIO_5__GPA3.get() ,
		var_GPIO_5__GPA2.get() ,
		var_GPIO_5__GPA1.get() ,
		var_GPIO_5__GPA0.get() ,
		var_GPIO_5__GPB7.get() ,
		var_GPIO_5__GPB6.get() ,
		var_GPIO_5__GPB5.get() ,
		var_GPIO_5__GPB4.get() ,
		var_GPIO_5__GPB3.get() ,
		var_GPIO_5__GPB2.get() ,
		var_GPIO_5__GPB1.get() ,
		var_GPIO_5__GPB0.get() ]
	# set gpio from bit
	GPIO_new = spio__gpio_conv__bit_to_reg(bit_list)
	cmu.spio_hw_adrs_write_spio(dev, EP_ADRS, [0x5], GPIO_new)

	# read all 
	IOCON, OLAT, IODIR, GPIO = cmu.spio_hw_adrs_scan(dev, EP_ADRS, [0x5])

	ee_IOCON_5 .delete(0,END)
	ee_IOCON_5 .insert(0,'0x{:04X}'.format(IOCON[0]))

	ee_OLAT_5  .delete(0,END)
	ee_OLAT_5  .insert(0,'0x{:04X}'.format( OLAT[0]))

	ee_IODIR_5 .delete(0,END)
	ee_IODIR_5 .insert(0,'0x{:04X}'.format(IODIR[0]))

	ee_GPIO_5  .delete(0,END)
	ee_GPIO_5  .insert(0,'0x{:04X}'.format( GPIO[0]))
	
	return

def spio_bb_update_hwa6():
	# bit _list = [GPIO_A_7, ... , GPIO_B_0]
	bit_list = [
		var_GPIO_6__GPA7.get() ,
		var_GPIO_6__GPA6.get() ,
		var_GPIO_6__GPA5.get() ,
		var_GPIO_6__GPA4.get() ,
		var_GPIO_6__GPA3.get() ,
		var_GPIO_6__GPA2.get() ,
		var_GPIO_6__GPA1.get() ,
		var_GPIO_6__GPA0.get() ,
		var_GPIO_6__GPB7.get() ,
		var_GPIO_6__GPB6.get() ,
		var_GPIO_6__GPB5.get() ,
		var_GPIO_6__GPB4.get() ,
		var_GPIO_6__GPB3.get() ,
		var_GPIO_6__GPB2.get() ,
		var_GPIO_6__GPB1.get() ,
		var_GPIO_6__GPB0.get() ]
	# set gpio from bit
	GPIO_new = spio__gpio_conv__bit_to_reg(bit_list)
	cmu.spio_hw_adrs_write_spio(dev, EP_ADRS, [0x6], GPIO_new)

	# read all 
	IOCON, OLAT, IODIR, GPIO = cmu.spio_hw_adrs_scan(dev, EP_ADRS, [0x6])

	ee_IOCON_6 .delete(0,END)
	ee_IOCON_6 .insert(0,'0x{:04X}'.format(IOCON[0]))

	ee_OLAT_6  .delete(0,END)
	ee_OLAT_6  .insert(0,'0x{:04X}'.format( OLAT[0]))

	ee_IODIR_6 .delete(0,END)
	ee_IODIR_6 .insert(0,'0x{:04X}'.format(IODIR[0]))

	ee_GPIO_6  .delete(0,END)
	ee_GPIO_6  .insert(0,'0x{:04X}'.format( GPIO[0]))
	
	return

def spio_bb_update_hwa7():
	# bit _list = [GPIO_A_7, ... , GPIO_B_0]
	bit_list = [
		var_GPIO_7__GPA7.get() ,
		var_GPIO_7__GPA6.get() ,
		var_GPIO_7__GPA5.get() ,
		var_GPIO_7__GPA4.get() ,
		var_GPIO_7__GPA3.get() ,
		var_GPIO_7__GPA2.get() ,
		var_GPIO_7__GPA1.get() ,
		var_GPIO_7__GPA0.get() ,
		var_GPIO_7__GPB7.get() ,
		var_GPIO_7__GPB6.get() ,
		var_GPIO_7__GPB5.get() ,
		var_GPIO_7__GPB4.get() ,
		var_GPIO_7__GPB3.get() ,
		var_GPIO_7__GPB2.get() ,
		var_GPIO_7__GPB1.get() ,
		var_GPIO_7__GPB0.get() ]
	# set gpio from bit
	GPIO_new = spio__gpio_conv__bit_to_reg(bit_list)
	cmu.spio_hw_adrs_write_spio(dev, EP_ADRS, [0x7], GPIO_new)

	# read all 
	IOCON, OLAT, IODIR, GPIO = cmu.spio_hw_adrs_scan(dev, EP_ADRS, [0x7])

	ee_IOCON_7 .delete(0,END)
	ee_IOCON_7 .insert(0,'0x{:04X}'.format(IOCON[0]))

	ee_OLAT_7  .delete(0,END)
	ee_OLAT_7  .insert(0,'0x{:04X}'.format( OLAT[0]))

	ee_IODIR_7 .delete(0,END)
	ee_IODIR_7 .insert(0,'0x{:04X}'.format(IODIR[0]))

	ee_GPIO_7  .delete(0,END)
	ee_GPIO_7  .insert(0,'0x{:04X}'.format( GPIO[0]))
	
	return


def tk_win_setup__spio(sub_spio, row_SPIO, SPIO_CONF):
	#### sub_spio ####
	Label(sub_spio, text="===[SPIO]===").grid(row=row_SPIO, column= 0, sticky=W, columnspan = 4)
	
	# display :
	#
	# HW adrs   | HWA0  HWA1  HWA2  HWA3  HWA4  HWA5  HWA6  HWA7
	# reg_IOCON | xxxx  xxxx  xxxx  xxxx  xxxx  xxxx  xxxx  xxxx
	# reg_OLAT  | xxxx  xxxx  xxxx  xxxx  xxxx  xxxx  xxxx  xxxx
	# reg_IODIR | xxxx  xxxx  xxxx  xxxx  xxxx  xxxx  xxxx  xxxx
	# reg_GPIO  | xxxx  xxxx  xxxx  xxxx  xxxx  xxxx  xxxx  xxxx
	# GPB bit7  |  []    []    []    []    []    []    []    []
	# GPB bit6  |  []    []    []    []    []    []    []    []
	# GPB bit5  |  []    []    []    []    []    []    []    []
	# GPB bit4  |  []    []    []    []    []    []    []    []
	# GPB bit3  |  []    []    []    []    []    []    []    []
	# GPB bit2  |  []    []    []    []    []    []    []    []
	# GPB bit1  |  []    []    []    []    []    []    []    []
	# GPB bit0  |  []    []    []    []    []    []    []    []
	# GPA bit7  |  []    []    []    []    []    []    []    []
	# GPA bit6  |  []    []    []    []    []    []    []    []
	# GPA bit5  |  []    []    []    []    []    []    []    []
	# GPA bit4  |  []    []    []    []    []    []    []    []
	# GPA bit3  |  []    []    []    []    []    []    []    []
	# GPA bit2  |  []    []    []    []    []    []    []    []
	# GPA bit1  |  []    []    []    []    []    []    []    []
	# GPA bit0  |  []    []    []    []    []    []    []    []
	# updates   |  [<---          update SPIO             --->]
	
	# labels 
	Label(sub_spio, text="HW adrs   : ").grid(row=row_SPIO+ 1, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="reg_IOCON : ").grid(row=row_SPIO+ 2, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="reg_OLAT  : ").grid(row=row_SPIO+ 3, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="reg_IODIR : ").grid(row=row_SPIO+ 4, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="reg_GPIO  : ").grid(row=row_SPIO+ 5, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="GPB bit7  : ").grid(row=row_SPIO+ 6, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="GPB bit6  : ").grid(row=row_SPIO+ 7, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="GPB bit5  : ").grid(row=row_SPIO+ 8, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="GPB bit4  : ").grid(row=row_SPIO+ 9, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="GPB bit3  : ").grid(row=row_SPIO+10, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="GPB bit2  : ").grid(row=row_SPIO+11, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="GPB bit1  : ").grid(row=row_SPIO+12, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="GPB bit0  : ").grid(row=row_SPIO+13, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="GPA bit7  : ").grid(row=row_SPIO+14, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="GPA bit6  : ").grid(row=row_SPIO+15, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="GPA bit5  : ").grid(row=row_SPIO+16, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="GPA bit4  : ").grid(row=row_SPIO+17, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="GPA bit3  : ").grid(row=row_SPIO+18, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="GPA bit2  : ").grid(row=row_SPIO+19, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="GPA bit1  : ").grid(row=row_SPIO+20, column=0, sticky=W, columnspan = 1)
	Label(sub_spio, text="GPA bit0  : ").grid(row=row_SPIO+21, column=0, sticky=W, columnspan = 1)
	#
	Label(sub_spio, text="updates   : ").grid(row=row_SPIO+31, column=0, sticky=W, columnspan = 1)
	
	Label(sub_spio, text="HWA0  ").grid(row=row_SPIO+1, column=1, sticky=W, columnspan = 1)
	Label(sub_spio, text="HWA1  ").grid(row=row_SPIO+1, column=2, sticky=W, columnspan = 1)
	Label(sub_spio, text="HWA2  ").grid(row=row_SPIO+1, column=3, sticky=W, columnspan = 1)
	Label(sub_spio, text="HWA3  ").grid(row=row_SPIO+1, column=4, sticky=W, columnspan = 1)
	Label(sub_spio, text="HWA4  ").grid(row=row_SPIO+1, column=5, sticky=W, columnspan = 1)
	Label(sub_spio, text="HWA5  ").grid(row=row_SPIO+1, column=6, sticky=W, columnspan = 1)
	Label(sub_spio, text="HWA6  ").grid(row=row_SPIO+1, column=7, sticky=W, columnspan = 1)
	Label(sub_spio, text="HWA7  ").grid(row=row_SPIO+1, column=8, sticky=W, columnspan = 1)

	ee_IOCON_0                    .grid(row=row_SPIO+2, column=1)
	ee_IOCON_1                    .grid(row=row_SPIO+2, column=2)
	ee_IOCON_2                    .grid(row=row_SPIO+2, column=3)
	ee_IOCON_3                    .grid(row=row_SPIO+2, column=4)
	ee_IOCON_4                    .grid(row=row_SPIO+2, column=5)
	ee_IOCON_5                    .grid(row=row_SPIO+2, column=6)
	ee_IOCON_6                    .grid(row=row_SPIO+2, column=7)
	ee_IOCON_7                    .grid(row=row_SPIO+2, column=8)
	
	ee_OLAT_0                     .grid(row=row_SPIO+3, column=1)
	ee_OLAT_1                     .grid(row=row_SPIO+3, column=2)
	ee_OLAT_2                     .grid(row=row_SPIO+3, column=3)
	ee_OLAT_3                     .grid(row=row_SPIO+3, column=4)
	ee_OLAT_4                     .grid(row=row_SPIO+3, column=5)
	ee_OLAT_5                     .grid(row=row_SPIO+3, column=6)
	ee_OLAT_6                     .grid(row=row_SPIO+3, column=7)
	ee_OLAT_7                     .grid(row=row_SPIO+3, column=8)
	
	ee_IODIR_0                    .grid(row=row_SPIO+4, column=1)
	ee_IODIR_1                    .grid(row=row_SPIO+4, column=2)
	ee_IODIR_2                    .grid(row=row_SPIO+4, column=3)
	ee_IODIR_3                    .grid(row=row_SPIO+4, column=4)
	ee_IODIR_4                    .grid(row=row_SPIO+4, column=5)
	ee_IODIR_5                    .grid(row=row_SPIO+4, column=6)
	ee_IODIR_6                    .grid(row=row_SPIO+4, column=7)
	ee_IODIR_7                    .grid(row=row_SPIO+4, column=8)
	
	ee_GPIO_0                     .grid(row=row_SPIO+5, column=1)
	ee_GPIO_1                     .grid(row=row_SPIO+5, column=2)
	ee_GPIO_2                     .grid(row=row_SPIO+5, column=3)
	ee_GPIO_3                     .grid(row=row_SPIO+5, column=4)
	ee_GPIO_4                     .grid(row=row_SPIO+5, column=5)
	ee_GPIO_5                     .grid(row=row_SPIO+5, column=6)
	ee_GPIO_6                     .grid(row=row_SPIO+5, column=7)
	ee_GPIO_7                     .grid(row=row_SPIO+5, column=8)
	
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][0][7], variable=var_GPIO_0__GPB7).grid(row=row_SPIO+ 6, column=1, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][0][6], variable=var_GPIO_0__GPB6).grid(row=row_SPIO+ 7, column=1, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][0][5], variable=var_GPIO_0__GPB5).grid(row=row_SPIO+ 8, column=1, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][0][4], variable=var_GPIO_0__GPB4).grid(row=row_SPIO+ 9, column=1, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][0][3], variable=var_GPIO_0__GPB3).grid(row=row_SPIO+10, column=1, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][0][2], variable=var_GPIO_0__GPB2).grid(row=row_SPIO+11, column=1, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][0][1], variable=var_GPIO_0__GPB1).grid(row=row_SPIO+12, column=1, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][0][0], variable=var_GPIO_0__GPB0).grid(row=row_SPIO+13, column=1, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][0][7], variable=var_GPIO_0__GPA7).grid(row=row_SPIO+14, column=1, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][0][6], variable=var_GPIO_0__GPA6).grid(row=row_SPIO+15, column=1, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][0][5], variable=var_GPIO_0__GPA5).grid(row=row_SPIO+16, column=1, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][0][4], variable=var_GPIO_0__GPA4).grid(row=row_SPIO+17, column=1, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][0][3], variable=var_GPIO_0__GPA3).grid(row=row_SPIO+18, column=1, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][0][2], variable=var_GPIO_0__GPA2).grid(row=row_SPIO+19, column=1, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][0][1], variable=var_GPIO_0__GPA1).grid(row=row_SPIO+20, column=1, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][0][0], variable=var_GPIO_0__GPA0).grid(row=row_SPIO+21, column=1, sticky=W) 
	
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][1][7], variable=var_GPIO_1__GPB7).grid(row=row_SPIO+ 6, column=2, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][1][6], variable=var_GPIO_1__GPB6).grid(row=row_SPIO+ 7, column=2, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][1][5], variable=var_GPIO_1__GPB5).grid(row=row_SPIO+ 8, column=2, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][1][4], variable=var_GPIO_1__GPB4).grid(row=row_SPIO+ 9, column=2, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][1][3], variable=var_GPIO_1__GPB3).grid(row=row_SPIO+10, column=2, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][1][2], variable=var_GPIO_1__GPB2).grid(row=row_SPIO+11, column=2, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][1][1], variable=var_GPIO_1__GPB1).grid(row=row_SPIO+12, column=2, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][1][0], variable=var_GPIO_1__GPB0).grid(row=row_SPIO+13, column=2, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][1][7], variable=var_GPIO_1__GPA7).grid(row=row_SPIO+14, column=2, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][1][6], variable=var_GPIO_1__GPA6).grid(row=row_SPIO+15, column=2, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][1][5], variable=var_GPIO_1__GPA5).grid(row=row_SPIO+16, column=2, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][1][4], variable=var_GPIO_1__GPA4).grid(row=row_SPIO+17, column=2, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][1][3], variable=var_GPIO_1__GPA3).grid(row=row_SPIO+18, column=2, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][1][2], variable=var_GPIO_1__GPA2).grid(row=row_SPIO+19, column=2, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][1][1], variable=var_GPIO_1__GPA1).grid(row=row_SPIO+20, column=2, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][1][0], variable=var_GPIO_1__GPA0).grid(row=row_SPIO+21, column=2, sticky=W) 
	
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][2][7], variable=var_GPIO_2__GPB7).grid(row=row_SPIO+ 6, column=3, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][2][6], variable=var_GPIO_2__GPB6).grid(row=row_SPIO+ 7, column=3, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][2][5], variable=var_GPIO_2__GPB5).grid(row=row_SPIO+ 8, column=3, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][2][4], variable=var_GPIO_2__GPB4).grid(row=row_SPIO+ 9, column=3, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][2][3], variable=var_GPIO_2__GPB3).grid(row=row_SPIO+10, column=3, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][2][2], variable=var_GPIO_2__GPB2).grid(row=row_SPIO+11, column=3, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][2][1], variable=var_GPIO_2__GPB1).grid(row=row_SPIO+12, column=3, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][2][0], variable=var_GPIO_2__GPB0).grid(row=row_SPIO+13, column=3, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][2][7], variable=var_GPIO_2__GPA7).grid(row=row_SPIO+14, column=3, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][2][6], variable=var_GPIO_2__GPA6).grid(row=row_SPIO+15, column=3, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][2][5], variable=var_GPIO_2__GPA5).grid(row=row_SPIO+16, column=3, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][2][4], variable=var_GPIO_2__GPA4).grid(row=row_SPIO+17, column=3, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][2][3], variable=var_GPIO_2__GPA3).grid(row=row_SPIO+18, column=3, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][2][2], variable=var_GPIO_2__GPA2).grid(row=row_SPIO+19, column=3, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][2][1], variable=var_GPIO_2__GPA1).grid(row=row_SPIO+20, column=3, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][2][0], variable=var_GPIO_2__GPA0).grid(row=row_SPIO+21, column=3, sticky=W) 
	
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][3][7], variable=var_GPIO_3__GPB7).grid(row=row_SPIO+ 6, column=4, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][3][6], variable=var_GPIO_3__GPB6).grid(row=row_SPIO+ 7, column=4, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][3][5], variable=var_GPIO_3__GPB5).grid(row=row_SPIO+ 8, column=4, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][3][4], variable=var_GPIO_3__GPB4).grid(row=row_SPIO+ 9, column=4, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][3][3], variable=var_GPIO_3__GPB3).grid(row=row_SPIO+10, column=4, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][3][2], variable=var_GPIO_3__GPB2).grid(row=row_SPIO+11, column=4, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][3][1], variable=var_GPIO_3__GPB1).grid(row=row_SPIO+12, column=4, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][3][0], variable=var_GPIO_3__GPB0).grid(row=row_SPIO+13, column=4, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][3][7], variable=var_GPIO_3__GPA7).grid(row=row_SPIO+14, column=4, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][3][6], variable=var_GPIO_3__GPA6).grid(row=row_SPIO+15, column=4, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][3][5], variable=var_GPIO_3__GPA5).grid(row=row_SPIO+16, column=4, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][3][4], variable=var_GPIO_3__GPA4).grid(row=row_SPIO+17, column=4, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][3][3], variable=var_GPIO_3__GPA3).grid(row=row_SPIO+18, column=4, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][3][2], variable=var_GPIO_3__GPA2).grid(row=row_SPIO+19, column=4, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][3][1], variable=var_GPIO_3__GPA1).grid(row=row_SPIO+20, column=4, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][3][0], variable=var_GPIO_3__GPA0).grid(row=row_SPIO+21, column=4, sticky=W) 
	
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][4][7], variable=var_GPIO_4__GPB7).grid(row=row_SPIO+ 6, column=5, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][4][6], variable=var_GPIO_4__GPB6).grid(row=row_SPIO+ 7, column=5, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][4][5], variable=var_GPIO_4__GPB5).grid(row=row_SPIO+ 8, column=5, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][4][4], variable=var_GPIO_4__GPB4).grid(row=row_SPIO+ 9, column=5, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][4][3], variable=var_GPIO_4__GPB3).grid(row=row_SPIO+10, column=5, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][4][2], variable=var_GPIO_4__GPB2).grid(row=row_SPIO+11, column=5, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][4][1], variable=var_GPIO_4__GPB1).grid(row=row_SPIO+12, column=5, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][4][0], variable=var_GPIO_4__GPB0).grid(row=row_SPIO+13, column=5, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][4][7], variable=var_GPIO_4__GPA7).grid(row=row_SPIO+14, column=5, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][4][6], variable=var_GPIO_4__GPA6).grid(row=row_SPIO+15, column=5, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][4][5], variable=var_GPIO_4__GPA5).grid(row=row_SPIO+16, column=5, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][4][4], variable=var_GPIO_4__GPA4).grid(row=row_SPIO+17, column=5, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][4][3], variable=var_GPIO_4__GPA3).grid(row=row_SPIO+18, column=5, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][4][2], variable=var_GPIO_4__GPA2).grid(row=row_SPIO+19, column=5, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][4][1], variable=var_GPIO_4__GPA1).grid(row=row_SPIO+20, column=5, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][4][0], variable=var_GPIO_4__GPA0).grid(row=row_SPIO+21, column=5, sticky=W) 
	
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][5][7], variable=var_GPIO_5__GPB7).grid(row=row_SPIO+ 6, column=6, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][5][6], variable=var_GPIO_5__GPB6).grid(row=row_SPIO+ 7, column=6, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][5][5], variable=var_GPIO_5__GPB5).grid(row=row_SPIO+ 8, column=6, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][5][4], variable=var_GPIO_5__GPB4).grid(row=row_SPIO+ 9, column=6, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][5][3], variable=var_GPIO_5__GPB3).grid(row=row_SPIO+10, column=6, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][5][2], variable=var_GPIO_5__GPB2).grid(row=row_SPIO+11, column=6, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][5][1], variable=var_GPIO_5__GPB1).grid(row=row_SPIO+12, column=6, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][5][0], variable=var_GPIO_5__GPB0).grid(row=row_SPIO+13, column=6, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][5][7], variable=var_GPIO_5__GPA7).grid(row=row_SPIO+14, column=6, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][5][6], variable=var_GPIO_5__GPA6).grid(row=row_SPIO+15, column=6, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][5][5], variable=var_GPIO_5__GPA5).grid(row=row_SPIO+16, column=6, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][5][4], variable=var_GPIO_5__GPA4).grid(row=row_SPIO+17, column=6, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][5][3], variable=var_GPIO_5__GPA3).grid(row=row_SPIO+18, column=6, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][5][2], variable=var_GPIO_5__GPA2).grid(row=row_SPIO+19, column=6, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][5][1], variable=var_GPIO_5__GPA1).grid(row=row_SPIO+20, column=6, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][5][0], variable=var_GPIO_5__GPA0).grid(row=row_SPIO+21, column=6, sticky=W) 
	
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][6][7], variable=var_GPIO_6__GPB7).grid(row=row_SPIO+ 6, column=7, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][6][6], variable=var_GPIO_6__GPB6).grid(row=row_SPIO+ 7, column=7, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][6][5], variable=var_GPIO_6__GPB5).grid(row=row_SPIO+ 8, column=7, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][6][4], variable=var_GPIO_6__GPB4).grid(row=row_SPIO+ 9, column=7, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][6][3], variable=var_GPIO_6__GPB3).grid(row=row_SPIO+10, column=7, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][6][2], variable=var_GPIO_6__GPB2).grid(row=row_SPIO+11, column=7, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][6][1], variable=var_GPIO_6__GPB1).grid(row=row_SPIO+12, column=7, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][6][0], variable=var_GPIO_6__GPB0).grid(row=row_SPIO+13, column=7, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][6][7], variable=var_GPIO_6__GPA7).grid(row=row_SPIO+14, column=7, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][6][6], variable=var_GPIO_6__GPA6).grid(row=row_SPIO+15, column=7, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][6][5], variable=var_GPIO_6__GPA5).grid(row=row_SPIO+16, column=7, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][6][4], variable=var_GPIO_6__GPA4).grid(row=row_SPIO+17, column=7, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][6][3], variable=var_GPIO_6__GPA3).grid(row=row_SPIO+18, column=7, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][6][2], variable=var_GPIO_6__GPA2).grid(row=row_SPIO+19, column=7, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][6][1], variable=var_GPIO_6__GPA1).grid(row=row_SPIO+20, column=7, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][6][0], variable=var_GPIO_6__GPA0).grid(row=row_SPIO+21, column=7, sticky=W) 
	
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][7][7], variable=var_GPIO_7__GPB7).grid(row=row_SPIO+ 6, column=8, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][7][6], variable=var_GPIO_7__GPB6).grid(row=row_SPIO+ 7, column=8, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][7][5], variable=var_GPIO_7__GPB5).grid(row=row_SPIO+ 8, column=8, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][7][4], variable=var_GPIO_7__GPB4).grid(row=row_SPIO+ 9, column=8, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][7][3], variable=var_GPIO_7__GPB3).grid(row=row_SPIO+10, column=8, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][7][2], variable=var_GPIO_7__GPB2).grid(row=row_SPIO+11, column=8, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][7][1], variable=var_GPIO_7__GPB1).grid(row=row_SPIO+12, column=8, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_B'][7][0], variable=var_GPIO_7__GPB0).grid(row=row_SPIO+13, column=8, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][7][7], variable=var_GPIO_7__GPA7).grid(row=row_SPIO+14, column=8, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][7][6], variable=var_GPIO_7__GPA6).grid(row=row_SPIO+15, column=8, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][7][5], variable=var_GPIO_7__GPA5).grid(row=row_SPIO+16, column=8, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][7][4], variable=var_GPIO_7__GPA4).grid(row=row_SPIO+17, column=8, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][7][3], variable=var_GPIO_7__GPA3).grid(row=row_SPIO+18, column=8, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][7][2], variable=var_GPIO_7__GPA2).grid(row=row_SPIO+19, column=8, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][7][1], variable=var_GPIO_7__GPA1).grid(row=row_SPIO+20, column=8, sticky=W) 
	Checkbutton(sub_spio, text=SPIO_CONF['BITNAME_A'][7][0], variable=var_GPIO_7__GPA0).grid(row=row_SPIO+21, column=8, sticky=W) 
	
	bb_spio_bb_update      = Button(sub_spio, text='read ALL registers', command=spio_bb_update) .grid(row=row_SPIO+31, column=1, columnspan = 8, sticky=W+E, pady=4)
	#
	bb_spio_bb_update_hwa0 = Button(sub_spio, text='update hwa0 ', command=spio_bb_update_hwa0) .grid(row=row_SPIO+32, column=1, columnspan = 1, sticky=W+E, pady=4)
	bb_spio_bb_update_hwa1 = Button(sub_spio, text='update hwa1 ', command=spio_bb_update_hwa1) .grid(row=row_SPIO+32, column=2, columnspan = 1, sticky=W+E, pady=4)
	bb_spio_bb_update_hwa2 = Button(sub_spio, text='update hwa2 ', command=spio_bb_update_hwa2) .grid(row=row_SPIO+32, column=3, columnspan = 1, sticky=W+E, pady=4)
	bb_spio_bb_update_hwa3 = Button(sub_spio, text='update hwa3 ', command=spio_bb_update_hwa3) .grid(row=row_SPIO+32, column=4, columnspan = 1, sticky=W+E, pady=4)
	bb_spio_bb_update_hwa4 = Button(sub_spio, text='update hwa4 ', command=spio_bb_update_hwa4) .grid(row=row_SPIO+32, column=5, columnspan = 1, sticky=W+E, pady=4)
	bb_spio_bb_update_hwa5 = Button(sub_spio, text='update hwa5 ', command=spio_bb_update_hwa5) .grid(row=row_SPIO+32, column=6, columnspan = 1, sticky=W+E, pady=4)
	bb_spio_bb_update_hwa6 = Button(sub_spio, text='update hwa6 ', command=spio_bb_update_hwa6) .grid(row=row_SPIO+32, column=7, columnspan = 1, sticky=W+E, pady=4)
	bb_spio_bb_update_hwa7 = Button(sub_spio, text='update hwa7 ', command=spio_bb_update_hwa7) .grid(row=row_SPIO+32, column=8, columnspan = 1, sticky=W+E, pady=4)
	
	return


def tk_win_setup(master, sub_spio, SPIO_CONF):
	
	#### master ####
	
	## title lines
	row_title = 0
	Label(master, text="== CMU-CPU controls : [SPIO] == ").grid(row=row_title, column= 0, sticky=W, columnspan = 4)
	
	## ee for wireout registers 
	Label(master, text=" FPGA_IMG# =").grid(row=row_title+1+1, column=0, sticky=E) 
	Label(master, text=" TEST_FLAG =").grid(row=row_title+1+2, column=0, sticky=E) 
	Label(master, text=" MON_XADC  =").grid(row=row_title+1+4, column=0, sticky=E) 
	ee_FPGA_IMAGE_ID_                 .grid(row=row_title+1+1, column=1)
	ee_TEST_FLAG_____                 .grid(row=row_title+1+2, column=1)
	ee_MON_XADC______                 .grid(row=row_title+1+4, column=1)
	#
	Label(master, text=" temp[C]   =").grid(row=row_title+1+4, column=2, sticky=E)
	ee_temp_fpga_____                 .grid(row=row_title+1+4, column=3)
	
	
	## update button for wireout registers
	Button(master, text='update board status', command=master_bb_update_board_status_wo) .grid(row=10,  sticky=W+E, pady=4, columnspan = 8)
	
	
	## buttons common 
	# quit
	bb_quit = Button(master, text='Quit', command=master.quit) .grid(row=100,  sticky=W+E, pady=4, columnspan = 8)


	##################
	
	#### sub_spio ####
	tk_win_setup__spio(sub_spio, 1, SPIO_CONF)
	
	
	
	return


#### window locations and sizes ####
master  .geometry('710x190+0+0')
sub_spio.geometry('1210x570+0+220')


#### call windows setup ####
tk_win_setup(master, sub_spio, SPIO_CONF)

## update status 
master_bb_update_board_status_wo()
spio_bb_update()

#### main loop for gui : until quit() ####
mainloop() 

#############################################


##  #######
##  # TODO: DWAVE
##  
##  ####
##  # dwave_enable 
##  ret = cmu.cmu_dwave_enable(dev, EP_ADRS)
##  print(ret)
##  ####
##  
##  ####
##  # read dwave base freq
##  ret = cmu.cmu_dwave_read_base_freq(dev, EP_ADRS)
##  print(ret)
##  ####
##  
##  ####
##  # dwave_wr_cnt_period
##  #   dwave_base  80MHz/40000=2000Hz=2kHz
##  #   dwave_base 160MHz/40000=4000Hz=4kHz
##  #
##  ## 10MHz
##  #CNT_PERIOD = 8
##  #CNT_DIFF   = 6
##  ## 5MHz
##  #CNT_PERIOD = 16 
##  #CNT_DIFF   = 12
##  ## 4MHz
##  #CNT_PERIOD = 20 
##  #CNT_DIFF   = 15
##  ## 3.3MHz
##  #CNT_PERIOD = 24
##  #CNT_DIFF   = 28
##  ## 2MHz
##  #CNT_PERIOD = 40 
##  #CNT_DIFF   = 30
##  ## 1MHz
##  #CNT_PERIOD = 80 
##  #CNT_DIFF   = 60
##  ## 800kHz
##  #CNT_PERIOD = 100 
##  #CNT_DIFF   =  75
##  ## 625kHz # 80000000Hz/625kHz=128
##  #CNT_PERIOD =  128
##  #CNT_DIFF   =   96
##  ## 400kHz@80MHz or 800kHz@160MHz
##  #CNT_PERIOD = 200 
##  #CNT_DIFF   = 150
##  ## 200kHz@80MHz or 400kHz@160MHz
##  CNT_PERIOD = 400 
##  CNT_DIFF   = 300
##  ## 100kHz
##  #CNT_PERIOD = 800 
##  #CNT_DIFF   = 600
##  ## 50kHz@80MHz or 100kHz@160MHz
##  #CNT_PERIOD = 1600
##  #CNT_DIFF   = 1200
##  ## 10kHz
##  #CNT_PERIOD = 8000
##  #CNT_DIFF   = 6000
##  ## 4kHz
##  #CNT_PERIOD = 20000
##  #CNT_DIFF   = 15000
##  #  
##  ret = cmu.cmu_dwave_wr_cnt_period(dev, EP_ADRS, CNT_PERIOD)
##  print(ret)
##  #
##  # dwave_wr_cnt_diff
##  ret = cmu.cmu_dwave_wr_cnt_diff(dev, EP_ADRS, CNT_DIFF)
##  print(ret)
##  ####
##  
##  ####
##  ret = cmu.cmu_dwave_wr_output_dis__enable_all(dev, EP_ADRS)
##  #ret = cmu.cmu_dwave_wr_output_dis__enable_path_i_only()
##  #ret = cmu.cmu_dwave_wr_output_dis__enable_path_q_only()
##  print(ret)
##  ####
##  
##  ####
##  # set dwave parameters
##  ret = cmu.cmu_dwave_set_para(dev, EP_ADRS)
##  print(ret)
##  ####
##  
##  ####
##  # dwave_pulse_on
##  ret = cmu.cmu_dwave_pulse_on(dev, EP_ADRS)
##  print(ret)
##  ####
##  
##  ####
##  # wait for pulse stability
##  cmu.sleep(1)


#############################################


##  ########
##  # TODO: ADC
##  
##  ####
##  # cmu_adc_enable
##  ret = cmu.cmu_adc_enable(dev, EP_ADRS)
##  print(ret)
##  ####
##  
##  ####
##  # cmu_adc_reset
##  ret = cmu.cmu_adc_reset(dev, EP_ADRS)
##  print(ret)
##  ####
##  
##  ####
##  # cmu_adc_set_para
##  #
##  ADC_BASE_FREQ       =210000000
##  #ADC_BASE_FREQ       =125000000
##  #
##  FS_TARGET          = 15000000   # 
##  #FS_TARGET          =10416666   # 125000000/   12=10416666 // 10.4Msps ###
##  #FS_TARGET          = 9615384   # 125000000/   13= 9615384 //  9.6Msps
##  #FS_TARGET          = 3906250   # 125000000/   32= 3906250 //  3.9Msps ###
##  #FS_TARGET          = 1893939   # 125000000/  66= 1893939 //  1.9Msps
##  #FS_TARGET          =  961538   # 125000000/  130=  961538 //  960ksps ###
##  #FS_TARGET          =   96153   # 125000000/ 1300=   96153 //   96ksps
##  #FS_TARGET          =    9615   # 125000000/13000=    9615 //  9615sps
##  #
##  #FS_TARGET           =8333333
##  #FS_TARGET           =7400000
##  #FS_TARGET           = 992064    # 125000000/ 126= 992064
##  #FS_TARGET           =  98971    # 125000000/1263=  98971
##  ADC_NUM_SAMPLES     =131072
##  ADC_INPUT_DELAY_TAP =15
##  PIN_TEST_FRC_HIGH   =0
##  PIN_DLLN_FRC_LOW    =0
##  PTTN_CNT_UP_EN      =0
##  #
##  ret = cmu.cmu_adc_set_para (dev, EP_ADRS,
##  	ADC_BASE_FREQ       = ADC_BASE_FREQ      ,
##  	FS_TARGET           = FS_TARGET          ,
##  	ADC_NUM_SAMPLES     = ADC_NUM_SAMPLES    ,
##  	ADC_INPUT_DELAY_TAP = ADC_INPUT_DELAY_TAP,
##  	PIN_TEST_FRC_HIGH   = PIN_TEST_FRC_HIGH  ,
##  	PIN_DLLN_FRC_LOW    = PIN_DLLN_FRC_LOW   ,
##  	PTTN_CNT_UP_EN      = PTTN_CNT_UP_EN     )
##  print(ret)
##  ####
##  
##  ####
##  # cmu_adc_init
##  ret = cmu.cmu_adc_init(dev, EP_ADRS)
##  print(ret)
##  ####
##  
##  ####
##  # cmu_check_adc_test_pattern
##  ret = cmu.cmu_check_adc_test_pattern(dev, EP_ADRS)
##  print(ret)
##  ####
##  
##  ####
##  # cmu_adc_is_fifo_empty
##  ret = cmu.cmu_adc_is_fifo_empty(dev, EP_ADRS)
##  print(ret)
##  ####
##  
##  ####
##  # cmu_adc_update
##  ret = cmu.cmu_adc_update(dev, EP_ADRS)
##  print(ret)
##  ####
##  
##  ####
##  # cmu_adc_load_from_fifo
##  adc_list = cmu.cmu_adc_load_from_fifo(dev, EP_ADRS)
##  ####
##  
##  ####
##  # adc_display_data_list_int
##  #ret = cmu.cmu_adc_display_data_list_int (adc_list,FS_TARGET)
##  #
##  DUMP_FILENAME='DUMP'
##  num_smp_zoom = 500
##  ret = cmu.cmu_adc_display_data_list_int__zoom (adc_list,DUMP_FILENAME,FS_TARGET,num_smp_zoom)
##  print(ret)
##  ####



########

####
#input('> See DWAVE on. Enter...')
#input('> press Enter ...')
####


####
# work above!
####



###############################
## finish test
###############################



##  ########
##  # TODO: ADC off
##  
##  ####
##  # cmu_adc_disable
##  ret = cmu.cmu_adc_disable(dev, EP_ADRS)
##  print(ret)
##  ####
##  
##  
##  ########
##  # TODO: DWAVE off
##  
##  ####
##  # TODO: dwave_pulse_off
##  ret = cmu.cmu_dwave_pulse_off(dev, EP_ADRS)
##  print(ret)
##  ####
##  
##  ####
##  # TODO: dwave_disable
##  ret = cmu.cmu_dwave_disable(dev, EP_ADRS)
##  print(ret)
##  ####


########
# TODO: SPIO off

# enable
print('\n>> {}'.format('SPIO  disable'))
#dev.SetWireInValue(EP_ADRS['SPIO_WI'], 0x00000000, 0xFFFFFFFF) # (ep,val,mask)
#dev.UpdateWireIns()	
cmu.spio_dis(dev, EP_ADRS)

########
# SPO off

####
ret = cmu.cmu_spo_bit__amp_pwr(dev, EP_ADRS,'OFF')
print(ret)
####

####
ret = cmu.cmu_spo_bit__leds(dev, EP_ADRS,0x00)
print(ret)
####

####
ret = cmu.cmu_spo_disable(dev, EP_ADRS)
print(ret)
####


########
# TEST off

####
## test counter off
##$$ret = cmu.cmu_test_counter('OFF')
ret = cmu.cmu_test_counter(dev, EP_ADRS,'OFF')
print(ret)
####

####
## test counter reset
##$$ret = cmu.cmu_test_counter('RESET')
ret = cmu.cmu_test_counter(dev, EP_ADRS,'RESET')
print(ret)
####


########
## close socket
dev.Close()
print('>>> device closed.')


##############################################







