## cmu_cpu__lib_conf.py : library configuration ... end-point addresses  for CMU-CPU-TEST-F5500


## end-point addresses 
EP_ADRS_CONFIG = {
	#'board_name'         : 'CMU-CPU-TEST-F5500',
	#'board_name'         : 'CMU-CPU-F5500',
	'board_name'         : 'CMU-CPU-F5500-REVA', # CMU-CPU-F5500-REVA 19-05-13
	#
	#'ver'                : '0xF9181128',
	#'ver'                : '0xF8181207',
	#'ver'                : '0xF3190305',
	#'ver'                : '0xF3190306',
	#'ver'                : '0xC8190320',
	#'ver'                : '0xED201125', # test LAN
	'ver'                : '0xED201203', # test SPIO
	#
	#'bit_filename'       : 'xem7310__cmu_cpu__top__F9_18_1128.bit', # for DWAVE control 
	#'bit_filename'       : 'xem7310__cmu_cpu__top__F8_18_1207.bit', # for ADC control 
	#'bit_filename'       : 'xem7310__cmu_cpu__top__F3_19_0305.bit', # for LAN control
	#'bit_filename'       : 'xem7310__cmu_cpu__top__F3_19_0306.bit', # for LAN end-point control
	#'bit_filename'       : 'xem7310__cmu_cpu__top.bit', #CMU-CPU-TEST-F5500 final release
	'bit_filename'        : 'download.bit',
	#
	# wire-in
	'SW_BUILD_ID'        : 0x00,
	'TEST_CON'           : 0x01,
	'TEST_CC_DIN'        : 0x02,
	'BRD_CON'            : 0x03, ##$$ 20201119
	'DAC_TEST_IN'        : 0x04,
	'DWAVE_DIN_BY_TRIG'  : 0x05,
	'DWAVE_CON'          : 0x06,
	'SPO_CON'            : 0x07,
	'SPO_DIN_B0_L'       : 0x08,
	'SPO_DIN_B0_H'       : 0x09,
	'SPO_DIN_B1_L'       : 0x0A,
	'SPO_DIN_B1_H'       : 0x0B,
	'SPO_DIN_B2_L'       : 0x0C,
	'SPO_DIN_B2_H'       : 0x0D,
	'SPO_DIN_B3_L'       : 0x0E,
	'SPO_DIN_B3_H'       : 0x0F,
	'DAC_A2A3_CON'       : 0x10,
	'DAC_BIAS_CON'       : 0x11,
	'MEM_FDAT_WI'        : 0x12, ##$$ 20201125
	'MEM_WI'             : 0x13, ##$$ 20201125
	'DAC_A2A3_DIN21'     : 0x14, 
	'DAC_A2A3_DIN43'     : 0x15, 
	'DAC_BIAS_DIN21'     : 0x16, 
	'DAC_BIAS_DIN43'     : 0x17,
	'ADC_HS_WI'          : 0x18,
	'MCS_SETUP_WI'       : 0x19, ##$$ 20201125
	'SPIO_FDAT_WI'       : 0x1A, ##$$ SPIO_FDAT_WI
	'SPIO_WI'            : 0x1B, ##$$ SPIO_WI
	'wi1C'               : 0x1C, 
	'ADC_HS_UPD_SMP'     : 0x1D,
	'ADC_HS_SMP_PRD'     : 0x1E,
	'ADC_HS_DLY_TAP_OPT' : 0x1F,
	# wire-out
	'FPGA_IMAGE_ID'      : 0x20,
	'TEST_OUT'           : 0x21,
	'TEST_CC_MON'        : 0x22,
	'DWAVE_BASE_FREQ'    : 0x23, 
	'DAC_TEST_OUT'       : 0x24, 
	'DWAVE_DOUT_BY_TRIG' : 0x25, 
	'DWAVE_FLAG'         : 0x26, 
	'SPO_FLAG'           : 0x27, 
	'SPO_MON_B0_L'       : 0x28,
	'SPO_MON_B0_H'       : 0x29,
	'SPO_MON_B1_L'       : 0x2A,
	'SPO_MON_B1_H'       : 0x2B,
	'SPO_MON_B2_L'       : 0x2C,
	'SPO_MON_B2_H'       : 0x2D,
	'SPO_MON_B3_L'       : 0x2E,
	'SPO_MON_B3_H'       : 0x2F,
	'DAC_A2A3_FLAG'      : 0x30, 
	'DAC_BIAS_FLAG'      : 0x31,
	'DAC_TEST_RB1'       : 0x32,
	'DAC_TEST_RB2'       : 0x33,
	'DAC_A2A3_RB21'      : 0x34, 
	'DAC_A2A3_RB43'      : 0x35, 
	'DAC_BIAS_RB21'      : 0x36,
	'DAC_BIAS_RB43'      : 0x37,
	'ADC_HS_WO'          : 0x38,
	'ADC_BASE_FREQ'	     : 0x39,
	'XADC_TEMP'          : 0x3A,
	'TIME_STAMP'         : 0x3A, ##$$ 20201120 alias for time-stamp
	'XADC_VOLT'          : 0x3B,
	'SPIO_WO'            : 0x3B, ##$$ SPIO_WO shared with XADC_VOLT
	'ADC_HS_DOUT0'       : 0x3C,
	'ADC_HS_DOUT1'       : 0x3D,
	'ADC_HS_DOUT2'       : 0x3E,
	'ADC_HS_DOUT3'       : 0x3F,
	#
	# trig-in
	'TEST_TI'            : 0x40,
	'TEST_TI_HS'         : 0x41, ##$$ 2019/3/5, 2018/12/8 
	'DWAVE_TI'           : 0x46,
	'DAC_BIAS_TI'        : 0x50,
	'DAC_A2A3_TI'        : 0x51, 
	'MEM_TI'             : 0x53, ##$$ 20201125
	'ADC_HS_TI'          : 0x58,
	'SPIO_TI'            : 0x5B, ##$$ SPIO_TI
	#
	# trig-out
	'TEST_TO'            : 0x60,
	'DAC_BIAS_TO'        : 0x70, 
	'DAC_A2A3_TO'        : 0x71,
	'MEM_TO'             : 0x73, ##$$ 20201125
	'ADC_HS_TO'          : 0x78,
	'SPIO_TO'            : 0x7B, ##$$ SPIO_TO 
	#
	# pipe-in
	'MEM_PI'             : 0x93, ##$$ 20201125
	#
	# pipe-out
	'MEM_PO'             : 0xB3, ##$$ 20201125
	'ADC_HS_DOUT0_PO'    : 0xBC,
	'ADC_HS_DOUT1_PO'    : 0xBD,
	'ADC_HS_DOUT2_PO'    : 0xBE,
	'ADC_HS_DOUT3_PO'    : 0xBF,
	#
	'end' : 'end'
}

## note that DAC_BIAS (AD5754) is not practically used.
#  we may re-use end-points of DAC_BIAS for new SPIO (MCP23S17)

## note that DAC_A2A3 (AD5754) shares pins with 6-pin connector for SPIO (MCP23S17)
#   CC8 : MOSI // reserved for MCP23S17
#   CC9 : SCLK // reserved
#   CC10: SCSB // reserved
#   CC12: MISO // reserved
#   ...
#   DAC_A2A3_MOSI // CC8
#   DAC_A2A3_SCLK // CC9
#   DAC_A2A3_SYNB // CC10
#   DAC_A2A3_MISO // CC12
#  we had better share end-point of DAC_A2A3 for new SPIO (MCP23S17)

## SPIO configuration 
# for MCP23S17
# IODIR output(0) or input(1)
# OLAT  initial output value 
SPIO_CONFIG_ref = {
	'BITNAME_A' : [ ['HA0_B0_A', 'HA0_B1_A', 'HA0_B2_A', 'HA0_B3_A', 'HA0_B4_A', 'HA0_B5_A', 'HA0_B6_A', 'HA0_B7_A'], 
					['HA1_B0_A', 'HA1_B1_A', 'HA1_B2_A', 'HA1_B3_A', 'HA1_B4_A', 'HA1_B5_A', 'HA1_B6_A', 'HA1_B7_A'], 
					['HA2_B0_A', 'HA2_B1_A', 'HA2_B2_A', 'HA2_B3_A', 'HA2_B4_A', 'HA2_B5_A', 'HA2_B6_A', 'HA2_B7_A'], 
					['HA3_B0_A', 'HA3_B1_A', 'HA3_B2_A', 'HA3_B3_A', 'HA3_B4_A', 'HA3_B5_A', 'HA3_B6_A', 'HA3_B7_A'],  
					['HA4_B0_A', 'HA4_B1_A', 'HA4_B2_A', 'HA4_B3_A', 'HA4_B4_A', 'HA4_B5_A', 'HA4_B6_A', 'HA4_B7_A'], 
					['HA5_B0_A', 'HA5_B1_A', 'HA5_B2_A', 'HA5_B3_A', 'HA5_B4_A', 'HA5_B5_A', 'HA5_B6_A', 'HA5_B7_A'], 
					['HA6_B0_A', 'HA6_B1_A', 'HA6_B2_A', 'HA6_B3_A', 'HA6_B4_A', 'HA6_B5_A', 'HA6_B6_A', 'HA6_B7_A'], 
					['HA7_B0_A', 'HA7_B1_A', 'HA7_B2_A', 'HA7_B3_A', 'HA7_B4_A', 'HA7_B5_A', 'HA7_B6_A', 'HA7_B7_A']],
	'BITNAME_B' : [ ['HA0_B0_B', 'HA0_B1_B', 'HA0_B2_B', 'HA0_B3_B', 'HA0_B4_B', 'HA0_B5_B', 'HA0_B6_B', 'HA0_B7_B'], 
					['HA1_B0_B', 'HA1_B1_B', 'HA1_B2_B', 'HA1_B3_B', 'HA1_B4_B', 'HA1_B5_B', 'HA1_B6_B', 'HA1_B7_B'], 
					['HA2_B0_B', 'HA2_B1_B', 'HA2_B2_B', 'HA2_B3_B', 'HA2_B4_B', 'HA2_B5_B', 'HA2_B6_B', 'HA2_B7_B'], 
					['HA3_B0_B', 'HA3_B1_B', 'HA3_B2_B', 'HA3_B3_B', 'HA3_B4_B', 'HA3_B5_B', 'HA3_B6_B', 'HA3_B7_B'],  
					['HA4_B0_B', 'HA4_B1_B', 'HA4_B2_B', 'HA4_B3_B', 'HA4_B4_B', 'HA4_B5_B', 'HA4_B6_B', 'HA4_B7_B'], 
					['HA5_B0_B', 'HA5_B1_B', 'HA5_B2_B', 'HA5_B3_B', 'HA5_B4_B', 'HA5_B5_B', 'HA5_B6_B', 'HA5_B7_B'], 
					['HA6_B0_B', 'HA6_B1_B', 'HA6_B2_B', 'HA6_B3_B', 'HA6_B4_B', 'HA6_B5_B', 'HA6_B6_B', 'HA6_B7_B'], 
					['HA7_B0_B', 'HA7_B1_B', 'HA7_B2_B', 'HA7_B3_B', 'HA7_B4_B', 'HA7_B5_B', 'HA7_B6_B', 'HA7_B7_B']],
	#
	#             hw0   hw1   hw2   hw3    hw4   hw5   hw6   hw7 
	'IODIR_A' : [0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF],
	'IODIR_B' : [0xFF, 0xFF, 0xFF, 0xFF,  0xFF, 0xFF, 0xFF, 0xFF],
	'OLAT_A'  : [0x00, 0x00, 0x00, 0x00,  0x00, 0x00, 0x00, 0x00],
	'OLAT_B'  : [0x00, 0x00, 0x00, 0x00,  0x00, 0x00, 0x00, 0x00]
	
}

# note on cmu_signal_dc_20201119.pdf
# CHECK_LED : hw2 gpb0 
SPIO_CONFIG = {
	'BITNAME_A' : [ ['HA0_B0_A       ', 'HA0_B1_A       ', 'HA0_B2_A       ', 'HA0_B3_A       ', 'HA0_B4_A       ', 'HA0_B5_A       ', 'HA0_B6_A       ', 'HA0_B7_A       '], 
					['HA1_B0_A_DACD00', 'HA1_B1_A_DACD01', 'HA1_B2_A_DACD02', 'HA1_B3_A_DACD03', 'HA1_B4_A_DACD04', 'HA1_B5_A_DACD05', 'HA1_B6_A_DACD06', 'HA1_B7_A_DACD07'], 
					['HA2_B0_A       ', 'HA2_B1_A       ', 'HA2_B2_A       ', 'HA2_B3_A       ', 'HA2_B4_A       ', 'HA2_B5_A       ', 'HA2_B6_A       ', 'HA2_B7_A       '], 
					['HA3_B0_A_T_0_1 ', 'HA3_B1_A_T_0_2 ', 'HA3_B2_A_T_0_4 ', 'HA3_B3_A_T_0_8 ', 'HA3_B4_A_T_0_16', 'HA3_B5_A_T_0_32', 'HA3_B6_A_F6K#  ', 'HA3_B7_A_LPF#  '],  
					['HA4_B0_A       ', 'HA4_B1_A       ', 'HA4_B2_A       ', 'HA4_B3_A       ', 'HA4_B4_A       ', 'HA4_B5_A       ', 'HA4_B6_A       ', 'HA4_B7_A       '], 
					['HA5_B0_A       ', 'HA5_B1_A       ', 'HA5_B2_A       ', 'HA5_B3_A       ', 'HA5_B4_A       ', 'HA5_B5_A       ', 'HA5_B6_A       ', 'HA5_B7_A       '], 
					['HA6_B0_A       ', 'HA6_B1_A       ', 'HA6_B2_A       ', 'HA6_B3_A       ', 'HA6_B4_A       ', 'HA6_B5_A       ', 'HA6_B6_A       ', 'HA6_B7_A       '], 
					['HA7_B0_A       ', 'HA7_B1_A       ', 'HA7_B2_A       ', 'HA7_B3_A       ', 'HA7_B4_A       ', 'HA7_B5_A       ', 'HA7_B6_A       ', 'HA7_B7_A       ']],
	'BITNAME_B' : [ ['HA0_B0_B       ', 'HA0_B1_B       ', 'HA0_B2_B       ', 'HA0_B3_B       ', 'HA0_B4_B       ', 'HA0_B5_B       ', 'HA0_B6_B       ', 'HA0_B7_B       '], 
					['HA1_B0_B_DACD08', 'HA1_B1_B_DACD09', 'HA1_B2_B_DACD10', 'HA1_B3_B_DACD11', 'HA1_B4_B_SPDUP ', 'HA1_B5_B_POL   ', 'HA1_B6_B_MODE2 ', 'HA1_B7_B_MODE1 '], 
					['HA2_B0_B_CHKLED', 'HA2_B1_B       ', 'HA2_B2_B       ', 'HA2_B3_B       ', 'HA2_B4_B       ', 'HA2_B5_B       ', 'HA2_B6_B       ', 'HA2_B7_B_EXBIAS'], 
					['HA3_B0_B_T90_1 ', 'HA3_B1_B_T90_2 ', 'HA3_B2_B_T90_4 ', 'HA3_B3_B_T90_8 ', 'HA3_B4_B_T90_16', 'HA3_B5_B_T90_32', 'HA3_B6_B_F60K# ', 'HA3_B7_B_F600K#'],  
					['HA4_B0_B       ', 'HA4_B1_B       ', 'HA4_B2_B       ', 'HA4_B3_B       ', 'HA4_B4_B       ', 'HA4_B5_B       ', 'HA4_B6_B       ', 'HA4_B7_B       '], 
					['HA5_B0_B       ', 'HA5_B1_B       ', 'HA5_B2_B       ', 'HA5_B3_B       ', 'HA5_B4_B       ', 'HA5_B5_B       ', 'HA5_B6_B       ', 'HA5_B7_B       '], 
					['HA6_B0_B       ', 'HA6_B1_B       ', 'HA6_B2_B       ', 'HA6_B3_B       ', 'HA6_B4_B       ', 'HA6_B5_B       ', 'HA6_B6_B       ', 'HA6_B7_B       '], 
					['HA7_B0_B       ', 'HA7_B1_B       ', 'HA7_B2_B       ', 'HA7_B3_B       ', 'HA7_B4_B       ', 'HA7_B5_B       ', 'HA7_B6_B       ', 'HA7_B7_B       ']],
	#
	#
	#             hw0   hw1   hw2   hw3    hw4   hw5   hw6   hw7 
	'IODIR_A' : [0xFF, 0x00, 0xFF, 0x00,  0xFF, 0xFF, 0xFF, 0xFF],
	'IODIR_B' : [0xFF, 0x00, 0x7E, 0x00,  0xFF, 0xFF, 0xFF, 0xFF],
	'OLAT_A'  : [0x00, 0x00, 0x00, 0xC0,  0x00, 0x00, 0x00, 0x00],
	'OLAT_B'  : [0x00, 0x00, 0x01, 0xC0,  0x00, 0x00, 0x00, 0x00]
}


###########################################################################
# test lib

def _test():
	print(EP_ADRS_CONFIG)
	#
	print(SPIO_CONFIG)
	#
	print(SPIO_CONFIG['BITNAME_A'])
	print(SPIO_CONFIG['BITNAME_B'])
	#
	print(SPIO_CONFIG['IODIR_A'])
	print(SPIO_CONFIG['IODIR_A'][0x0]) # hardware adrs 0x0
	print(SPIO_CONFIG['IODIR_A'][0x1]) # hardware adrs 0x1
	print(SPIO_CONFIG['IODIR_A'][0x2]) # hardware adrs 0x2
	print(SPIO_CONFIG['IODIR_A'][0x3]) # hardware adrs 0x3
	print(SPIO_CONFIG['IODIR_A'][0x4]) # hardware adrs 0x4
	print(SPIO_CONFIG['IODIR_A'][0x5]) # hardware adrs 0x5
	print(SPIO_CONFIG['IODIR_A'][0x6]) # hardware adrs 0x6
	print(SPIO_CONFIG['IODIR_A'][0x7]) # hardware adrs 0x7
	#
	#print('NAME {} : IO {} : LAT {}'.format(
	#	SPIO_CONFIG['BITNAME_A'][0x0][0],
	#	(SPIO_CONFIG['IODIR_A'][0x0]>>0)&0x01,
	#	(SPIO_CONFIG['OLAT_A' ][0x0]>>0)&0x01)
	#	)
	#
	for aa in range(0,8): # hardware address
		for bb in range(0,8): # bit location
			#print('{} {}'.format(aa,bb))
			print('NAME {} : IOD {} : LAT {}, NAME {} : IOD {} : LAT {}'.format(
				SPIO_CONFIG['BITNAME_A'][aa] [bb],
				(SPIO_CONFIG[ 'IODIR_A'][aa]>>bb)&0x01,
				(SPIO_CONFIG[  'OLAT_A'][aa]>>bb)&0x01,
				SPIO_CONFIG['BITNAME_B'][aa] [bb],
				(SPIO_CONFIG[ 'IODIR_B'][aa]>>bb)&0x01,
				(SPIO_CONFIG[  'OLAT_B'][aa]>>bb)&0x01)
				)
	#
	return

if __name__ == '__main__':
	if __debug__:
		print('>>>>>> In debug mode ... ')
	_test()
	
