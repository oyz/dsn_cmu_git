#include <stdio.h>
#include <string.h>
//#include "xil_printf.h"
#include "microblaze_sleep.h" // for usleep

//$$ support CMU-CPU
#include "../CMU_CPU_TOP/platform.h" // for init_platform()
#include "../CMU_CPU_TOP/cmu_cpu_config.h" // CMU-CPU board config
#include "../CMU_CPU_TOP/cmu_cpu.h" // for hw_reset

//$$ support W5500
#include "../CMU_CPU_TOP/ioLibrary/Ethernet/W5500/w5500.h" // for w5500 io functions
#include "../CMU_CPU_TOP/ioLibrary/Ethernet/socket.h"	// Just include one header for WIZCHIP

//$$ loopback socket
#include "../CMU_CPU_TOP/ioLibrary/Appmod/Loopback/loopback.h"


/////////////////////////////////////////
// SOCKET NUMBER DEFINION for Examples //
/////////////////////////////////////////
#define SOCK_TCPS        0
//#define SOCK_UDPS        1

////////////////////////////////////////////////
// Shared Buffer Definition                   //
////////////////////////////////////////////////
//$$#define DATA_BUF_SIZE   2048
uint8_t gDATABUF[DATA_BUF_SIZE]; // DATA_BUF_SIZE from loopback.h // -->bss


///////////////////////////////////
// Default Network Configuration //
///////////////////////////////////
wiz_NetInfo gWIZNETINFO = { .mac = {0x00, 0x08, 0xdc,0x00, 0xab, 0xcd},
							.ip = {192, 168, 168, 123},
							.sn = {255,255,255,0},
							.gw = {192, 168, 168, 1},
							.dns = {0,0,0,0},
							.dhcp = NETINFO_STATIC };





//////////////////////////////////
// For example of ioLibrary_BSD //
//////////////////////////////////
void network_init(void);								// Initialize Network information and display it
//int32_t loopback_tcps(uint8_t, uint8_t*, uint16_t);		// Loopback TCP server
//int32_t loopback_udps(uint8_t, uint8_t*, uint16_t);		// Loopback UDP server
//////////////////////////////////

#define MAX_ADC_BUF_SIZE 100 // in u32, OK
//#define MAX_ADC_BUF_SIZE 1024 // in u32, OK
//#define MAX_ADC_BUF_SIZE 2048 // in u32, OK with heap=0x7000
//$$#define MAX_ADC_BUF_SIZE 8192/2 // in u32, OK with heap=0x8000 // 32KB
//#define MAX_ADC_BUF_SIZE 16384 // in u32, NG with heap=0x10000 // 64KB // heap is out

int main(void)
{
   uint8_t tmp;
   int32_t ret = 0;
   //uint8_t memsize[2][8] = {{2,2,2,2,2,2,2,2},{2,2,2,2,2,2,2,2}}; // KB => 16KB
   uint8_t memsize[2][8] = {{8,0,0,0,0,0,0,0},{8,0,0,0,0,0,0,0}}; // KB => 16KB
   //
   //$$ for CMU-CPU
   u32 adrs;
   u32 value;
   u32 mask;
   u32 ii;
   //

   // test max inner memory
   //u32 data_adc[131072]; // 2^17 depth // 2^19 B = 2^9 KB =  512 KB
   //u32 data_adc[8192]; // 2^13 depth // 2^15 B = 2^5 KB =  32 KB
   //u32 data_adc[4096]; // 2^12 depth // 2^14 B = 2^4 KB =  16 KB
   u32 data_adc0[MAX_ADC_BUF_SIZE];
   u32 data_adc1[MAX_ADC_BUF_SIZE];
   for (ii=0;ii<MAX_ADC_BUF_SIZE;ii++) {
	   data_adc0[ii] = 0xA1B2C3D4+ii;
	   data_adc1[ii] = 0x11223344+ii;
   }
   xil_printf(">>> mem heap alloc test: 0x%08X @ 0x%08X \r\n", data_adc0[0], &data_adc0[0]);
   xil_printf(">>> mem heap alloc test: 0x%08X @ 0x%08X \r\n", data_adc1[0], &data_adc1[0]);

   ///////////////////////////////////////////
   // Host dependent peripheral initialized //
   ///////////////////////////////////////////
   init_platform();


   //// FPGA setup
   // test for print on jtag-terminal
   print("> Hello World!! \r\n");
   print(">>> build_info: "__TIME__", " __DATE__ ".\r\n");

   // test master_spi_wz850.v
   xil_printf(">> test mcs_io_bridge.v \r\n");
   xil_printf(">>> mcs_io_bridge_inst0 for LAN spi control \r\n");
   //

   // ADRS_FPGA_IMAGE_ID
   xil_printf(">>> FPGA_IMAGE_ID: \r\n");
   adrs = ADRS_FPGA_IMAGE;
   value = XIomodule_In32 (adrs);
   xil_printf(">done: rd: 0x%08X @ 0x%08X \r\n", value, adrs);
   //
   // ADRS_TEST_REG
   xil_printf(">>> TEST_REG: \r\n");
   adrs = ADRS_TEST_REG;
   value = 0xABCD1234;
   //
   XIomodule_Out32 (adrs, value);
   xil_printf(">done: wr: 0x%08X @ 0x%08X \r\n", value, adrs);
   //
   value = XIomodule_In32 (adrs);
   xil_printf(">done: rd: 0x%08X @ 0x%08X \r\n", value, adrs);
   //

   //// test mcs endpoints
   xil_printf(">>> mcs_io_bridge_inst1 for MCS Endpoints \r\n");
   //

   // ADRS_FPGA_IMAGE_CMU
   xil_printf(">>> ADRS_FPGA_IMAGE_CMU: \r\n");
   adrs = ADRS_FPGA_IMAGE_CMU;
   value = read_mcs_fpga_img_id();
   xil_printf(">done: rd: 0x%08X @ 0x%08X \r\n", value, adrs);
   //
   // ADRS_TEST_REG___CMU
   xil_printf(">>> ADRS_TEST_REG___CMU: \r\n");
   adrs = ADRS_TEST_REG___CMU;
   value = 0xA4A4A4A4;
   //
   write_mcs_test_reg(value);
   xil_printf(">done: wr: 0x%08X @ 0x%08X \r\n", value, adrs);
   //
   value = read_mcs_test_reg();
   xil_printf(">done: rd: 0x%08X @ 0x%08X \r\n", value, adrs);
   //

   //
   xil_printf(">>> disable MCS endpoints : \r\n");
   //adrs = ADRS_PORT_WI_10;
   //value = 0x00000000;
   //XIomodule_Out32 (adrs, value);
   //xil_printf(">done: wr: 0x%08X @ 0x%08X \r\n", value, adrs);
   disable_mcs_ep();

   xil_printf(">>> write SW_BUILD_ID from MCS endpoint : \r\n");
   value = 0x00001234;
   write_mcs_ep_wi(0x00, value, 0xFFFFFFFF);
   xil_printf(">done: wr: 0x%08X \r\n", value);

   xil_printf(">>> read FPGA_IMAGE_ID from MCS endpoint : \r\n");
   //adrs = ADRS_PORT_WO_20_CMU;
   //value = XIomodule_In32 (adrs);
   //xil_printf(">done: rd: 0x%08X @ 0x%08X \r\n", value, adrs);
   value = read_mcs_ep_wo(0x20, 0xFFFFFFFF);
   xil_printf(">done: rd: 0x%08X \r\n", value);

   //
   xil_printf(">>> enable MCS endpoints : \r\n");
   enable_mcs_ep();
   value = is_enabled_mcs_ep();
   xil_printf(">done: wr: 0x%08X \r\n", value);

   xil_printf(">>> write SW_BUILD_ID from MCS endpoint : \r\n");
   value = 0x00001234;
   write_mcs_ep_wi(0x00, value, 0xFFFFFFFF);
   xil_printf(">done: wr: 0x%08X \r\n", value);

   xil_printf(">>> read FPGA_IMAGE_ID from MCS endpoint : \r\n");
   value = read_mcs_ep_wo(0x20, 0xFFFFFFFF);
   xil_printf(">done: rd: 0x%08X \r\n", value);

   xil_printf(">>> write SW_BUILD_ID from MCS endpoint : \r\n");
   value = 0x00000000;
   write_mcs_ep_wi(0x00, value, 0xFFFFFFFF);
   xil_printf(">done: wr: 0x%08X \r\n", value);

   xil_printf(">>> read FPGA_IMAGE_ID from MCS endpoint : \r\n");
   value = read_mcs_ep_wo(0x20, 0xFFFFFFFF);
   xil_printf(">done: rd: 0x%08X \r\n", value);

   //// test mask for MCS endpoints

   // test count :  set autocount2
   xil_printf(">>> test count :  set autocount2 \r\n");
   adrs  = 0x01;
   value = 0x00000004;
   mask  = 0x00000004;
   write_mcs_ep_wi(adrs, value, mask);
   xil_printf(">done: wr: 0x%08X & 0x%08X @ 0x%02X \r\n", value, mask, adrs);

   // test count :  readback autocount2
   xil_printf(">>> test count :  readback autocount2 \r\n");
   value = read_mcs_ep_wi(0x01);
   xil_printf(">done: rd: 0x%08X \r\n", value);

   // test count :  reset autocount2
   xil_printf(">>> test count :  reset autocount2 \r\n");
   adrs  = 0x01;
   value = 0x00000000;
   mask  = 0x00000004;
   write_mcs_ep_wi(adrs, value, mask);
   xil_printf(">done: wr: 0x%08X & 0x%08X @ 0x%02X \r\n", value, mask, adrs);

   // test count :  trig reset count2
   xil_printf(">>> test count :  set autocount2 \r\n");
   adrs  = 0x40;
   value = 0x000000001;
   mask  = 0x000000001;
   write_mcs_ep_ti(adrs, value, mask);
   xil_printf(">done: wr: 0x%08X & 0x%08X @ 0x%02X \r\n", value, mask, adrs);

   xil_printf(">>> wait for a while \r\n");
   sleep(1); //sec

   // test count :  trig up autocount2
   xil_printf(">>> test count :  trig up autocount2 \r\n");
   adrs  = 0x40;
   value = 0x000000002;
   mask  = 0x000000002;
   write_mcs_ep_ti(adrs, value, mask);
   xil_printf(">done: wr: 0x%08X & 0x%08X @ 0x%02X \r\n", value, mask, adrs);

   xil_printf(">>> wait for a while \r\n");
   sleep(1); //sec

   // test count :  trig up autocount2
   xil_printf(">>> test count :  trig up autocount2 \r\n");
   adrs  = 0x40;
   value = 0x000000002;
   mask  = 0x000000002;
   write_mcs_ep_ti(adrs, value, mask);
   xil_printf(">done: wr: 0x%08X & 0x%08X @ 0x%02X \r\n", value, mask, adrs);

   xil_printf(">>> wait for a while \r\n");
   sleep(1); //sec

   // test count :  trig down autocount2
   xil_printf(">>> test count :  trig down autocount2 \r\n");
   //adrs  = 0x40;
   //value = 0x000000004;
   //mask  = 0x000000004;
   //write_mcs_ep_ti(adrs, value, mask);
   //xil_printf(">done: wr: 0x%08X & 0x%08X @ 0x%02X \r\n", value, mask, adrs);
   activate_mcs_ep_ti(adrs, 2);


   //// test PIPE-DATA
   // pipe-in  address: pi8A
   // pipe-out address: poAA poBC poBD
   //
   // u32 write_mcs_ep_pi_data(u32 offset, u32 data); // write data from a value to pipe-in
   // u32  read_mcs_ep_po_data(u32 offset);           // read data from pipe-out to a value
   xil_printf(">>> test PIPE-DATA \r\n");
   adrs = 0xAA;
   value = read_mcs_ep_po_data(adrs);
   xil_printf(">done: rd: 0x%08X @ 0x%02X \r\n", value, adrs);
   value = read_mcs_ep_po_data(adrs);
   xil_printf(">done: rd: 0x%08X @ 0x%02X \r\n", value, adrs);
   //
   adrs = 0x8A;
   value = 0x01030A11;
   write_mcs_ep_pi_data(adrs,value);
   xil_printf(">done: wr: 0x%08X @ 0x%02X \r\n", value, adrs);
   value = 0x01030A22;
   write_mcs_ep_pi_data(adrs,value);
   xil_printf(">done: wr: 0x%08X @ 0x%02X \r\n", value, adrs);
   //
   adrs = 0xAA;
   value = read_mcs_ep_po_data(adrs);
   xil_printf(">done: rd: 0x%08X @ 0x%02X \r\n", value, adrs);
   value = read_mcs_ep_po_data(adrs);
   xil_printf(">done: rd: 0x%08X @ 0x%02X \r\n", value, adrs);
   value = read_mcs_ep_po_data(adrs);
   xil_printf(">done: rd: 0x%08X @ 0x%02X \r\n", value, adrs);

   //// test PIPE-DATA in BUFFER
   //
   // data_adc0[], data_adc1[]
   // u32 write_mcs_ep_pi_buf(u32 offset, u32 len_byte, u8 *p_data); // write data from buffer to pipe-in
   // u32  read_mcs_ep_po_buf(u32 offset, u32 len_byte, u8 *p_data); // read data from pipe-out to buffer
   xil_printf(">>> test PIPE-DATA in BUFFER \r\n");
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc0[0], "data_adc0[0]");
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc0[1], "data_adc0[1]");
   //xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc0[2], "data_adc0[2]");
   //xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc0[3], "data_adc0[3]");
   //
   write_mcs_ep_pi_buf(0x8A, 8, (u8*)data_adc0);
   xil_printf(">done: %s \r\n", "data_adc0[0:1]-->pipe8A");
   read_mcs_ep_po_buf(0xAA, 8, (u8*)&(data_adc1[0]));
   xil_printf(">done: %s \r\n", "pipeAA-->data_adc1[0:1]");
   //
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc1[0], "data_adc1[0]");
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc1[1], "data_adc1[1]");
   //xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc1[2], "data_adc1[2]");
   //xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc1[3], "data_adc1[3]");

   //// test PIPE-DATA in FIFO/PIPE
   //
   // u32 write_mcs_ep_pi_fifo(u32 offset, u32 len_byte, u8 *p_data); // write data from fifo to pipe-in
   // u32  read_mcs_ep_po_fifo(u32 offset, u32 len_byte, u8 *p_data); // read data from pipe-out to fifo
   xil_printf(">>> test PIPE-DATA in FIFO \r\n");
   //void dcopy_pipe32_to_pipe32(u32 src_adrs_p32, u32 dst_adrs_p32, u32 len_byte);
   //void dcopy_pipe8_to_pipe32 (u32 src_adrs_p8,  u32 dst_adrs_p32, u32 len_byte);
   //void dcopy_pipe32_to_pipe8 (u32 src_adrs_p32, u32 dst_adrs_p8,  u32 len_byte);
   write_mcs_ep_pi_buf(0x8A, 8, (u8*)data_adc0);
   xil_printf(">done: %s \r\n", "data_adc0[0:1]-->pipe8A");
   //
   dcopy_pipe32_to_pipe32(ADRS_PORT_PO_AA_CMU, ADRS_PORT_PI_8A_CMU, 8);
   xil_printf(">done: %s \r\n", "pipeAA_32b-->pipe8A_32b");
   //
   read_mcs_ep_po_buf(0xAA, 8, (u8*)&(data_adc1[0]));
   xil_printf(">done: %s \r\n", "pipeAA-->data_adc1[0:1]");
   //
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc1[0], "data_adc1[0]");
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc1[1], "data_adc1[1]");
   //
   //
   write_mcs_ep_pi_buf(0x8A, 8*4, (u8*)data_adc0);
   xil_printf(">done: %s \r\n", "data_adc0[0:7]-->pipe8A");
   //
   dcopy_pipe8_to_pipe32 (ADRS_PORT_PO_AA_CMU, ADRS_PORT_PI_8A_CMU, 8);
   xil_printf(">done: %s \r\n", "pipeAA_8b-->pipe8A_32b");
   //
   read_mcs_ep_po_buf(0xAA, 8, (u8*)&(data_adc1[0]));
   xil_printf(">done: %s \r\n", "pipeAA-->data_adc1[0:1]");
   //
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc1[0], "data_adc1[0]");
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc1[1], "data_adc1[1]");
   //
   //
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc0[0], "data_adc0[0]");
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc0[1], "data_adc0[1]");
   //
   write_mcs_ep_pi_buf(0x8A, 8, (u8*)data_adc0);
   xil_printf(">done: %s \r\n", "data_adc0[0:1]-->pipe8A");
   //
   dcopy_pipe32_to_pipe8 (ADRS_PORT_PO_AA_CMU, ADRS_PORT_PI_8A_CMU, 8);
   xil_printf(">done: %s \r\n", "pipeAA_32b-->pipe8A_8b");
   //
   read_mcs_ep_po_buf(0xAA, 8*4, (u8*)&(data_adc1[0]));
   xil_printf(">done: %s \r\n", "pipeAA-->data_adc1[0:7]");
   //
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc1[0], "data_adc1[0]");
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc1[1], "data_adc1[1]");
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc1[2], "data_adc1[2]");
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc1[3], "data_adc1[3]");
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc1[4], "data_adc1[4]");
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc1[5], "data_adc1[5]");
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc1[6], "data_adc1[6]");
   xil_printf(">done: rd: 0x%08X @ %s \r\n", data_adc1[7], "data_adc1[7]");
   //



   //// wait a moment
   //xil_printf("Press any key: ");
   //inbyte(); // input a char for wait
   //xil_printf("\r\n");

   //// test cmu functions

   cmu_read_fpga_image_id();

   cmu_monitor_fpga();

   // test counter
   cmu_test_counter(CMU_PAR__RESET);
   cmu_test_counter(CMU_PAR__ON);

   // SPO
   cmu_spo_enable();
   cmu_spo_init();
   cmu_spo_update();
   cmu_spo_bit__leds(0x01);
   usleep(100000); //us
   cmu_spo_bit__leds(0x02);
   usleep(100000); //us
   cmu_spo_bit__leds(0x04);
   usleep(100000); //us
   cmu_spo_bit__leds(0x08);
   usleep(100000); //us
   cmu_spo_bit__leds(0x10);
   usleep(100000); //us
   cmu_spo_bit__leds(0x20);
   usleep(100000); //us
   cmu_spo_bit__leds(0x40);
   usleep(100000); //us
   cmu_spo_bit__leds(0x80);
   usleep(100000); //us
   cmu_spo_bit__leds(0xDA);
   usleep(100000); //us
   cmu_spo_bit__amp_pwr(CMU_PAR__ON);
   usleep(100000); //us
   cmu_spo_bit__amp_pwr(CMU_PAR__OFF);
   usleep(100000); //us
   cmu_spo_bit__adc0_gain(CMU_PAR__GN_OFF);
   cmu_spo_bit__adc1_gain(CMU_PAR__GN_OFF);
   cmu_spo_bit__vi_bw(CMU_PAR__BW_OFF);
   cmu_spo_bit__vq_bw(CMU_PAR__BW_OFF);

   // DAC_BIAS
   cmu_dac_bias_enable();
   cmu_dac_bias_init();
   cmu_dac_bias_set_buffer(0xAA,0xAB,0xAC,0xAD);
   cmu_dac_bias_update();
   //
   u32 DAC_RB[4];
   cmu_dac_bias_readback((u32*)&DAC_RB);
   //
   usleep(100000); //us


   // DWAVE
   cmu_dwave_enable();
   cmu_dwave_read_base_freq();
   cmu_dwave_wr_cnt_period(40);
   cmu_dwave_rd_cnt_period();
   usleep(100000); //us


   //// done
   xil_printf("\r\n");
   xil_printf(">>> wait for a while \r\n");
   sleep(3); //sec

   cmu_dwave_disable();

   cmu_dac_bias_init();
   cmu_dac_bias_disable();

   cmu_spo_init(); // clear SPO IC
   cmu_spo_disable();

   cmu_test_counter(CMU_PAR__OFF);
   cmu_test_counter(CMU_PAR__RESET);

   //
   xil_printf("\r\n");
   xil_printf(">>> disable MCS endpoints : \r\n");
   disable_mcs_ep();
   value = is_enabled_mcs_ep();
   xil_printf(">done: wr: 0x%08X \r\n", value);


   //// hw reset wz850
   xil_printf(">>> hw reset wz850 \r\n");
   hw_reset__wz850();


   //// socket setup

	/* WIZCHIP SOCKET Buffer initialize */
	if(ctlwizchip(CW_INIT_WIZCHIP,(void*)memsize) == -1)
	{
	   printf("WIZCHIP Initialized fail. \n");
	   while(1);
	}

	/* PHY link status check */
	do
	{
	   if(ctlwizchip(CW_GET_PHYLINK, (void*)&tmp) == -1)
		  printf("Unknown PHY Link stauts. \n");
	}while(tmp == PHY_LINK_OFF);


	/* Network initialization */
	network_init();


	/*******************************/
	/* WIZnet W5500 Code Examples  */
	/* TCPS/UDPS Loopback test     */
	/*******************************/
	/* Main loop */
	while(1)
	{

		/* SCPI server Test */
		if( (ret = scpi_tcps(SOCK_TCPS, gDATABUF, 5025)) < 0) {
			printf("SOCKET ERROR : %ld \n", ret);
		}

		/* Loopback Test */
		// TCP server loopback test
		//if( (ret = loopback_tcps(SOCK_TCPS, gDATABUF, 5025)) < 0) {
		//	printf("SOCKET ERROR : %ld \n", ret);
		//}

		// UDP server loopback test
		//if( (ret = loopback_udps(SOCK_UDPS, gDATABUF, 3000)) < 0) {
		//	printf("SOCKET ERROR : %ld \n", ret);
		//}

	} // end of Main loop

    /////
    cleanup_platform();

} // end of main()

/////////////////////////////////////////////////////////////
// Intialize the network information to be used in WIZCHIP //
/////////////////////////////////////////////////////////////
void network_init(void)
{
   uint8_t tmpstr[6];
	ctlnetwork(CN_SET_NETINFO, (void*)&gWIZNETINFO);
	ctlnetwork(CN_GET_NETINFO, (void*)&gWIZNETINFO);

	// set timeout para
	setRTR(2000);
	setRCR(23);

	// Display Network Information
	ctlwizchip(CW_GET_ID,(void*)tmpstr);
	printf(" \n=== %s NET CONF === \n",(char*)tmpstr);
	printf("MAC: %02X:%02X:%02X:%02X:%02X:%02X \n",gWIZNETINFO.mac[0],gWIZNETINFO.mac[1],gWIZNETINFO.mac[2],
		  gWIZNETINFO.mac[3],gWIZNETINFO.mac[4],gWIZNETINFO.mac[5]);
	printf("SIP: %d.%d.%d.%d \n", gWIZNETINFO.ip[0],gWIZNETINFO.ip[1],gWIZNETINFO.ip[2],gWIZNETINFO.ip[3]);
	printf("GAR: %d.%d.%d.%d \n", gWIZNETINFO.gw[0],gWIZNETINFO.gw[1],gWIZNETINFO.gw[2],gWIZNETINFO.gw[3]);
	printf("SUB: %d.%d.%d.%d \n", gWIZNETINFO.sn[0],gWIZNETINFO.sn[1],gWIZNETINFO.sn[2],gWIZNETINFO.sn[3]);
	printf("DNS: %d.%d.%d.%d \n", gWIZNETINFO.dns[0],gWIZNETINFO.dns[1],gWIZNETINFO.dns[2],gWIZNETINFO.dns[3]);
	printf("====================== \n");
	printf("RTP: %d (0x%04X) \n", getRTR(), getRTR());
	printf("RCR: %d (0x%04X) \n", getRCR(), getRCR());
	printf("====================== \n");


}
/////////////////////////////////////////////////////////////
