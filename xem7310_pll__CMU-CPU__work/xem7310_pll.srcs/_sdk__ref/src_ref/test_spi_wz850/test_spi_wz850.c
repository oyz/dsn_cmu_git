/*
 * test_spi_wz850.c : test wiz850io module over MB MCS IO bus
 * note: stdio --> MDM ... see system.mss in bsp
 */

// note:
//   low level IO access driver : xiomodule_l.h
//   logic-design under test : master_spi_wz850.v




#include <stdio.h>
//#include <stdlib.h>
#include "xil_printf.h"

#include "xiomodule_l.h" // low-level driver
//#include "xstatus.h"
//#include "mb_interface.h"
#include "microblaze_sleep.h" // for usleep

#include "../CMU_CPU_TOP/platform.h" // for init_platform()
#include "../CMU_CPU_TOP/cmu_cpu_config.h" // CMU-CPU board config

int main()
//int main_old()
{
    init_platform();

    // test for print on jtag-terminal
    print("Hello World!!\n\r");

    // test master_spi_wz850.v
    xil_printf(">> test master_spi_wz850.v \n\r");
    //
    u32 adrs;
    u32 value;
    //
    // ADRS_FPGA_IMAGE_ID
    xil_printf(">>> FPGA_IMAGE_ID: \n\r");
    adrs = ADRS_FPGA_IMAGE;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    // ADRS_TEST_REG
    xil_printf(">>> TEST_REG: \n\r");
    adrs = ADRS_TEST_REG;
    value = 0xABCD1234;
    //
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    value = XIomodule_In32 (adrs);
    xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
    //

    u8 cc;
    //
    while (1) {
    //// trig LAN RESET @  master_spi_wz850
    xil_printf(">>> trig LAN RESET @  master_spi_wz850: \n\r");
    //
    adrs = ADRS_PORT_WO_20; // {...,w_LAN_INTn,w_LAN_SSNn,w_LAN_RSTn,w_LAN_valid}
    value = XIomodule_In32 (adrs);
    xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = ADRS_PORT_WI_00;
    value = 0x00000001;
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = ADRS_PORT_WO_20; // {...,w_LAN_INTn,w_LAN_SSNn,w_LAN_RSTn,w_LAN_valid}
    value = XIomodule_In32 (adrs);
    xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    adrs = ADRS_PORT_WI_00;
    value = 0x00000000;
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
    //
    	while (1) {
   	    //
   	    adrs = ADRS_PORT_WO_20; // {...,w_LAN_INTn,w_LAN_SSNn,w_LAN_RSTn,w_LAN_valid}
   	    value = XIomodule_In32 (adrs);
   	    xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
   	    //
   	    if ((value & 0x03) == 0x03) break;
   	    //
   	    usleep(100); // 1us*100 sleep
   	    //
    	}
    //

    //
    //// write fifo data
    xil_printf(">>> write fifo data @  master_spi_wz850: \n\r");
    //MCS_IO_BUS_WRITE(32'h_C000_0800,32'h_0000_00AB);
    adrs = ADRS_PORT_PI_80;
    value = 0x000000AB;
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
    //MCS_IO_BUS_WRITE(32'h_C000_0800,32'h_0000_0012);
    adrs = ADRS_PORT_PI_80;
    value = 0x00000012;
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
    //MCS_IO_BUS_WRITE(32'h_C000_0800,32'h_0000_00CD);
    adrs = ADRS_PORT_PI_80;
    value = 0x000000CD;
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
    //MCS_IO_BUS_WRITE(32'h_C000_0800,32'h_0000_0089);
    adrs = ADRS_PORT_PI_80;
    value = 0x00000089;
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
    //// set up write frame
    xil_printf(">>> set up write frame @  master_spi_wz850: \n\r");
    //MCS_IO_BUS_WRITE(32'h_C000_0010,{16'h0040, 5'h06, 1'b1, 2'b0, 8'b0}); // wr frame setup
    adrs = ADRS_PORT_WI_01;
    value = (0x0040<<8)|(0x06<<3)|(0x4); // adrs 0x0040, block 0x06, write
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
    adrs = ADRS_PORT_WI_02;
    value = 4; // numbers of bytes: 4
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
    //// trig write frame over IO bus
    xil_printf(">>> trig write frame @  master_spi_wz850: \n\r");
    //MCS_IO_BUS_WRITE(32'h_C000_0000,{16'd04, 14'h0, 1'b1, 1'b0}); //
    adrs = ADRS_PORT_WI_00;
    value = 0x0002; // ... , trig_frame, trig_reset
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
    //MCS_IO_BUS_WRITE(32'h_C000_0000,{16'd04, 14'h0, 1'b0, 1'b0}); //
    adrs = ADRS_PORT_WI_00;
    value =0x0000; // ... , trig_frame, trig_reset
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
    //// read w_done_SPI_frame
    xil_printf(">>> read w_done_SPI_frame @  master_spi_wz850: \n\r");
    //MCS_IO_BUS_READ (32'h_C000_0200); // w_port_wo_20_0
    //
    	while (1) {
   	    //
   	    adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}
   	    value = XIomodule_In32 (adrs);
   	    xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
   	    //
   	    if ((value & 0x10) == 0x10) break;
   	    //
   	    usleep(100); // 1us*100 sleep
   	    //
    	}
    //
    //// set up read frame
    xil_printf(">>> set up read frame @  master_spi_wz850: \n\r");
    //MCS_IO_BUS_WRITE(32'h_C000_0010,{16'h0040, 5'h06, 1'b0, 2'b0, 8'b0}); // rd frame setup
    adrs = ADRS_PORT_WI_01;
    value = (0x0040<<8)|(0x06<<3)|(0x0); // adrs 0x0040, block 0x06, read
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
    adrs = ADRS_PORT_WI_02;
    value = 4; // numbers of bytes: 4
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
    //// trig read frame over IO bus
    xil_printf(">>> trig read frame @  master_spi_wz850: \n\r");
    //MCS_IO_BUS_WRITE(32'h_C000_0000,{16'd04, 14'h0, 1'b1, 1'b0}); //
    adrs = ADRS_PORT_WI_00;
    value = 0x0002; // ... , trig_frame, trig_reset
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
    //MCS_IO_BUS_WRITE(32'h_C000_0000,{16'd04, 14'h0, 1'b0, 1'b0}); //
    adrs = ADRS_PORT_WI_00;
    value = 0x0000; // ... , trig_frame, trig_reset
    XIomodule_Out32 (adrs, value);
    xil_printf(">done: wr: 0x%08X @ 0x%08X \n\r", value, adrs);
    //// read w_done_SPI_frame
    xil_printf(">>> read w_done_SPI_frame @  master_spi_wz850: \n\r");
    //MCS_IO_BUS_READ (32'h_C000_0200); // w_port_wo_20_0
    //
    	while (1) {
   	    //
   	    adrs = ADRS_PORT_WO_20; // {...,w_done_SPI_frame,w_LAN_INTn,w_LAN_SCSn,w_LAN_RSTn,w_done_LAN_reset}}
   	    value = XIomodule_In32 (adrs);
   	    xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
   	    //
   	    if ((value & 0x10) == 0x10) break;
   	    //
   	    usleep(100); // 1us*100 sleep
   	    //
    	}
    //
    //// read fifo data
    xil_printf(">>> read fifo data @  master_spi_wz850: \n\r");
    //MCS_IO_BUS_READ (32'h_C000_0A00);
    adrs = ADRS_PORT_PO_A0;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
    //MCS_IO_BUS_READ (32'h_C000_0A00);
    adrs = ADRS_PORT_PO_A0;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
    //MCS_IO_BUS_READ (32'h_C000_0A00);
    adrs = ADRS_PORT_PO_A0;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
    //MCS_IO_BUS_READ (32'h_C000_0A00);
    adrs = ADRS_PORT_PO_A0;
    value = XIomodule_In32 (adrs);
    xil_printf(">done: rd: 0x%08X @ 0x%08X \n\r", value, adrs);
    //

    ////
    print(">> Note: you must see AB, 12, CD, 89 from FIFO!!\n\r");

    //// repeat
    xil_printf (">> Enter X to finish: ");
    //
    //cc = getchar(); // waiting for enter char...
    cc = inbyte(); // input a char // without return char...
    //
    xil_printf ("%c\n\r",cc); // echo a char
    //
    if (cc=='X' || cc=='x') break;
    //
    xil_printf ("\n\r");
    }

    // test sscanf -- pass with stack 2KB and heap 32KB.
    int dd;
    int xx;
    sscanf("123, ABC","%d,%x",&dd,&xx);
    xil_printf("%d,%x \n\r",dd,xx);

    // test scanf -- pass with stack 2KB and heap 28KB.
    xil_printf (">> Enter any to finish: ");
    scanf ("%c",&cc);
    xil_printf ("%c\n\r",cc); // echo a char

    // process for nothing
    print(">> No more code is left! But, logics outside processor are still alive!!\n\r");

    /////
    cleanup_platform();
    return 0;
}
