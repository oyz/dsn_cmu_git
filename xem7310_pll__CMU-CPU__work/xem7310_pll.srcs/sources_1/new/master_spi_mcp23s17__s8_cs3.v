//------------------------------------------------------------------------
// master_spi_mcp23s17__s8_cs3.v
// - objective 
//		send and recv SPI frame of 16 bit data
//		write and read 16 bit PORT // researved
//		8 subsockets and 3 chip selections for 24EA devices sharing SPI bus
//
// - doc 
//		mcp23s17
//			http://ww1.microchip.com/downloads/en/DeviceDoc/20001952C.pdf
//
// - IO pin 
//		o_S1_SPI_CSB[2:0]
//		...
//		o_S8_SPI_CSB[2:0]
//		o_SPIOx_SCLK
//		o_SPIOx_MOSI
//		i_SPIOx_MISO
//
// - IO port
//		i_trig_SPI_frame  // generate SPI frame with 16 bit data.
//		o_done_SPI_frame  // sending SPI frame is done and data read is available.
//
//		i_socket_en[7:0]  // socket enable for 8EA
//		i_CS_en    [2:0]  // frame cs enable for 3EA 
//
//		i_pin_adrs_A[2:0] // hardware pin address or
//		i_R_W_bar         // 0 for write; 1 for read
//		i_reg_adrs_A[7:0] // register address
//		i_wr_DA[7:0]
//		i_wr_DB[7:0]
//		o_rd_DA[7:0]
//		o_rd_DB[7:0]
//
//
//		i_trig_PORT_write
//		o_done_PORT_write
//		i_trig_PORT_read
//		o_done_PORT_read
//
//		i_dr_CS0_GPA[7:0]  // 1 for input; 0 for output.
//		i_dr_CS0_GPB[7:0]  // 1 for input; 0 for output.
//		i_wr_CS0_GPA[7:0]
//		i_wr_CS0_GPB[7:0]
//		o_rd_CS0_GPA[7:0]
//		o_rd_CS0_GPB[7:0]
//		i_dr_CS1_GPA[7:0]  // 1 for input; 0 for output.
//		i_dr_CS1_GPB[7:0]  // 1 for input; 0 for output.
//		i_wr_CS1_GPA[7:0]
//		i_wr_CS1_GPB[7:0]
//		o_rd_CS1_GPA[7:0]
//		o_rd_CS1_GPB[7:0]
//
// 
// - SPI timing 
//		parameters
//			base SPI logic clock : 10MHz ~ 100ns 
//			base SPI clock       :  5MHz ~ 200ns ... _-
//			clock rate                   : 10MHz max ... (100ns)*2EA
//			cs setup time to clk rise    : 50ns min  ... (100ns)*1EA
//			data setup time to clk rise  : 20ns min  ... (100ns)*1EA
//			data hold time from clk rise : 20ns min  ... (100ns)*1EA
//			cs disable time              : 100ns min ... (100ns)*3EA
//			output valid from clk fall   : 90ns max  ... (100ns)*1EA
//			output disable time          : 100ns max ... (100ns)*1EA
//
//		<write> 
//		  o_SPIOx_CS_B -________________________________________________________________---________________________________________________________________---
//		  o_SPIOx_SCLK __-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-__-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//		  o_SPIOx_MOSI _CCccCCccCCccCCccAAaaAAaaAAaaAAaaDDddDDddDDddDDddDDddDDddDDddDDdd___CCccCCccCCccCCccAAaaAAaaAAaaAAaaDDddDDddDDddDDddDDddDDddDDddDDdd___
//
//		<read> 
//		  o_SPIOx_CS_B -________________________________________________________________---________________________________________________________________---
//		  o_SPIOx_SCLK __-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-____-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-___
//		  o_SPIOx_MOSI _CCccCCccCCccCCccAAaaAAaaAAaaAAaa___________________________________CCccCCccCCccCCccAAaaAAaaAAaaAAaa___________________________________
//		  o_SPIOx_MISO ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DDddDDddDDddDDddDDddDDddDDddDDdd~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~DDddDDddDDddDDddDDddDDddDDddDDdd~~
//
//
// - SPI frame format: 16 bit long data
//		<write> 
//		  o_SPIOx_CS_B -________________________________________________________________---
//		  o_SPIOx_SCLK __-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
//		  o_SPIOx_MOSI _C7C6C5C4C3C2C1C0A7A6A5A4A3A2A1A0D7D6D5D4D3D2D1D0E7E6E5E4E3E2E1E0___
//                     
//		<read>           
//		  o_SPIOx_CS_B -________________________________________________________________---
//		  o_SPIOx_SCLK __-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-___
//		  o_SPIOx_MOSI _C7C6C5C4C3C2C1C0A7A6A5A4A3A2A1A0___________________________________
//		  o_SPIOx_MISO ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~D7D6D5D4D3D2D1D0E7E6E5E4E3E2E1E0~~
//
//		control bits      : C[7:0]
//			C7 0
//			C6 1
//			C5 0
//			C4 0
//			C3 HW_A2
//			C2 HW_A1
//			C1 HW_A0
//			C0 R_W_bar
//		address bits      : A[7:0]
//		data bits for GPA : D[7:0]
//		data bits for GPB : E[7:0]
//
//
// - state assignment on SPI frame
//		<write>
//        state_frame  0000012121212121212121212121212121212121212121212121212121212121212123470000012121212
//   i_trig_SPI_frame  ____--______________________________________________________________________--_______
//   o_done_SPI_frame  -----___________________________________________________________________-----________
//		  o_SPIOx_CS_B -----________________________________________________________________--------________
//		  o_SPIOx_SCLK ______-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_______-_-_-_-
//		  o_SPIOx_MOSI _____C7C6C5C4C3C2C1C0A7A6A5A4A3A2A1A0D7D6D5D4D3D2D1D0E7E6E5E4E3E2E1E0________C7C6C5C4
//		  r_CS_id      ~~~~~XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX~~~~~~~~~~~~~
//		  r_R_W_bar    ~~~~~0000000000000000000000000000000000000000000000000000000000000000000~~~~~~~~~~~~~
//	 r_fbit_index_H    0000000000000011111111112222222222333333333344444444445555555555666666660000000000000
//	 r_fbit_index_L    0000012345678901234567890123456789012345678901234567890123456789012345670000012345678
//
//		<read>           
//        state_frame  0000012121212121212121212121212121212121212121212121212121212121212125670000012121212
//   i_trig_SPI_frame  ____--______________________________________________________________________--_______
//   o_done_SPI_frame  -----___________________________________________________________________-----________
//		  o_SPIOx_CS_B -----________________________________________________________________--------________
//		  o_SPIOx_SCLK ______-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_________-_-_-_-
//		  o_SPIOx_MOSI _____C7C6C5C4C3C2C1C0A7A6A5A4A3A2A1A0________________________________________C7C6C5C4
//		  o_SPIOx_MISO ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~D7D6D5D4D3D2D1D0E7E6E5E4E3E2E1E0~~~~~~~~~~~~~~~
//		  r_CS_id      ~~~~~XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX~~~~~~~~~~~~~
//		  r_R_W_bar    ~~~~~1111111111111111111111111111111111111111111111111111111111111111111~~~~~~~~~~~~~
//	 r_fbit_index_H    0000000000000011111111112222222222333333333344444444445555555555666666660000000000000
//	 r_fbit_index_L    0000012345678901234567890123456789012345678901234567890123456789012345670000012345678
//
// - state transitions and job
//		* s0 --> s1, if i_trig_SPI_frame == 1
//		* s1 --> s2, always
//		* s2 --> s1, if r_fbit_index < 64 
//		* s2 --> s3, if r_fbit_index >== 64 and r_R_W_bar == 0  
//		* s2 --> s5, if r_fbit_index >== 64 and r_R_W_bar == 1  
//		* s3 --> s4 --> s7, always
//		* s5 --> s6 --> s7, always
//		* s7 --> s0, always
//
// - state and job 
//		* s0 : done high, sclk low , cs_b high // job: ready to trig frame
//		* s1 : done low , sclk low , cs_b low
//		* s2 : done low , sclk high, cs_b low  // job: shift r_shift_send and r_shift_recv
//		* s3 : done low , sclk low , cs_b high 
//		* s4 : done low , sclk high, cs_b high
//		* s5 : done low , sclk low , cs_b high // job: load data from r_shift_recv
//		* s6 : done low , sclk low , cs_b high
//		* s7 : done low , sclk low , cs_b high // job: last state, go to s0.
//
//------------------------------------------------------------------------

`timescale 1ns / 1ps
module master_spi_mcp23s17__s8_cs3 (  
	// 
	input wire clk, // assume 10MHz 
	input wire reset_n,
	
	// control 
	input  wire i_trig_SPI_frame    , // SPI frame trigger 
	output wire o_done_SPI_frame    , // SPI frame done 
	output wire o_done_SPI_frame_TO , // SPI frame done // one shot trig out // @ w_clk_div_en
	output wire o_busy_SPI_frame    ,
	output wire [15:0] o_cnt_spio_trig_frame , //$$ count i_trig_SPI_frame based on clk
	
	// IO
	//output wire o_SPIO0_CS   ,
	//output wire o_SPIO1_CS   ,
	output wire [2:0] o_S1_SPI_CSB,
	output wire [2:0] o_S2_SPI_CSB,
	output wire [2:0] o_S3_SPI_CSB,
	output wire [2:0] o_S4_SPI_CSB,
	output wire [2:0] o_S5_SPI_CSB,
	output wire [2:0] o_S6_SPI_CSB,
	output wire [2:0] o_S7_SPI_CSB,
	output wire [2:0] o_S8_SPI_CSB,
	//
	output wire o_SPIOx_SCLK ,
	output wire o_SPIOx_MOSI ,
	input  wire i_SPIOx_MISO ,
	
	// SPI frame contents
	//input  wire       i_CS_id      , // 0 for SPIO0; 1 for SPIO1
	input  wire [7:0] i_socket_en  ,
	input  wire [2:0] i_CS_en      ,
	//
	input  wire	i_forced_pin_mode_en ,
	input  wire	i_forced_sig_mosi    ,
	input  wire	i_forced_sig_sclk    ,
	input  wire i_forced_sig_csel    ,
	//
	input  wire [2:0] i_pin_adrs_A , // hardware pin address or
	input  wire       i_R_W_bar    , // 0 for write; 1 for read
	input  wire [7:0] i_reg_adrs_A , // register address
	input  wire [7:0] i_wr_DA      ,
	input  wire [7:0] i_wr_DB      ,
	output wire [7:0] o_rd_DA      ,
	output wire [7:0] o_rd_DB      ,
	//
	// multi-frame controls and contents
	//input  wire i_trig_PORT_write ,
	//output wire o_done_PORT_write ,
	//input  wire i_trig_PORT_read  ,
	//output wire o_done_PORT_read  ,
	////
	//input  wire [7:0] i_dr_CS0_GPA, 
	//input  wire [7:0] i_dr_CS0_GPB, 
	//input  wire [7:0] i_wr_CS0_GPA,
	//input  wire [7:0] i_wr_CS0_GPB,
	//output wire [7:0] o_rd_CS0_GPA,
	//output wire [7:0] o_rd_CS0_GPB,
	////
	//input  wire [7:0] i_dr_CS1_GPA, 
	//input  wire [7:0] i_dr_CS1_GPB, 
	//input  wire [7:0] i_wr_CS1_GPA,
	//input  wire [7:0] i_wr_CS1_GPB,
	//output wire [7:0] o_rd_CS1_GPA,
	//output wire [7:0] o_rd_CS1_GPB,
	//
	// flag
	output wire valid
);

// valid 
(* keep = "true" *) reg r_valid;
assign valid = r_valid;
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		r_valid <= 1'b0;
	end
	else begin
		r_valid <= 1'b1;
	end

// clock div4
reg [3:0] sr_clk;
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		sr_clk <= 4'b0010;
	end
	else begin
		sr_clk <= {sr_clk[2:0],sr_clk[3]}; // shifr 
	end
//
wire w_clk_div_en = sr_clk[0];



// frame control
reg r_done_SPI_frame;
assign o_done_SPI_frame = r_done_SPI_frame;
reg r_done_SPI_frame_TO;
assign o_done_SPI_frame_TO = r_done_SPI_frame_TO;
//
reg r_SPIOx_SCLK;
assign o_SPIOx_SCLK   = (i_forced_pin_mode_en)? i_forced_sig_sclk : r_SPIOx_SCLK;
//
reg r_SPIOx_CS  ;
wire w_SPIOx_CS       = (i_forced_pin_mode_en)? i_forced_sig_csel : r_SPIOx_CS;

wire [7:0] w_socket_en = i_socket_en;
wire [2:0] w_CS_en     = i_CS_en    ;
//
assign o_S1_SPI_CSB[0] = (w_socket_en[0] & w_CS_en[0])? ~w_SPIOx_CS : 1'b1;
assign o_S2_SPI_CSB[0] = (w_socket_en[1] & w_CS_en[0])? ~w_SPIOx_CS : 1'b1;
assign o_S3_SPI_CSB[0] = (w_socket_en[2] & w_CS_en[0])? ~w_SPIOx_CS : 1'b1;
assign o_S4_SPI_CSB[0] = (w_socket_en[3] & w_CS_en[0])? ~w_SPIOx_CS : 1'b1;
assign o_S5_SPI_CSB[0] = (w_socket_en[4] & w_CS_en[0])? ~w_SPIOx_CS : 1'b1;
assign o_S6_SPI_CSB[0] = (w_socket_en[5] & w_CS_en[0])? ~w_SPIOx_CS : 1'b1;
assign o_S7_SPI_CSB[0] = (w_socket_en[6] & w_CS_en[0])? ~w_SPIOx_CS : 1'b1;
assign o_S8_SPI_CSB[0] = (w_socket_en[7] & w_CS_en[0])? ~w_SPIOx_CS : 1'b1;
//
assign o_S1_SPI_CSB[1] = (w_socket_en[0] & w_CS_en[1])? ~w_SPIOx_CS : 1'b1;
assign o_S2_SPI_CSB[1] = (w_socket_en[1] & w_CS_en[1])? ~w_SPIOx_CS : 1'b1;
assign o_S3_SPI_CSB[1] = (w_socket_en[2] & w_CS_en[1])? ~w_SPIOx_CS : 1'b1;
assign o_S4_SPI_CSB[1] = (w_socket_en[3] & w_CS_en[1])? ~w_SPIOx_CS : 1'b1;
assign o_S5_SPI_CSB[1] = (w_socket_en[4] & w_CS_en[1])? ~w_SPIOx_CS : 1'b1;
assign o_S6_SPI_CSB[1] = (w_socket_en[5] & w_CS_en[1])? ~w_SPIOx_CS : 1'b1;
assign o_S7_SPI_CSB[1] = (w_socket_en[6] & w_CS_en[1])? ~w_SPIOx_CS : 1'b1;
assign o_S8_SPI_CSB[1] = (w_socket_en[7] & w_CS_en[1])? ~w_SPIOx_CS : 1'b1;
//
assign o_S1_SPI_CSB[2] = (w_socket_en[0] & w_CS_en[2])? ~w_SPIOx_CS : 1'b1;
assign o_S2_SPI_CSB[2] = (w_socket_en[1] & w_CS_en[2])? ~w_SPIOx_CS : 1'b1;
assign o_S3_SPI_CSB[2] = (w_socket_en[2] & w_CS_en[2])? ~w_SPIOx_CS : 1'b1;
assign o_S4_SPI_CSB[2] = (w_socket_en[3] & w_CS_en[2])? ~w_SPIOx_CS : 1'b1;
assign o_S5_SPI_CSB[2] = (w_socket_en[4] & w_CS_en[2])? ~w_SPIOx_CS : 1'b1;
assign o_S6_SPI_CSB[2] = (w_socket_en[5] & w_CS_en[2])? ~w_SPIOx_CS : 1'b1;
assign o_S7_SPI_CSB[2] = (w_socket_en[6] & w_CS_en[2])? ~w_SPIOx_CS : 1'b1;
assign o_S8_SPI_CSB[2] = (w_socket_en[7] & w_CS_en[2])? ~w_SPIOx_CS : 1'b1;
//
reg [7:0] r_rd_DA;
reg [7:0] r_rd_DB;
//
assign o_rd_DA              = r_rd_DA;
assign o_rd_DB              = r_rd_DB;
//
reg [31:0] r_shift_send_w32; 
reg [15:0] r_shift_recv_w16; 
reg  [7:0] r_fbit_index    ; // check around 64 bit count
reg        r_R_W_bar       ;
//
assign o_SPIOx_MOSI   = (i_forced_pin_mode_en)? i_forced_sig_mosi : r_shift_send_w32[31];


// trig SPI frame  
reg r_smp_trig_SPI_frame;
reg r_trig_SPI_frame;
//
(* keep = "true" *) reg [15:0] r_cnt_spio_trig_frame;
//
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		r_smp_trig_SPI_frame   <= 1'b0;
		r_trig_SPI_frame       <= 1'b0;
		r_cnt_spio_trig_frame  <= 16'b0;
	end
	else begin 
		//
		r_smp_trig_SPI_frame   <= i_trig_SPI_frame; // sampling
		//
		if ((~r_smp_trig_SPI_frame)&i_trig_SPI_frame) // detect rise
			r_trig_SPI_frame     <= 1'b1;
		else if (r_done_SPI_frame == 1'b0) // detect frame start
			r_trig_SPI_frame   <= 1'b0;
		//
		if ((~r_smp_trig_SPI_frame)&i_trig_SPI_frame) // detect rise
			r_cnt_spio_trig_frame <= r_cnt_spio_trig_frame + 1;
		else 
			r_cnt_spio_trig_frame <= r_cnt_spio_trig_frame;
		//
	end
//
assign o_cnt_spio_trig_frame = r_cnt_spio_trig_frame;

// busy 
reg r_busy_SPI_frame;
assign o_busy_SPI_frame = r_busy_SPI_frame;

// state register
(* keep = "true" *) reg [7:0] state; 

// state def 
parameter STATE_SIG_0 = 8'h00; 
parameter STATE_SIG_1 = 8'h01; 
parameter STATE_SIG_2 = 8'h02; 
parameter STATE_SIG_3 = 8'h03; 
parameter STATE_SIG_4 = 8'h04; 
parameter STATE_SIG_5 = 8'h05; 
parameter STATE_SIG_6 = 8'h06; 
parameter STATE_SIG_7 = 8'h07; 
//


// process state 
always @(posedge clk, negedge reset_n)
	if (!reset_n) begin
		state				<= STATE_SIG_0;
		r_done_SPI_frame	<=  1'b1;
		r_done_SPI_frame_TO <=  1'b0;
		r_busy_SPI_frame    <=  1'b0;
		//
		r_SPIOx_CS          <=  1'b0;
		r_SPIOx_SCLK        <=  1'b0;
		r_rd_DA             <=  8'b0; // [7:0] 
		r_rd_DB             <=  8'b0; // [7:0] 
		r_fbit_index        <=  8'b0; // [7:0]
		r_shift_recv_w16    <= 16'b0; // [15:0]
		r_shift_send_w32    <= 32'b0; // [31:0]
		r_R_W_bar           <=  1'b0;
		end 
	else case (state)
		// 
		//STATE_SIG_0 :  begin
		STATE_SIG_0 : if (w_clk_div_en) begin
			r_done_SPI_frame_TO <=  1'b0; // return to '0' for one pulse...
			r_busy_SPI_frame    <=  1'b0;
			//
			if (r_trig_SPI_frame) begin
				state				<= STATE_SIG_1;
				r_done_SPI_frame	<= 1'b0;
				r_busy_SPI_frame    <= 1'b1;
				//
				//
				r_SPIOx_CS          <=  1'b1;
				//
				r_SPIOx_SCLK        <=  1'b0;
				r_rd_DA             <=  8'b0; // [7:0] 
				r_rd_DB             <=  8'b0; // [7:0] 
				r_fbit_index        <=  8'b1; // [7:0]
				r_shift_recv_w16    <= 16'b0; // [15:0]
				//
				// load frame control 
				r_shift_send_w32    <= {
									{4'b0100, i_pin_adrs_A, i_R_W_bar},
									i_reg_adrs_A,
									i_wr_DA,
									i_wr_DB};
				r_R_W_bar           <=  i_R_W_bar;
				//
				end
			end
		// 
		STATE_SIG_1 : if (w_clk_div_en) begin
			state				<= STATE_SIG_2;
			r_fbit_index        <= r_fbit_index + 1;
			r_SPIOx_SCLK        <=  1'b1;
			//
			end
		// 
		STATE_SIG_2 : if (w_clk_div_en) begin
			// common
			r_fbit_index        <= r_fbit_index + 1;
			r_SPIOx_SCLK        <=  1'b0;
			// shift data 
			r_shift_send_w32 <= {r_shift_send_w32[30:0], 1'b0};
			r_shift_recv_w16 <= {r_shift_recv_w16[14:0], i_SPIOx_MISO};
			// still in frame 
			if (r_fbit_index < 8'd64) begin 
				state				<= STATE_SIG_1;
				end 
			// end of write frame 
			else if (r_R_W_bar == 0) begin 
				state				<= STATE_SIG_3;
				r_SPIOx_CS          <=  1'b0;
				//
				end
			// end of read frame 
			else begin 
				state				<= STATE_SIG_5;
				r_SPIOx_CS          <=  1'b0;
				//
				end
			end
		// 
		STATE_SIG_3 : if (w_clk_div_en) begin
			state				<= STATE_SIG_4;
			r_fbit_index        <= r_fbit_index + 1;
			r_SPIOx_SCLK        <=  1'b1;
			//
			end
		// 
		STATE_SIG_4 : if (w_clk_div_en) begin
			state				<= STATE_SIG_7;
			r_fbit_index        <= r_fbit_index + 1;
			r_SPIOx_SCLK        <=  1'b0;
			//
			end
		// 
		STATE_SIG_5 : if (w_clk_div_en) begin
			state				<= STATE_SIG_6;
			r_fbit_index        <= r_fbit_index + 1;
			r_SPIOx_SCLK        <=  1'b0;
			//
			r_rd_DA             <= r_shift_recv_w16[15:8];
			r_rd_DB             <= r_shift_recv_w16[ 7:0];
			//
			end
		// 
		STATE_SIG_6 : if (w_clk_div_en) begin
			state		        <= STATE_SIG_7;
			r_fbit_index        <= r_fbit_index + 1;
			r_SPIOx_SCLK        <=  1'b0;
			end
		//
		STATE_SIG_7 : if (w_clk_div_en) begin
			state				<= STATE_SIG_0;
			r_fbit_index        <= 8'b0;
			//
			r_done_SPI_frame	<= 1'b1; 
			r_done_SPI_frame_TO <=  1'b1; // trig out // one pulse
			end
		default:
			state				<= STATE_SIG_0;
		// 
	endcase
	
endmodule
