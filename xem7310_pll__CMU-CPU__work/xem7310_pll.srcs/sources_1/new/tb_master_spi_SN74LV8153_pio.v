`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Module Name: tb_master_spi_SN74LV8153_pio
// 
// http://www.asic-world.com/verilog/art_testbench_writing1.html
// http://www.asic-world.com/verilog/art_testbench_writing2.html
// https://www.xilinx.com/support/documentation/sw_manuals/xilinx2016_1/ug937-vivado-design-suite-simulation-tutorial.pdf
//
//////////////////////////////////////////////////////////////////////////////////


module tb_master_spi_SN74LV8153_pio;
reg clk; // assume 10MHz or 100ns
reg reset_n;
reg en;
//
reg clk_bus; //$$ 9.92ns for USB3.0	
//
reg init;
reg update;
reg test;
reg [7:0] conf;
//
reg [31:0] test_sdi_pdata;
wire [31:0] test_sdo_pdata;
//
reg [31:0] r_sdo;
wire sdo = r_sdo[31];

// DUT
wire sclk;
master_spi_SN74LV8153_pio master_spi_SN74LV8153_pio_inst(
	.clk(clk), // assume 10MHz or 100ns
	.reset_n(reset_n),
    .en(en),
	//
	//.clk_bus(clk_bus),
	//
    .init(init),
    .update(update),
    .test(test),
	//
	//.adrs_start(3'b010),
	//.adrs_start(3'b000),
	.adrs_start(3'b100),
	//.num_bytes(3'd3), // bytes to send = num_bytes + 1
	.num_bytes(3'd7), // bytes to send = num_bytes + 1
	.test_pdata(test_sdi_pdata[13:0]),
	//
	.ctr_in(64'h_CA75_56AD_56AD_CA75),
	//
	.error(),
	.debug_out()
);

initial begin
#0		clk = 1'b0;
		reset_n = 1'b0;
		en = 1'b0;
		clk_bus = 1'b0;
		init = 1'b0;
		update = 1'b0;
		test = 1'b0;
		conf = 8'b0;
		test_sdi_pdata = 32'b1010_0101_1001_0110_1100_1010_0011_0101;
#200	reset_n = 1'b1;
#200	en = 1'b1;
#200	test = 1'b1;
		conf = 8'b0000_0001;
#15_000	init = 1'b1;
		test = 1'b0;
#30_000	update = 1'b1;
		test = 1'b1;
#8000_000; // wait 8ms = 8000_000ns
		conf = 8'b0000_0010;
		update = 1'b0;
#200;
		update = 1'b1;
#3000_000; // wait 3ms = 3000_000ns
		conf = 8'b0000_0100;
		update = 1'b0;
#200;
		update = 1'b1;
#3000_000; // wait 3ms = 3000_000ns
#3000_000; // wait 3ms = 3000_000ns
#3000_000; // wait 3ms = 3000_000ns
#3000_000; // wait 3ms = 3000_000ns
#3000_000; // wait 3ms = 3000_000ns
		en = 1'b0;
end

always
#50 	clk = ~clk; // toggle every 50ns --> clock 100ns 

always
#4.96 	clk_bus = ~clk_bus; // toggle every 4.96ns --> clock 9.92ns for USB3.0	 

always @(posedge sclk, negedge reset_n)
    if (!reset_n) begin
        r_sdo <= 32'b1010_0101_1001_0110_1100_1010_0011_0101;
        end 
    else begin
        r_sdo <= {r_sdo[30:0], r_sdo[31]}; 
        end

//initial begin
	//$dumpfile ("waveform.vcd"); 
	//$dumpvars; 
//end 
  
//initial  begin
	//$display("\t\t time,\t clk,\t reset_n,\t en"); 
	//$monitor("%d,\t%b,\t%b,\t%b,\t%d",$time,clk,reset_n,en); 
//end 
  
//initial begin
	//#200_000 $finish; // 200us = 200_000 ns
	//#1000 $finish; // 1us = 1000 ns
	//#1000_000 $finish; // 1ms = 1000_000 ns
//end

endmodule
