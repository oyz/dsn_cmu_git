# ok_cmu_cpu__report_result_from_dict_file.py
#
#  performance report of data from dict file 
#


####
## library call
import ok_cmu_cpu__lib as cmu


## find filenames
def get_dict_filenames_from_dialog():
	pass
#
get_dict_filenames_from_dialog()

## set dict_file
#dict_filename = './report_samples/DUMP_0-0___TS_20181221T192311.dict'
#
#dict_filename = './report_samples/DUMP_0-0___TS_20181222T154203.dict'
#dict_filename = './report_samples/DUMP_1-1___TS_20181221T192318.dict'
dict_filename = './report_samples/DUMP_1-2___TS_20181222T154214.dict'


## readback dict
exec( 'readback_dict = ' + open(dict_filename).read() )
print(len(readback_dict))
#print(readback_dict)

## show full spectrum and save figure
#  
TEST_CONF = readback_dict
try:
	FS_TARGET     = TEST_CONF['FS_TARGET'    ]
	DUMP_FILE_PRE = TEST_CONF['DUMP_FILE_PRE']
	ADC_LIST_INT  = TEST_CONF['ADC_LIST_INT']
	NUM_SMP_ZOOM  = TEST_CONF['NUM_SMP_ZOOM']
	#freq_to_see_in_phase = 100e3
	freq_to_see_in_phase = 1e6
	freq_margin_ratio_to_see_in_phase = 1e-2
	fig_filename,Pxx_psd0,freqs_psd0,Pxx_psd1,freqs_psd1,S_phs0,freqs_phs0,S_phs1,freqs_phs1 \
		= cmu.cmu_adc_display_data_list_int__zoom__full_spectrum (ADC_LIST_INT,DUMP_FILE_PRE,FS_TARGET,NUM_SMP_ZOOM,freq_to_see_in_phase,freq_margin_ratio_to_see_in_phase)
except:
	fig_filename = None
#
print(fig_filename)


## report some measures on spectrum
#
# find test freq 
# find max test ripple freq 
#
# find max noise level

###
#import matplotlib.pyplot as plt
#
#####
### filename to batch
#PY_FILENAME = 'ok_cmu_cpu__09_test_sheet_data.py'
### batch interval
##INT_SEC = 1800 # 30min
##INT_SEC = 300 # 5min
#INT_SEC = 30 
##INT_SEC = 0.1 
### repeat number
##MAX_COUNT = 2*12
#MAX_COUNT = 36
#
#####
#cnt=0
#while True:
#	plt.close('all')
#	#
#	exec(open(PY_FILENAME).read())
#	#
#	#plt.draw()
#	#plt.show()
#	#plt.pause(10)
#	#
#	print('> continue: {}'.format(cnt))
#	#
#	plt.pause(INT_SEC)
#	#
#	cnt=cnt+1
#	if cnt==MAX_COUNT:
#		break
#	#
##
#print('> all done!')
#
#####