# read_postproc_files__cmu_cpu.py
#  
#  read report file and display 
#
#  rev0: draft
#
#  https://www.quora.com/How-does-one-extract-arrays-from-CSV-files-with-columns-in-Python
#
#  https://www.devdungeon.com/content/working-binary-data-python
#  https://pyformat.info/
#  https://matplotlib.org/api/_as_gen/matplotlib.pyplot.psd.html#matplotlib.pyplot.psd
#    https://matplotlib.org/gallery/lines_bars_and_markers/psd_demo.html#sphx-glr-gallery-lines-bars-and-markers-psd-demo-py
#
# for import numpy: https://scipy.org/install.html
#   python -m pip install --user numpy scipy matplotlib ipython jupyter pandas sympy nose
# for import multiarray:
#   https://stackoverflow.com/questions/21324426/numpy-build-fails-with-cannot-import-multiarray
#   pip install numpy
#   pip install matplotlib
#
# https://matplotlib.org/users/tight_layout_guide.html
#
# https://pythonspot.com/tk-file-dialogs/
#
#



## load library/module
import csv
import numpy as np
import matplotlib.pyplot as plt
plt.ion() # matplotlib interactive mode 
#plt.close('all')
from sys import exit
import tkinter as tk
from tkinter import filedialog
import os
import datetime
#
import glob # for filenames

## controls

## parameters

## functions

def is_int(str):
	try:
		tmp = int(str)
		return True
	except ValueError:
		return False

def is_float(str):
	try:
		tmp = float(str)
		return True
	except ValueError:
		return False

def is_complex(str):
	try:
		tmp = complex(str)
		return True
	except ValueError:
		return False



## start

## locate report file

#filename_rpt = 'RPT_NP_DUMP_BD1_731__U_001M_210_211_16K_18690__TS_20190725T090909.txt'
filename_rpt = 'RPT_NP_DUMP_BD5_531__U_001M_210_211_16K_18690__TS_20190725T090909.txt'

#filename_rpt = 'RPT_NP_DUMP_BD1__U_001M_210_211_16K_18690__TS_20190724T094240.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_001M_210_211_16K_18690__TS_20190724T094240.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_001M_210_211_16K_18690__TS_20190724T094240.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_001M_210_211_16K_18690__TS_20190724T094240.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_001M_210_211_16K_18690__TS_20190724T094240.txt'

#filename_rpt = 'RPT_NP_DUMP_BD1__U_001M_210_211_16K_18690__TS_20190718T191919.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_001M_210_211_16K_18690__TS_20190718T191919.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_001M_210_211_16K_18690__TS_20190718T191919.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_001M_210_211_16K_18690__TS_20190718T191919.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_001M_210_211_16K_18690__TS_20190718T191919.txt'


#filename_rpt = 'RPT_NP_DUMP_BD1__U_100K_210_2101_2100_4K__TS_20190720T0121212.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_001M_210_211_3990_4K__TS_20190720T0121212.txt'


#filename_rpt = 'RPT_NP_DUMP_BD1__U_001M_210_211_3990_4K__TS_20190719T090909.txt'

#filename_rpt = 'RPT_NP_DUMP_BD1__U_001M_210_211_16K_18690__TS_20190716T170855.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_001M_210_211_16K_18690__TS_20190716T181736.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_001M_210_211_16K_18690__TS_20190716T184729.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_001M_210_211_16K_18690__TS_20190716T192111.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_001M_210_211_16K_18690__TS_20190717T082630.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_001M_210_211_16K_18690__TS_20190717T092709.txt'
#
#filename_rpt = 'RPT_NP_DUMP_BD1__U_001M_210_211_16K_18690__TS_20190717T181241.txt'  # 
#filename_rpt = 'RPT_NP_DUMP_BD2__U_001M_210_211_16K_18690__TS_20190717T181501.txt'  # shift at 100 trial ... fpga image changed 
#filename_rpt = 'RPT_NP_DUMP_BD3__U_001M_210_211_16K_18690__TS_20190717T181947.txt'  # shift at 100 trial ... fpga image changed 
#filename_rpt = 'RPT_NP_DUMP_BD4__U_001M_210_211_16K_18690__TS_20190717T182238.txt'  # shift at 100 trial ... fpga image changed 
#filename_rpt = 'RPT_NP_DUMP_BD5__U_001M_210_211_16K_18690__TS_20190717T182407.txt'  # shift at 100 trial ... fpga image changed 
#

#filename_rpt = 'RPT_NP_DUMP_BD1__U_001M_210_211_16K_18690__TS_20190716__merge.txt'

#filename_rpt = 'RPT_NP_DUMP_BD1__U_001M_210_211_18690_16K__TS_20190715T101010.txt'

#filename_rpt = 'RPT_NP_DUMP_BD1__U_010K_210_21010_14700_16K__TS_20190715T101010.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_010K_210_21010_16K_14700__TS_20190710T084615.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_010K_210_21010_16K_14700__TS_20190709T170918.txt'

#filename_rpt = 'RPT_XP_DUMP_BD5__N_001K_210_14_45000_64K__TS_20190704T151515.txt'

#filename_rpt = 'RPT_XP_DUMP_BD1__U_010K_210_21010_2100_4K__TS_20190704T101010.txt' ## 15ppm
#filename_rpt = 'RPT_XP_DUMP_BD1__U_010K_210_21010_14700_16K__TS_20190704T101010.txt' ## 10ppm

#filename_rpt = 'RPT_XP_DUMP_BD1__U_010K_210_21010_65100_64K__TS_20190704T101010.txt' ## NG 0.9%
#filename_rpt = 'RPT_XP_DUMP_BD1__U_010K_210_21014_64500_64K__TS_20190704T101010.txt' ## NG 0.9%
#filename_rpt = 'RPT_XP_DUMP_BD1__U_010K_210_21007_57000_64K__TS_20190704T101010.txt' ## NG 0.9%

#filename_rpt = 'RPT_XP_DUMP_BD1__U_010K_210_21010_128100_128K__TS_20190704T101010.txt' ## NG 0.9%

#filename_rpt = 'RPT_NP_DUMP_BD1__N_001K_210_14_16K_15000__TS_20190629T170251.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__N_001K_210_15_16K_28000__TS_20190629T170553.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__N_002K_210_14_16K_15000__TS_20190629T170639.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__N_002K_210_15_16K_21000__TS_20190629T170706.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__N_005K_210_14_16K_15000__TS_20190629T170731.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__N_005K_210_15_16K_14000__TS_20190629T170752.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_010K_210_21007_16K_15000__TS_20190629T170902.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_010K_210_21010_16K_14700__TS_20190629T170824.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_010K_210_21014_16K_10500__TS_20190629T170843.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_020K_210_10501_16K_10500__TS_20190629T171003.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_020K_210_10505_16K_14700__TS_20190629T170941.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_020K_210_10507_16K_10500__TS_20190629T170923.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_050K_210_4202_16K_14700__TS_20190629T171048.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_050K_210_4203_16K_15400__TS_20190629T171022.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_100K_210_2101_16K_14700__TS_20190629T171122.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_100K_210_2102_16K_13650__TS_20190629T171147.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_200K_210_1051_16K_13650__TS_20190629T171213.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_500K_210_421_16K_15540__TS_20190629T171236.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_001M_210_211_16K_18690__TS_20190629T171258.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_002M_210_106_16K_15855__TS_20190629T171323.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_005M_210_43_16K_16338__TS_20190629T171352.txt'
#filename_rpt = 'RPT_NP_DUMP_BD1__U_010M_210_41_16K_16359__TS_20190629T171426.txt'
#
#filename_rpt = 'RPT_NP_DUMP_BD2__N_001K_210_14_16K_15000__TS_20190630T075143.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__N_001K_210_15_16K_28000__TS_20190630T075204.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__N_002K_210_14_16K_15000__TS_20190630T075228.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__N_002K_210_15_16K_21000__TS_20190630T075249.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__N_005K_210_14_16K_15000__TS_20190630T075314.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__N_005K_210_15_16K_14000__TS_20190630T075333.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_010K_210_21007_16K_15000__TS_20190630T075432.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_010K_210_21010_16K_14700__TS_20190630T075357.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_010K_210_21014_16K_10500__TS_20190630T075416.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_020K_210_10501_16K_10500__TS_20190630T075524.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_020K_210_10505_16K_14700__TS_20190630T075506.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_020K_210_10507_16K_10500__TS_20190630T075449.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_050K_210_4202_16K_14700__TS_20190630T075601.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_050K_210_4203_16K_15400__TS_20190630T075544.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_100K_210_2101_16K_14700__TS_20190630T075619.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_100K_210_2102_16K_13650__TS_20190630T075639.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_200K_210_1051_16K_13650__TS_20190630T075658.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_500K_210_421_16K_15540__TS_20190630T075723.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_001M_210_211_16K_18690__TS_20190630T075743.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_002M_210_106_16K_15855__TS_20190630T075806.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_005M_210_43_16K_16338__TS_20190630T075829.txt'
#filename_rpt = 'RPT_NP_DUMP_BD2__U_010M_210_41_16K_16359__TS_20190630T075851.txt'
#
#filename_rpt = 'RPT_NP_DUMP_BD3__N_001K_210_14_16K_15000__TS_20190630T080134.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__N_001K_210_15_16K_28000__TS_20190630T080201.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__N_002K_210_14_16K_15000__TS_20190630T080239.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__N_002K_210_15_16K_21000__TS_20190630T080304.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__N_005K_210_14_16K_15000__TS_20190630T080329.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__N_005K_210_15_16K_14000__TS_20190630T080350.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_010K_210_21007_16K_15000__TS_20190630T080504.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_010K_210_21010_16K_14700__TS_20190630T080423.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_010K_210_21014_16K_10500__TS_20190630T080443.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_020K_210_10501_16K_10500__TS_20190630T080555.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_020K_210_10505_16K_14700__TS_20190630T080537.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_020K_210_10507_16K_10500__TS_20190630T080522.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_050K_210_4202_16K_14700__TS_20190630T080629.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_050K_210_4203_16K_15400__TS_20190630T080612.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_100K_210_2101_16K_14700__TS_20190630T080647.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_100K_210_2102_16K_13650__TS_20190630T080705.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_200K_210_1051_16K_13650__TS_20190630T080723.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_500K_210_421_16K_15540__TS_20190630T080741.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_001M_210_211_16K_18690__TS_20190630T080801.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_002M_210_106_16K_15855__TS_20190630T080821.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_005M_210_43_16K_16338__TS_20190630T080843.txt'
#filename_rpt = 'RPT_NP_DUMP_BD3__U_010M_210_41_16K_16359__TS_20190630T080905.txt'
#
#filename_rpt = 'RPT_NP_DUMP_BD4__N_001K_210_14_16K_15000__TS_20190701T192743.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__N_001K_210_15_16K_28000__TS_20190701T192805.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__N_002K_210_14_16K_15000__TS_20190701T192824.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__N_002K_210_15_16K_21000__TS_20190701T192838.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__N_005K_210_14_16K_15000__TS_20190701T192857.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__N_005K_210_15_16K_14000__TS_20190701T192911.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_010K_210_21007_16K_15000__TS_20190701T192947.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_010K_210_21010_16K_14700__TS_20190701T192926.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_010K_210_21014_16K_10500__TS_20190701T192937.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_020K_210_10501_16K_10500__TS_20190701T193020.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_020K_210_10505_16K_14700__TS_20190701T193009.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_020K_210_10507_16K_10500__TS_20190701T192959.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_050K_210_4202_16K_14700__TS_20190701T193128.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_050K_210_4203_16K_15400__TS_20190701T193118.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_100K_210_2101_16K_14700__TS_20190701T193139.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_100K_210_2102_16K_13650__TS_20190701T193150.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_200K_210_1051_16K_13650__TS_20190701T193201.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_500K_210_421_16K_15540__TS_20190701T193212.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_001M_210_211_16K_18690__TS_20190701T193223.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_002M_210_106_16K_15855__TS_20190701T193234.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_005M_210_43_16K_16338__TS_20190701T193245.txt'
#filename_rpt = 'RPT_NP_DUMP_BD4__U_010M_210_41_16K_16359__TS_20190701T193256.txt'
#
#filename_rpt = 'RPT_NP_DUMP_BD5__N_001K_210_14_16K_15000__TS_20190701T193340.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__N_001K_210_15_16K_28000__TS_20190701T193353.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__N_002K_210_14_16K_15000__TS_20190701T193408.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__N_002K_210_15_16K_21000__TS_20190701T193419.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__N_005K_210_14_16K_15000__TS_20190701T193431.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__N_005K_210_15_16K_14000__TS_20190701T193441.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_010K_210_21007_16K_15000__TS_20190701T193517.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_010K_210_21010_16K_14700__TS_20190701T090502.txt' ##
#filename_rpt = 'RPT_NP_DUMP_BD5__U_010K_210_21010_16K_14700__TS_20190701T193456.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_010K_210_21014_16K_10500__TS_20190701T193506.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_020K_210_10501_16K_10500__TS_20190701T193550.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_020K_210_10505_16K_14700__TS_20190701T193539.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_020K_210_10507_16K_10500__TS_20190701T193530.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_050K_210_4202_16K_14700__TS_20190701T193617.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_050K_210_4203_16K_15400__TS_20190701T193601.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_100K_210_2101_16K_14700__TS_20190701T193628.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_100K_210_2102_16K_13650__TS_20190701T193638.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_200K_210_1051_16K_13650__TS_20190701T193649.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_500K_210_421_16K_15540__TS_20190701T193701.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_001M_210_211_16K_18690__TS_20190701T193710.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_002M_210_106_16K_15855__TS_20190701T193721.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_005M_210_43_16K_16338__TS_20190701T193732.txt'
#filename_rpt = 'RPT_NP_DUMP_BD5__U_010M_210_41_16K_16359__TS_20190701T193745.txt'

#...
#filename_rpt = 'RPT_DUMP_BD1__N_001K_210_14_16K_15000__TS_20190628T083435.txt'    # normal port
#filename_rpt = 'RPT_DUMP_BD1__N_001K_210_15_16K_28000__TS_20190628T083544.txt'    # normal port
#filename_rpt = 'RPT_DUMP_BD1__N_002K_210_14_16K_15000__TS_20190628T083650.txt'    # normal port
#filename_rpt = 'RPT_DUMP_BD1__N_002K_210_15_16K_21000__TS_20190628T083718.txt'    # normal port
#filename_rpt = 'RPT_DUMP_BD1__N_005K_210_14_16K_15000__TS_20190628T083812.txt'    # normal port
#filename_rpt = 'RPT_DUMP_BD1__N_005K_210_15_16K_14000__TS_20190628T083901.txt'    # normal port 
#filename_rpt = 'RPT_DUMP_BD1__U_010K_210_21007_16K_15000__TS_20190628T084105.txt' # normal port // ratio_amg mean 1.000208
#filename_rpt = 'RPT_DUMP_BD1__U_010K_210_21010_16K_14700__TS_20190628T083938.txt' # normal port
#filename_rpt = 'RPT_DUMP_BD1__U_010K_210_21014_16K_10500__TS_20190628T084050.txt' # normal port
#filename_rpt = 'RPT_DUMP_BD1__U_020K_210_10501_16K_10500__TS_20190628T084317.txt' # normal port
#filename_rpt = 'RPT_DUMP_BD1__U_020K_210_10505_16K_14700__TS_20190628T084147.txt' # normal port
#filename_rpt = 'RPT_DUMP_BD1__U_020K_210_10507_16K_10500__TS_20190628T084132.txt' # normal port
#filename_rpt = 'RPT_DUMP_BD1__U_050K_210_4202_16K_14700__TS_20190628T084414.txt'  # normal port
#filename_rpt = 'RPT_DUMP_BD1__U_050K_210_4203_16K_15400__TS_20190628T084351.txt'  # normal port
#filename_rpt = 'RPT_DUMP_BD1__U_100K_210_2101_16K_14700__TS_20190628T084434.txt'  # normal port
#filename_rpt = 'RPT_DUMP_BD1__U_100K_210_2102_16K_13650__TS_20190628T084449.txt'  # normal port
#filename_rpt = 'RPT_DUMP_BD1__U_200K_210_1051_16K_13650__TS_20190628T084516.txt'  # normal port
#filename_rpt = 'RPT_DUMP_BD1__U_500K_210_421_16K_15540__TS_20190628T084536.txt'   # normal port
#filename_rpt = 'RPT_DUMP_BD1__U_001M_210_211_16K_18690__TS_20190628T084644.txt'   # normal port
#filename_rpt = 'RPT_DUMP_BD1__U_002M_210_106_16K_15855__TS_20190628T084745.txt'   # normal port
#filename_rpt = 'RPT_DUMP_BD1__U_005M_210_43_16K_16338__TS_20190628T084817.txt'    # normal port
#filename_rpt = 'RPT_DUMP_BD1__U_010M_210_41_16K_16359__TS_20190628T084852.txt'    # normal port
#...
#filename_rpt = 'RPT_DUMP_BD1__N_001K_210_2100_16K_16300__TS_20190628T083414.txt'  # normal port // poor
#filename_rpt = 'RPT_DUMP_BD1__N_002K_210_1050_16K_16300__TS_20190628T083620.txt'  # normal port // poor
#filename_rpt = 'RPT_DUMP_BD1__N_005K_210_420_16K_16300__TS_20190628T083740.txt'   # normal port // poor
#filename_rpt = 'RPT_DUMP_BD1__U_001M_210_212_16K_15855__TS_20190628T084603.txt'   # normal port // not selected
#...
#filename_rpt = 'RPT_DUMP_BD1__N_001K_210_14_16K_15000__TS_20190628T133718.txt'    # cross  port // ratio > 1 ??
#filename_rpt = 'RPT_DUMP_BD1__N_001K_210_15_16K_28000__TS_20190628T133704.txt'    # cross  port // ratio > 1 ??
#filename_rpt = 'RPT_DUMP_BD1__N_002K_210_14_16K_15000__TS_20190628T133653.txt'    # cross  port // ratio > 1 ??
#filename_rpt = 'RPT_DUMP_BD1__N_002K_210_15_16K_21000__TS_20190628T133642.txt'    # cross  port // ratio > 1 ??
#filename_rpt = 'RPT_DUMP_BD1__N_005K_210_14_16K_15000__TS_20190628T133629.txt'    # cross  port // ratio > 1 ??
#filename_rpt = 'RPT_DUMP_BD1__N_005K_210_15_16K_14000__TS_20190628T133619.txt'    # cross  port // ratio > 1 ??
#filename_rpt = 'RPT_DUMP_BD1__U_010K_210_21007_16K_15000__TS_20190628T133536.txt' # cross  port // ratio_amg mean 1.000126 // ratio > 1 ??
#filename_rpt = 'RPT_DUMP_BD1__U_010K_210_21010_16K_14700__TS_20190628T133557.txt' # cross  port // ratio > 1 ??
#filename_rpt = 'RPT_DUMP_BD1__U_010K_210_21014_16K_10500__TS_20190628T133547.txt' # cross  port // ratio > 1 ??
#filename_rpt = 'RPT_DUMP_BD1__U_020K_210_10501_16K_10500__TS_20190628T133458.txt' # cross  port
#filename_rpt = 'RPT_DUMP_BD1__U_020K_210_10505_16K_14700__TS_20190628T133512.txt' # cross  port
#filename_rpt = 'RPT_DUMP_BD1__U_020K_210_10507_16K_10500__TS_20190628T133522.txt' # cross  port
#filename_rpt = 'RPT_DUMP_BD1__U_050K_210_4202_16K_14700__TS_20190628T133437.txt'  # cross  port
#filename_rpt = 'RPT_DUMP_BD1__U_050K_210_4203_16K_15400__TS_20190628T133447.txt'  # cross  port
#filename_rpt = 'RPT_DUMP_BD1__U_100K_210_2101_16K_14700__TS_20190628T133427.txt'  # cross  port
#filename_rpt = 'RPT_DUMP_BD1__U_100K_210_2102_16K_13650__TS_20190628T133417.txt'  # cross  port
#filename_rpt = 'RPT_DUMP_BD1__U_200K_210_1051_16K_13650__TS_20190628T133407.txt'  # cross  port
#filename_rpt = 'RPT_DUMP_BD1__U_500K_210_421_16K_15540__TS_20190628T133355.txt'   # cross  port
#filename_rpt = 'RPT_DUMP_BD1__U_001M_210_211_16K_18690__TS_20190628T133342.txt'   # cross  port
#filename_rpt = 'RPT_DUMP_BD1__U_002M_210_106_16K_15855__TS_20190628T133331.txt'   # cross  port
#filename_rpt = 'RPT_DUMP_BD1__U_005M_210_43_16K_16338__TS_20190628T133317.txt'    # cross  port
#filename_rpt = 'RPT_DUMP_BD1__U_010M_210_41_16K_16359__TS_20190628T133205.txt'    # cross  port



#filename_rpt = 'RPT___TS_20190520T154004.txt'
#filename_rpt = 'RPT___TS_20190520T164437.txt' # 200kHz @ 15Msps
#filename_rpt = 'RPT___TS_20190520T185550.txt'
#filename_rpt = 'RPT___TS_20190521T115947.txt' # 2MHz @ 14Msps
#filename_rpt = 'RPT___TS_20190522T160603.txt' # 200kHz @ 15Msps, cross adc port
#filename_rpt = 'RPT___TS_20190523T105247.txt' # 2MHz signal @ 1.98Msps << down-sampling // 210/106 = 1.98113208, 1237, 129885, (2000) - ((210000) / 106) = 18.8679245283
#filename_rpt = 'RPT___TS_20190523T141926.txt' # 2MHz signal / dwave @ 1.98Msps
#filename_rpt = 'RPT___TS_20190523T151701.txt' # DUMP_9-1___TS_20190523T142856.csv # test 
#
#filename_rpt = 'RPT___TS_20190527T110348.txt' # DUMP_2-0___TS_20190523T160933.csv #
#filename_rpt = 'RPT___TS_20190527T110855.txt'  # DUMP_2-4___TS_20190523T160938.csv # 
#filename_rpt = 'RPT___TS_20190527T110917.txt'  # DUMP_9-0___TS_20190523T160943.csv #
#filename_rpt = 'RPT___TS_20190527T110938.txt'  # DUMP_9-1___TS_20190523T160948.csv #
#
#filename_rpt = 'RPT___TS_20190528T015511.txt' # 129885
#filename_rpt = 'RPT___TS_20190528T014829.txt' # 64365
#filename_rpt = 'RPT___TS_20190528T014456.txt' # 32235
#filename_rpt = 'RPT___TS_20190528T021927.txt' # 15855
#filename_rpt = 'RPT___TS_20190528T022203.txt' # 7665
#filename_rpt = 'RPT___TS_20190528T022241.txt' # 3885
#filename_rpt = 'RPT___TS_20190528T022315.txt' # 1995
#filename_rpt = 'RPT___TS_20190528T022350.txt' # 735
#filename_rpt = 'RPT___TS_20190528T024045.txt' # 210
#filename_rpt = 'RPT___TS_20190528T031436.txt' # 105
#
#filename_rpt = 'RPT___TS_20190528T091136.txt' # 9-1, 105
#filename_rpt = 'RPT___TS_20190528T091834.txt' # 9-1, 3885
#filename_rpt = 'RPT___TS_20190528T092851.txt' # 9-1, 32235
#filename_rpt = 'RPT___TS_20190527T110938.txt' # 9-1, 129885

#filename_rpt = 'RPT___TS_20190530T092042.txt'  # DUMP_9-0___...
#filename_rpt = 'RPT___TS_20190530T092343.txt'  # DUMP_9-1___...

#filename_rpt = 'DUMP_2___TS_20190601/RPT___TS_20190601T192059.txt' # DUMP_2-0 # CMU-CPU-TEST-BD3
#filename_rpt = 'DUMP_2___TS_20190601/RPT___TS_20190601T191907.txt' # DUMP_2-4 # CMU-CPU-TEST-BD3

#filename_rpt = 'DUMP_2___TS_20190602/RPT___TS_20190603T082207.txt' # DUMP_2-0 # CMU-CPU-F5500-BD1
#filename_rpt = 'DUMP_2___TS_20190602/RPT___TS_20190603T081408.txt' # DUMP_2-4 # CMU-CPU-F5500-BD1

#filename_rpt = 'RPT___TS_20190604T090358.txt'  # DUMP_2-4___...
#filename_rpt = 'RPT___TS_20190605T082830.txt'  # DUMP_7-1___...



## make title on fig 

FIG_TITLE = filename_rpt
#FIG_TITLE = filename_rpt + ' // normal ports'
#FIG_TITLE = filename_rpt + ' // cross ports'

#FIG_TITLE = filename_rpt + ' // DUMP_2-0'
#FIG_TITLE = filename_rpt + ' // DUMP_2-4'
#FIG_TITLE = filename_rpt + ' // DUMP_7-0'
#FIG_TITLE = filename_rpt + ' // DUMP_7-1'
#FIG_TITLE = filename_rpt + ' // DUMP_9-0'
#FIG_TITLE = filename_rpt + ' // DUMP_9-1'

## 
#MAX_ROW_COUNT    = 1000
#MAX_ROW_COUNT    = 1300
MAX_ROW_COUNT    = 4000
LAST_ROW_SIDE_EN = 1

## read measure from the report file 
def f_read_rpt_file(filename_rpt, MAX_ROW_COUNT=50, LAST_ROW_SIDE_EN=0):
	filename_list  = []
	Fs_list           = []
	TEST_FREQ_HZ_list = []
	SAMPLES_DFT_list  = []
	TEMP_list      = []
	mean_x_list    = []
	mean_y_list    = []
	std_x_list     = []
	std_y_list     = []
	mag_X_f_0_list = []
	mag_Y_f_0_list = []
	mag_X_f_t_list = []
	mag_Y_f_t_list = []
	mag_R_f_t_list = []
	ang_R_f_t_list = []
	with open(filename_rpt, 'r', newline='') as csvFile:
		read_data = csv.reader(csvFile, delimiter=',')
		line_count = 0
		for row in read_data:
			# count lines
			line_count += 1
			#
			# add list 
			#
			filename_list     +=[row[ 0]]
			Fs_list           +=[int(row[1])] ##
			TEMP_list         +=[float(row[ 4])]
			mean_x_list       +=[float(row[ 7])]
			mean_y_list       +=[float(row[ 8])]
			std_x_list        +=[float(row[ 9])]
			std_y_list        +=[float(row[10])]
			TEST_FREQ_HZ_list += [float(row[11])] ##
			#
			SAMPLES_DFT       = int(row[12])
			SAMPLES_DFT_list  += [SAMPLES_DFT] ##
			#
			mag_X_f_0_list    +=[np.real(complex(row[13]))/SAMPLES_DFT] # scale for 1/N*DFT
			mag_Y_f_0_list    +=[np.real(complex(row[14]))/SAMPLES_DFT] # scale for 1/N*DFT
			mag_X_f_t_list    +=[float(row[19])/SAMPLES_DFT] # scale for 1/N*DFT
			mag_Y_f_t_list    +=[float(row[20])/SAMPLES_DFT] # scale for 1/N*DFT
			mag_R_f_t_list    +=[float(row[21])]
			ang_R_f_t_list    +=[float(row[22])]
			#
			if LAST_ROW_SIDE_EN == 0 and line_count >= MAX_ROW_COUNT :
				break
			#
		#
		if LAST_ROW_SIDE_EN == 1 :
			filename_list     =  filename_list    [(-MAX_ROW_COUNT):]
			Fs_list           =  Fs_list          [(-MAX_ROW_COUNT):]
			TEMP_list         =  TEMP_list        [(-MAX_ROW_COUNT):]
			mean_x_list       =  mean_x_list      [(-MAX_ROW_COUNT):]
			mean_y_list       =  mean_y_list      [(-MAX_ROW_COUNT):]
			std_x_list        =  std_x_list       [(-MAX_ROW_COUNT):]
			std_y_list        =  std_y_list       [(-MAX_ROW_COUNT):]
			TEST_FREQ_HZ_list =  TEST_FREQ_HZ_list[(-MAX_ROW_COUNT):]
			SAMPLES_DFT_list  =  SAMPLES_DFT_list [(-MAX_ROW_COUNT):]
			mag_X_f_0_list    =  mag_X_f_0_list   [(-MAX_ROW_COUNT):]
			mag_Y_f_0_list    =  mag_Y_f_0_list   [(-MAX_ROW_COUNT):]
			mag_X_f_t_list    =  mag_X_f_t_list   [(-MAX_ROW_COUNT):]
			mag_Y_f_t_list    =  mag_Y_f_t_list   [(-MAX_ROW_COUNT):]
			mag_R_f_t_list    =  mag_R_f_t_list   [(-MAX_ROW_COUNT):]
			ang_R_f_t_list    =  ang_R_f_t_list   [(-MAX_ROW_COUNT):]
		#
	return  filename_list, Fs_list, TEST_FREQ_HZ_list, SAMPLES_DFT_list, TEMP_list, \
		mean_x_list, mean_y_list, std_x_list, std_y_list, mag_X_f_0_list, mag_Y_f_0_list, \
		mag_X_f_t_list, mag_Y_f_t_list, mag_R_f_t_list, ang_R_f_t_list
	
## call read function : f_read_rpt_file
#ret = f_read_rpt_file(filename_rpt)
ret = f_read_rpt_file(filename_rpt, MAX_ROW_COUNT, LAST_ROW_SIDE_EN) 
#
filename_list     = ret[0]
Fs_list           = ret[1]
TEST_FREQ_HZ_list = ret[2]
SAMPLES_DFT_list  = ret[3]
TEMP_list         = ret[4]
mean_x_list       = ret[5]
mean_y_list       = ret[6]
std_x_list        = ret[7]
std_y_list        = ret[8]
mag_X_f_0_list    = ret[9]
mag_Y_f_0_list    = ret[10]
mag_X_f_t_list    = ret[11]
mag_Y_f_t_list    = ret[12]
mag_R_f_t_list    = ret[13]
ang_R_f_t_list    = ret[14]

# test
#ret = f_read_rpt_file(filename_rpt)
#print(len(ret))
#print(ret[0])

## assign common parameters 
SAMPLES_DFT  = SAMPLES_DFT_list[0]
Fs           = Fs_list[0]
TEST_FREQ_HZ = TEST_FREQ_HZ_list[0]

## check PLC number 
PLC_PER_MEASURE = (SAMPLES_DFT/Fs)/(1/60.0) # 60Hz base PLC count
print('{} = {}'.format('PLC_PER_MEASURE',PLC_PER_MEASURE))
print('{} = {}'.format('SAMPLES_DFT',SAMPLES_DFT))
print('{} = {}'.format('Fs',Fs))
	
## measure average
# ... ns
#NUM_PLC = 10
#NUM_PLC = 0.5
NUM_PLC = 0
if NUM_PLC==0:
	NUM_MEAS_AVG = 1
else:
	NUM_MEAS_AVG = int(NUM_PLC / PLC_PER_MEASURE + 0.5)
#NUM_MEAS_AVG = 200
print('{} = {}'.format('NUM_MEAS_AVG',NUM_MEAS_AVG))

#
def f_measure_average (in_list, num_measure_average):
	delayed_list = in_list
	out_list = [0]*len(in_list)
	for ii in range(num_measure_average):
		#print(ii)
		# sum 
		out_list = [sum(x) for x in zip(out_list, delayed_list)]
		# delay
		#delayed_list = [0] +  delayed_list[:-1]
		delayed_list = [delayed_list[0]] +  delayed_list[:-1]
		#delayed_list = [delayed_list[-1]] +  delayed_list[:-1]
		pass
	#
	out_list = [x / num_measure_average for x in out_list]
	#
	# duplicate leading
	out_list[0:(num_measure_average-1)] = [out_list[num_measure_average-1]]*(num_measure_average-1)
	#
	return out_list

TEMP_list = f_measure_average(TEMP_list, NUM_MEAS_AVG)
#
mean_x_list = f_measure_average(mean_x_list, NUM_MEAS_AVG)
mean_y_list = f_measure_average(mean_y_list, NUM_MEAS_AVG)
#
std_x_list = f_measure_average(std_x_list, NUM_MEAS_AVG)
std_y_list = f_measure_average(std_y_list, NUM_MEAS_AVG)
#
mag_X_f_0_list = f_measure_average(mag_X_f_0_list, NUM_MEAS_AVG)
mag_Y_f_0_list = f_measure_average(mag_Y_f_0_list, NUM_MEAS_AVG)
#
mag_X_f_t_list = f_measure_average(mag_X_f_t_list, NUM_MEAS_AVG)
mag_Y_f_t_list = f_measure_average(mag_Y_f_t_list, NUM_MEAS_AVG)
#
mag_R_f_t_list = f_measure_average(mag_R_f_t_list, NUM_MEAS_AVG)
ang_R_f_t_list = f_measure_average(ang_R_f_t_list, NUM_MEAS_AVG)
	
	
## control index	
idx_start = 0
idx_stop  = len(filename_list)
#
#idx_start = 0
#idx_stop  = 3470
#
#idx_start = 3470
#idx_stop  = 3650
#
#idx_start = 3650
#idx_stop  = len(filename_list)


# xlist
xlist = list(range(len(filename_list)))
print(xlist[0])
print(xlist[-1])

# reduction
xlist = xlist[idx_start:idx_stop]
filename_list  = filename_list [idx_start:idx_stop]
TEMP_list      = TEMP_list     [idx_start:idx_stop]
mean_x_list    = mean_x_list   [idx_start:idx_stop]
mean_y_list    = mean_y_list   [idx_start:idx_stop]
std_x_list     = std_x_list    [idx_start:idx_stop]
std_y_list     = std_y_list    [idx_start:idx_stop]
mag_X_f_0_list = mag_X_f_0_list[idx_start:idx_stop]
mag_Y_f_0_list = mag_Y_f_0_list[idx_start:idx_stop]
mag_X_f_t_list = mag_X_f_t_list[idx_start:idx_stop]
mag_Y_f_t_list = mag_Y_f_t_list[idx_start:idx_stop]
mag_R_f_t_list = mag_R_f_t_list[idx_start:idx_stop]
ang_R_f_t_list = ang_R_f_t_list[idx_start:idx_stop]


## show filenames 
print('>>> the first: {}'.format(filename_list[0]))
print('>>> the last : {}'.format(filename_list[-1]))
print('>>> len : {}'.format(len(filename_list)))

## calculate measures variations
print('>>> {}'.format('TEMP'))
#mean_TEMP=np.mean(TEMP_list[0:-1]) ##
mean_TEMP=np.mean(TEMP_list) 
std_TEMP =np.std (TEMP_list)
cv_TEMP  =np.std (TEMP_list)/np.mean(TEMP_list)

print('>>> {}'.format('mag_X_f_t'))
mean_mag_X=np.mean(mag_X_f_t_list)
std_mag_X =np.std (mag_X_f_t_list)
cv_mag_X  =np.std (mag_X_f_t_list)/np.mean(mag_X_f_t_list)

print('>>> {}'.format('mag_Y_f_t'))
mean_mag_Y=np.mean(mag_Y_f_t_list)
std_mag_Y =np.std (mag_Y_f_t_list)
cv_mag_Y  =np.std (mag_Y_f_t_list)/np.mean(mag_Y_f_t_list)

print('>>> {}'.format('mag_R_f_t'))
mean_mag_R=np.mean(mag_R_f_t_list)
std_mag_R =np.std (mag_R_f_t_list)
cv_mag_R  =np.std (mag_R_f_t_list)/np.mean(mag_R_f_t_list)

print('>>> {}'.format('ang_R_f_t'))
mean_ang_R=np.mean(ang_R_f_t_list)
std_ang_R =np.std (ang_R_f_t_list)
cv_ang_R  =np.std (ang_R_f_t_list)/abs(np.mean(ang_R_f_t_list))

print('>>> {}'.format('mag_X_f_0'))
mean_mag_X_0=np.mean(mag_X_f_0_list)
std_mag_X_0 =np.std (mag_X_f_0_list)
cv_mag_X_0  =np.std (mag_X_f_0_list)/abs(np.mean(mag_X_f_0_list))

print('>>> {}'.format('mag_Y_f_t'))
mean_mag_Y_0=np.mean(mag_Y_f_0_list)
std_mag_Y_0 =np.std (mag_Y_f_0_list)
cv_mag_Y_0  =np.std (mag_Y_f_0_list)/abs(np.mean(mag_Y_f_0_list))

print('>>> {}'.format('std_x'))
mean_std_x=np.mean(std_x_list)
std_std_x =np.std (std_x_list)
cv_std_x  =np.std (std_x_list)/abs(np.mean(std_x_list))

print('>>> {}'.format('std_y'))
mean_std_y=np.mean(std_y_list)
std_std_y =np.std (std_y_list)
cv_std_y  =np.std (std_y_list)/abs(np.mean(std_y_list))


## display measures vs times
#
FIG_1_EN = 1 #
FIG_2_EN = 0
FIG_3_EN = 0
FIG_4_EN = 0
FIG_5_EN = 0
#
#
plt.ion() # matplotlib interactive mode 

##
if FIG_1_EN:
	FIG_NUM = 1 # for new figure windows
	fig = plt.figure(FIG_NUM,figsize=(15,10))
	#
	plt.subplot(411) ### 
	#
	#xlist = list(range(len(TEMP_list)))
	plt.plot(xlist,TEMP_list, 'r-')
	plt.title(FIG_TITLE + '\n   temperatures over times: ' + 
		'(mean,std,cv)=({:.3f}, {:.3f}, {:.6f})'.format(mean_TEMP,std_TEMP,cv_TEMP))
	plt.ylabel('TEMP[C]')
	plt.xlabel('trials')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	#
	plt.subplot(412) ### 
	#
	#xlist = list(range(len(mag_X_f_t_list)))
	plt.plot(xlist,mag_X_f_t_list, 'r-', alpha=0.75)
	plt.plot(xlist,mag_Y_f_t_list, 'b-', alpha=0.75)
	plt.title('\n mag_X(red) and mag_Y(blue) over times: '+
		'\n (mean,std,cv)of mag_X=({:.6f}, {:.6f}, {:.6f})'.format(mean_mag_X,std_mag_X,cv_mag_X) +
		'\n (mean,std,cv)of mag_Y=({:.6f}, {:.6f}, {:.6f})'.format(mean_mag_Y,std_mag_Y,cv_mag_Y)
		)
	plt.ylabel('code')
	plt.xlabel('trials')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	#
	plt.subplot(413) ### 
	#
	#xlist = list(range(len(mag_R_f_t_list)))
	plt.plot(xlist,mag_R_f_t_list, 'r-')
	#
	# ref line: mean
	plt.plot(xlist,[mean_mag_R]*len(mag_R_f_t_list), 'g-', alpha=0.5, label='line mean')
	# ref line: 200ppm
	plt.plot(xlist,[mean_mag_R*1.000200]*len(mag_R_f_t_list), 'g:', alpha=0.5, label='line 200ppm')
	plt.plot(xlist,[mean_mag_R*0.999800]*len(mag_R_f_t_list), 'g:', alpha=0.5)
	# ref line: 0.1%
	#plt.plot(xlist,[mean_mag_R*1.001000]*len(mag_R_f_t_list), 'g-.', alpha=0.5, label='line 0.1%')
	#plt.plot(xlist,[mean_mag_R*0.999000]*len(mag_R_f_t_list), 'g-.', alpha=0.5)
	#
	plt.legend()
	#
	plt.title('\n   mag_R over times: ' + 
		'(mean,std,cv)=({:.6f}, {:.6g}, {:.6f})'.format(mean_mag_R,std_mag_R,cv_mag_R))
	plt.ylabel('ratio')
	plt.xlabel('trials')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	#
	plt.subplot(414) ### 
	#
	#xlist = list(range(len(ang_R_f_t_list)))
	plt.plot(xlist,ang_R_f_t_list, 'r-')
	#
	# ref line: mean
	plt.plot(xlist,[mean_ang_R]*len(mag_R_f_t_list), 'g-', alpha=0.5, label='line mean')
	# ref line: 200ppm
	plt.plot(xlist,[mean_ang_R*1.000200]*len(mag_R_f_t_list), 'g:', alpha=0.5, label='line 200ppm')
	plt.plot(xlist,[mean_ang_R*0.999800]*len(mag_R_f_t_list), 'g:', alpha=0.5)
	# ref line: 0.1%
	#plt.plot(xlist,[mean_ang_R*1.001000]*len(mag_R_f_t_list), 'g-.', alpha=0.5, label='line 0.1%')
	#plt.plot(xlist,[mean_ang_R*0.999000]*len(mag_R_f_t_list), 'g-.', alpha=0.5)
	#
	plt.legend()
	#
	plt.title('\n   ang_R over times: ' + 
		'(mean,std,cv)=({:.6f}, {:.6g}, {:.6f})'.format(mean_ang_R,std_ang_R,cv_ang_R))
	plt.ylabel('[degree]')
	plt.xlabel('trials')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	#
	fig.tight_layout()
	#
	# save figure 
	filename_fig = filename_rpt[0:-4]+'.png'
	print(filename_fig)
	plt.savefig(filename_fig)
	

##
if FIG_2_EN:
	FIG_NUM = 2 # for new figure windows
	fig = plt.figure(FIG_NUM,figsize=(15,10))
	#
	plt.subplot(411) ### 
	#
	#xlist = list(range(len(TEMP_list)))
	plt.plot(xlist,TEMP_list, 'r-')
	plt.title(FIG_TITLE + '\n   temperatures over times: ' + 
		'(mean,std,cv)=({:.3f}, {:.3f}, {:.6f})'.format(mean_TEMP,std_TEMP,cv_TEMP))
	plt.ylabel('TEMP[C]')
	plt.xlabel('trials')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	#
	plt.subplot(412) ### 
	#
	#xlist = list(range(len(mean_x_list)))
	plt.plot(xlist,mean_x_list, 'r-', alpha=0.75)
	plt.plot(xlist,mean_y_list, 'b-', alpha=0.75)
	plt.title('\n mean_x(red) and mean_y(blue) over times: '+
		''
		#'\n (mean,std,cv)of mag_X=({:.6f}, {:.6f}, {:.6f})'.format(mean_mag_X,std_mag_X,cv_mag_X) +
		#'\n (mean,std,cv)of mag_Y=({:.6f}, {:.6f}, {:.6f})'.format(mean_mag_Y,std_mag_Y,cv_mag_Y)
		)
	plt.ylabel('code')
	plt.xlabel('trials')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	#
	plt.subplot(413) ### 
	#
	#xlist = list(range(len(std_x_list)))
	plt.plot(xlist,std_x_list, 'r-', alpha=0.75)
	plt.plot(xlist,std_y_list, 'b-', alpha=0.75)
	plt.title('\n std_x(red) and std_y(blue) over times: '+
		#''
		'\n (mean,std,cv)of std_x=({:.6f}, {:.6f}, {:.6f})'.format(mean_std_x,std_std_x,cv_std_x) +
		'\n (mean,std,cv)of std_y=({:.6f}, {:.6f}, {:.6f})'.format(mean_std_y,std_std_y,cv_std_y)
		)
	plt.ylabel('code')
	plt.xlabel('trials')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	#
	plt.subplot(414) ### 
	#
	#xlist = list(range(len(mag_X_f_0_list)))
	plt.plot(xlist,mag_X_f_0_list, 'r-', alpha=0.75)
	plt.plot(xlist,mag_Y_f_0_list, 'b-', alpha=0.75)
	plt.title('\n DC level X(red) and DC level Y(blue) over times: '+
		#''
		'\n (mean,std,cv)of DC X=({:.6f}, {:.6f}, {:.6f})'.format(mean_mag_X_0,std_mag_X_0,cv_mag_X_0) +
		'\n (mean,std,cv)of DC Y=({:.6f}, {:.6f}, {:.6f})'.format(mean_mag_Y_0,std_mag_Y_0,cv_mag_Y_0)
		)
	plt.ylabel('code')
	plt.xlabel('trials')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	#
	fig.tight_layout()
	#

##
if FIG_3_EN:
	FIG_NUM = 3 # for new figure windows
	fig = plt.figure(FIG_NUM,figsize=(15,10))
	#
	plt.subplot(211) ### 
	#
	#xlist = list(range(len(TEMP_list)))
	plt.plot(xlist,TEMP_list, 'r-')
	plt.title(FIG_TITLE + '\n   temperatures over times: ' + 
		'(mean,std,cv)=({:.3f}, {:.3f}, {:.6f})'.format(mean_TEMP,std_TEMP,cv_TEMP))
	plt.ylabel('TEMP[C]')
	plt.xlabel('trials')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	#
	plt.subplot(212) ### 
	#
	#xlist = list(range(len(mag_X_f_t_list)))
	plt.plot(xlist,mag_X_f_t_list, 'r-', alpha=0.8)
	plt.plot(xlist,mag_Y_f_t_list, 'b-', alpha=0.8)
	plt.title('\n mag_X(red) and mag_Y(blue) over times: '+
		'\n (mean,std,cv)of mag_X=({:.6f}, {:.6f}, {:.6f})'.format(mean_mag_X,std_mag_X,cv_mag_X) +
		'\n (mean,std,cv)of mag_Y=({:.6f}, {:.6f}, {:.6f})'.format(mean_mag_Y,std_mag_Y,cv_mag_Y)
		)
	plt.ylabel('code')
	plt.xlabel('trials')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	#
	fig.tight_layout()
	#

##
if FIG_4_EN:
	FIG_NUM = 4 # for new figure windows
	fig = plt.figure(FIG_NUM,figsize=(15,10))
	#
	plt.subplot(311) ### 
	#
	#xlist = list(range(len(TEMP_list)))
	plt.plot(xlist,TEMP_list, 'r-')
	plt.title(FIG_TITLE + '\n   temperatures over times: ' + 
		'(mean,std,cv)=({:.3f}, {:.3f}, {:.6f})'.format(mean_TEMP,std_TEMP,cv_TEMP))
	plt.ylabel('TEMP[C]')
	plt.xlabel('trials')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	#
	plt.subplot(312) ### 
	#
	#xlist = list(range(len(mag_R_f_t_list)))
	plt.plot(xlist,mag_R_f_t_list, 'r-')
	plt.title('\n   mag_R over times: ' + 
		'(mean,std,cv)=({:.6f}, {:.6g}, {:.6f})'.format(mean_mag_R,std_mag_R,cv_mag_R))
	plt.ylabel('ratio')
	plt.xlabel('trials')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	#
	plt.subplot(313) ### 
	#
	#xlist = list(range(len(ang_R_f_t_list)))
	plt.plot(xlist,ang_R_f_t_list, 'r-')
	plt.title('\n   ang_R over times: ' + 
		'(mean,std,cv)=({:.6f}, {:.6g}, {:.6f})'.format(mean_ang_R,std_ang_R,cv_ang_R))
	plt.ylabel('[degree]')
	plt.xlabel('trials')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	#
	fig.tight_layout()
	#

##
if FIG_5_EN:
	FIG_NUM = 5 # for new figure windows
	fig = plt.figure(FIG_NUM,figsize=(15,10))
	#
	plt.subplot(311) ### 
	#
	#xlist = list(range(len(TEMP_list)))
	plt.plot(xlist,TEMP_list, 'r-')
	plt.title(FIG_TITLE + '\n   temperatures over times: ' + 
		'(mean,std,cv)=({:.3f}, {:.3f}, {:.6f})'.format(mean_TEMP,std_TEMP,cv_TEMP))
	plt.ylabel('TEMP[C]')
	plt.xlabel('trials')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	#
	plt.subplot(312) ### 
	#
	#xlist = list(range(len(mean_x_list)))
	plt.plot(xlist,mean_x_list, 'r-', alpha=0.8)
	plt.plot(xlist,mean_y_list, 'b-', alpha=0.8)
	plt.title('\n mean_x(red) and mean_y(blue) over times: '+
		''
		#'\n (mean,std,cv)of mag_X=({:.6f}, {:.6f}, {:.6f})'.format(mean_mag_X,std_mag_X,cv_mag_X) +
		#'\n (mean,std,cv)of mag_Y=({:.6f}, {:.6f}, {:.6f})'.format(mean_mag_Y,std_mag_Y,cv_mag_Y)
		)
	plt.ylabel('code')
	plt.xlabel('trials')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	#
	plt.subplot(313) ### 
	#
	#xlist = list(range(len(std_x_list)))
	plt.plot(xlist,std_x_list, 'r-', alpha=0.8)
	plt.plot(xlist,std_y_list, 'b-', alpha=0.8)
	plt.title('\n std_x(red) and std_y(blue) over times: '+
		#''
		'\n (mean,std,cv)of std_x=({:.6f}, {:.6f}, {:.6f})'.format(mean_std_x,std_std_x,cv_std_x) +
		'\n (mean,std,cv)of std_y=({:.6f}, {:.6f}, {:.6f})'.format(mean_std_y,std_std_y,cv_std_y)
		)
	plt.ylabel('code')
	plt.xlabel('trials')
	plt.grid(True)
	plt.autoscale(enable=True, axis='x', tight=True)
	#
	fig.tight_layout()
	#

#
####

